<?php

include_once realpath(__DIR__).'/compiller/compiller.php';
$compiller =  new gaCompiller(realpath(__DIR__), $interfaces, $classes, $factories,$modules);
if(empty($_GET)){
    ?>
    <html>
    <head>
        <style>
            body {
                background-color: #212121;
                color: #ff8135;
                font-size: 1.2em;
                font-family: 'Arial',sans-serif;
            }
            form {

                margin: 0;
                padding: 0;

            }
            .name {
                width: 210px;
                color:#5dff87;
                display: inline-block;
                vertical-align: top;
            }
            .value{
                display: inline-block;
            }
            .field {
                padding-left: 20px;
                margin-top: 10px;
                padding-bottom: 20px;
                border-bottom: 1px #ccc solid;
            }
            .value strong {
                min-width: 120px;
                display: inline-block;
            }
            .form {
                width: 700px;
                margin: 0px auto;
                border: 1px #a7a9a7 solid;
            }
        </style>
    </head>
    <body>
    <div class="form">
    <form method="get" action="builder.txt" target="_blank">
        <div class="field">
            <div class="name">
            Фабрики
            </div>
            <div class="value">
                <?php
                foreach($compiller->getFactories() as $factory=>$types){
                    echo "<div><strong>$factory</strong> <select name='f[$factory]'>";
                    foreach($types as $k=>$v){
                        echo "<option value='$k'>$k ".$v['size']."  (".$v['csize']." сжатый)</option>";
                    }
                    echo "<option value=''>No add</option></select></div>";
                }
                ?>
            </div>
        </div>

        <div class="field">
            <div class="name">
                Модули
            </div>
            <div class="value">
                <?php
                foreach($compiller->getModules() as $module=>$v){
                    echo "<div><strong>$module</strong><input type='checkbox' name='m[$module]'  checked value='$module'> ".$v['size']."  (".$v['csize']." сжатый)";

                    echo "</div>";
                }
                ?>
            </div>
        </div>

        <div class="field">
            <div class="name">
                Скомпилировать как:
            </div>
            <div class="value">
            <select name="data_type">
                <option value="file">Код для cохранения в файл</option>
                <option value="eval">Код для Eval()</option>

            </select>
            </div>
        </div>
        <div class="field">
            <div class="name">
               Сжатие:
            </div>
            <div class="value">
                Удаление пробелов и комментариев <input type='checkbox' name='compress_whitespaces' value='1'>
                <br><br>
                Укоротить названия переменных?  <input type='checkbox' name='compress_variables' value='1'>
            </div>
        </div>
        <div class="field">

            <div class="value">
                <input type='submit' value='Собрать'>
            </div>
        </div>
    </form>
    </div>
    </body>
    </html>
<?php
    exit;
}
header("Content-Type: text/plain");
if(isset($_GET['f']) && is_array($_GET['f']) && !empty($_GET['f'])) {
    $factories = $_GET['f'];
}
if(isset($_GET['m']) && is_array($_GET['m']) && !empty($_GET['m'])) {
    $modules = $_GET['m'];
}
$compiller =  new gaCompiller(realpath(__DIR__), $interfaces, $classes, $factories, $modules);
if(isset($_GET['compress_whitespaces'])) {
    $compiller->compressWhitespaces();
}
if(isset($_GET['compress_variables'])) {
    $compiller->compressVariables();
}

if(isset($_GET['data_type']) && $_GET['data_type']=='eval') {
    $compiller->compile();
} else{
    $compiller->_file();
}
$compiller->_echo();
