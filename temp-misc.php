<?php


/**
 * Проверяет наличие ячейки массива или сразу наличие нескольких
 * Решает проблему длинных проверок массива
 * пример замена такого кода:
 * if(array_key_exists('tmp_name', $_FILES)  && array_key_exists('name', $_FILES)  && && array_key_exists('error', $_FILES)) {
 *      ////code
 * }
 * на такой:
 * if(arkex(['tmp_name','name','error'], $_POST) {
 *    //// code
 * }
 * array_key_exists() multi
 */
function arkex($key, $a)  {
    if(is_array($key)) {
        foreach($key as $v) {
            if(!arkex($v,$a)){return false;}
        }
        return true;
    }
    return is_array($a) ? array_key_exists($key, $a) : false;
}


/**
 * Получение вложенной ячейки массива с проверкой функцией, если передана
 * пример
 * $arr = [
 *          'key'=> [
 *              'sub_key'=> [
 *                  'empty_key' => 0,
 *                  'int_key' => 12213,
 *                  'object_key' => new waModel();
 *               ]
 *           ]
 *  ];
 * arget($arr, 'key', 'default') return ['sub_key'=>....] - ячейка есть, вернулось ее значение
 * arget($arr, 'key', 'default', 'is_string') return 'default' - значение не прошло проверку функцией is_string, вернулось значение по умолчанию
 *
 * arget($arr, ['key','sub_key','int_key'], 'default',  'is_array') return 'default' - значение 12213 не прошло проверку функцией is_array
 * arget($arr, ['key','sub_key','no_exists_key_vrv34g'], 'default') return 'default' - ячейки не существует, вернулось значение по умолчанию
 *
 * arget($arr, ['key','sub_key','empty_key'], 'default') return 0 - ячейка есть, вернулось значение
 * arget($arr, ['key','sub_key','empty_key'], 'default',  'empty') return 'default' - значение не прошло проверку функцией
 *
 * arget($arr, ['key','sub_key','int_key'], 'default') return 12213 - ячейка есть, вернулось значение
 * arget($arr, ['key','sub_key','int_key'], 'default',  'is_array') return 'default' - значение 12213 не прошло проверку функцией is_array
 *
 *
 * @param mixed        $array массив из которого нужно получить вложенную ячейку
 * @param array|string $key ключи пути к ячейке или ключ ячейки в виде строки ['key','sub_key','sub_sub_key']
 * @param null         $default Значение по умолчанию при отсутствии нужной ячейки или не проходе проверки функцией
 * @param bool         $func - функция проверки с которой бедет сравнение правильности полученной чейки, например empty()
 *
 * @return mixed|null
 */
function array_get($array, $key, $default = null, $func = false) {
    if(func_num_args() > 4) {
        $keys = func_get_args();
        $func = array_pop($keys);
        $default = array_pop($keys);
        $array = array_shift($keys);
    } else {
        $keys = is_array($key) ? $key : (array)$key;
    }

    foreach($keys as $segment) {
        if(is_object($array)) {
            if(!$array instanceof ArrayAccess || !$array->offsetExists($segment)) {
                return $default;
            }
        } elseif(!is_array($array) || (is_array($array) && !array_key_exists($segment, $array))) {
            return $default;
        }
        $array = $array[$segment];
    }
    if($func) {
        $array = check_value($array, $func, $default);
    }

    return $array;
}

/**
 * проверяет значение функцией, если не проходит возвращает значение по умолчанию
 * пример:
 *  check_value('string','is_array', 'default') return 'default'
 *  check_value(['key','key2'],'is_array', 'default') return ['key','key2']
 *
 *  check_value('0','empty', 'default') return 'default'
 *  check_value('no_empty_str','empty', 'default') return 'no_empty_str'
 *
 * @param $value
 * @param $func
 * @param null $default
 *
 * @return null
 */
function check_value($value, $func, $default = null) {
    if(is_string($func) && !empty($func)) {
        switch(strtolower($func)) {
            case 'empty':
                $value = (!empty($value)) ? $value : $default;
                break;
            case 'isset':
                $value = !is_null($value) ? $value : $default;
                break;
            default:
                if(function_exists($func)) {
                    $value = (($func($value))) ? $value : $default;
                }

        }
    }
    return $value;
}

/**
 * @see ifempty()
 * @see ifset()
 * @see ifisar()
 * @param $var
 * @param null $key
 * @param null $def
 * @param string $func
 *
 * @return mixed|null
 */
function iffunc($var, $key = null, $def = null, $func = 'isset') {
    if(func_num_args() < 4) {
        $func = $def;
        $def = $key;
        return check_value($var, $func, $def);
    } elseif(func_num_args() == 4) {
        return array_get($var, $key, $def, $func);
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return array_get($var, $key, $def, $func);
    }
}

/**
 * Проверяет существование ячейки массива и сравнивает через is_null  или возвращает переданное значение по умолчанию
 * используется для проверки массивов или сорращения конструкций
 *
 * примеры:
 *
 * if(isset($_POST['value'])) {
 *     $val = $_POST['value'];
 * } else {
 *     $val = '';
 * }
 * ====== можно так:
 * $val = ifset($_POST, 'value','');
 *
 *
 * if(isset($_POST['files']) && isset(isset($_POST['files']['upload']))) {
 *     $files = $_POST['files']['upload'];
 * } else {
 *     $files = array();
 * }
 * ====== можно так:
 * $files = ifset($_POST, ['files', 'upload'], array() );
 *
 *
 * @param $var
 * @param null $key
 * @param null $def
 *
 * @return mixed|null
 */
function ifset($var, $key = null, $def = null) {
    if(func_num_args() < 3) {
        return iffunc($var, $key, 'isset');
    } elseif(func_num_args() == 3) {
        return iffunc($var, $key, $def, 'isset');
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return iffunc($var, $key, $def, 'isset');
    }
}

/**
 * Проверяет существование ячейки массива и сравнивает значение через empty() или возвращает переданное значение по умолчанию
 * используется для проверки массивов или сорращения конструкций
 *
 * примеры:
 *
 * if(isset($_POST['value']) && !empty($_POST['value'])) {
 *     $val = $_POST['value'];
 * } else {
 *     $val = 'def';
 * }
 * ====== можно так:
 * $val = ifempty($_POST, 'value','def');
 *
 *
 * if(isset($_POST['files']) && isset(isset($_POST['files']['upload']))  && !empty($_POST['files']['upload']) {
 *     $files = $_POST['files']['upload'];
 * } else {
 *     $files = array();
 * }
 * ====== можно так:
 * $files = ifempty($_POST, ['files', 'upload'], array());
 *
 * !!!!! Важно, функция не предназначена для проверки существования простых переменных, хотя проверяет и обычные на is_null
 *
 * @param $var
 * @param null $key
 * @param null $def
 *
 * @return mixed|null
 */
function ifempty($var, $key, $def = null) {
    if(func_num_args() < 3) {
        return iffunc($var, $key, 'empty');
    } elseif(func_num_args() == 3) {
        return iffunc($var, $key, $def, 'empty');
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return iffunc($var, $key, $def, 'empty');
    }
}

/**
 * Проверяет существование ячейки массива и сравнивает значение через is_array() или возвращает переданное значение по умолчанию
 * используется для проверки массивов или сорращения конструкций
 *
 * примеры:
 *
 * if(isset($_POST['value']) && is_array($_POST['value'])) {
 *     $val = $_POST['value'];
 * } else {
 *     $val = 'def';
 * }
 * ====== можно так:
 * $val = ifisar($_POST, 'value',array('1','2'));
 *
 *
 * if(isset($_POST['files']) && isset(isset($_POST['files']['upload']))  && !is_array($_POST['files']['upload']) {
 *     $files = $_POST['files']['upload'];
 * } else {
 *     $files = array('def_file'=>array('tmp_name'...));
 * }
 * ====== можно так:
 * $files = ifisar($_POST, ['files', 'upload'], array('def_file'=>array('tmp_name'...))  );
 *
 * @param $var
 * @param null $key
 * @param null $def
 *
 * @return mixed|null
 */
function ifisar($var, $key, $def = null) {
    if(func_num_args() < 3) {
        return iffunc($var, $key, 'is_array');
    } elseif(func_num_args() == 3) {
        return iffunc($var, $key, $def, 'is_array');
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return iffunc($var, $key, $def, 'is_array');
    }
}

#######################################################################################################################################
###########################################                                  ##########################################################
###########################################              TESTS               ##########################################################
###########################################                                  ##########################################################
#######################################################################################################################################


$array = [
    'text' => ['value' =>'value text','class' => 'classsd'],
    'select' => [
        'title' => 'Селект',
        'value' => null,
        'options' => [
            'asdsa1' => 'asdsa 1',
            'asdsa2' => 'asdsa 2',
            'asdsa3' => 'asdsa 3',
        ]
    ],
];
$value = [
    'null' => null,
    'false' => false,
    '0' => 0,
    'true' => true,
    'string' => 'string',
    'empty array' => [],
    'array' => ['key' => 'val']
];
$tests = [
    '$value = null' => [
        'value' => $value['null'],
        'result' => [
            'ifset' =>   'default',
            'ifempty' => 'default',
            'ifisar' =>  'default',
        ]
    ],
    '$value = false' => [
        'value' => $value['false'],
        'result' => [
            'ifset' =>  $value['false'],
            'ifempty' => 'default',
            'ifisar' =>  'default',
        ]
    ],
    '$value = 0' => [
        'value' => $value['0'],
        'result' => [
            'ifset' =>  $value['0'],
            'ifempty' => 'default',
            'ifisar' =>  'default',
        ]
    ],
    '$value = true' => [
        'value' => $value['true'],
        'result' => [
            'ifset' =>  $value['true'],
            'ifempty' => $value['true'],
            'ifisar' =>  'default',
        ]

    ],
    '$value = string' => [
        'value' => $value['string'],
        'result' => [
            'ifset' =>  $value['string'],
            'ifempty' => $value['string'],
            'ifisar' =>  'default',
        ]
    ],
    '$value = empty array' => [
        'value' => $value['empty array'],
        'result' => [
            'ifset' => $value['empty array'],
            'ifempty' => 'default',
            'ifisar' =>  $value['empty array'],
        ]
    ],
    '$value = array' => [
        'value' => $value['array'],
        'result' => [
            'ifset' =>  $value['array'],
            'ifempty' => $value['array'],
            'ifisar' =>   $value['array'],
        ]
    ],
    '$array key = erwe, no exists key return default value' => [
        'value' => $array,
        'keys' => 'erwes',
        'result' => [
            'ifset' =>   'default',
            'ifempty' => 'default',
            'ifisar' =>  'default',
        ]
    ],
    '$array  key = [text, value]' => [
        'value' => $array,
        'keys' => ['text','value'],
        'result' => [
            'ifset' =>  $array['text']['value'],
            'ifempty' => $array['text']['value'],
            'ifisar' =>  'default',
        ]
    ],
    '$array key = [select, value]' => [
        'value' => $array,
        'keys' => ['select','value'],
        'result' => [
            'ifset' =>  'default',
            'ifempty' => 'default',
            'ifisar' =>  'default',
        ]
    ],
];
echo "<h2>ifset</h2>\n";

foreach ($tests as $name => $test) {
    foreach ($test['result'] as $func => $return) {
        echo "<strong>$func</strong> $name<br>\n";
        echo "<strong>RESULT:</strong><br>\n";

        if(array_key_exists('keys', $test)) {
            $result = $func($test['value'], $test['keys'], 'default');
        } else {
            $result = $func($test['value'],  'default');
        }
        var_dump($result);
        if($result===$return) {
            echo '<span style="color: #00cc00;">OK</span>';
        } else {
            echo '<span style="color: #cc0000;">Fail</span>';
        }
        echo "<br><br>\n\n";
    }

}



