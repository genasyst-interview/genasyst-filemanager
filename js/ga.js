if (typeof($) == "undefined") {
    $ = {};
}
$.ajax = function (data) {
    var jxnr = {
        url: location.href,
        method: "GET",
        _success: true,
        _error: true,
        async: true,
        data: {},
        dataType: 'text',
        result: false,
        'headers': {},
        init: function (data) {
            var d = ['url', 'method', 'headers', 'success', 'async', 'dataType', 'data', 'error'];
            for (var k in d) {
                var v = d[k];
                if (this.hasOwnProperty(v) && data.hasOwnProperty(v)) {
                    if (typeof this[v] === 'function') {
                        this[v](data[v]);
                    } else {
                        this[v] = data[v];
                    }
                }
            }
            this.request.init();
            var l = ga.d.createElement('div');
            l.setAttribute("id", "load_bar");
            l.innerHTML = '<span id="load_bar_fill"></span><span class="circle_loader"></span>';
            ga.d.body.appendChild(l);
            var prog = function (p, req) {
                if (this.perc == null) {
                    this.perc = 8;
                    this.g = 30;
                }

                function formatSize(length) {
                    var i = 0, type = ['b', 'kb', 'Mb', 'Gb'];
                    while ((length / 1000 | 0) && i < type.length - 1) {
                        length /= 1024;
                        i++;
                    }
                    return length.toFixed(2) + ' ' + type[i];
                }

                var b = ga.d.getElementById('load_bar_fill');
                if (p) {
                    if (this.g > 50) {
                        this.perc = 1;
                    }
                    if (this.g > 60) {
                        this.perc = 0.5;
                    }
                    this.g = (this.g + this.perc);
                    if (this.perc > 1) {
                        this.perc -= 1;
                    }
                    b.style = 'width: ' + (this.g > 94 ? 94 : this.g) + '%;background-color:#ff8f00;';
                    b.innerHTML = formatSize(p.loaded) + ' ';
                }
                if (req) {
                    if (req.readyState == 1) {
                        b.style = 'width: 10%;background-color:#f00;';
                    } else if (req.readyState == 2) {
                        b.style = 'width: 20%;background-color:#ff8f00;';
                    }
                    /*else if(req.readyState == 3) {
                                         //s b.style = 'width: 40%;background-color:#ff8f00;';
                                      }else if(req.readyState ==  4) {
                                       //  b.style = 'width: 100%;background-color:#cc8f00;';
                                      }*/
                }
            };

            this.request.progress(prog);
            this.request.prepare(this.method.toString().toUpperCase(), this.url);
            /*add headers */
            var h = this.headers;
            if (typeof  h === 'object' /*&& this.headers.length>0*/) {
                for (var k in h) {
                    this.request.header(k, h[k]);
                }
            }
            this.request.send(this.getParams(), this);
        },
        onload: function (data) {
            try {
                if (this.dataType == 'json') {
                    data = JSON.parse(data);
                }
                if (typeof this._success === 'function') {
                    this._success(data);
                } else {
                    this.result = data;
                }
            } catch (e) {
                console.log(e);
            }
        },
        onerror: function () {
            if (typeof this._error === 'function') {
                this._error();
            }
        },
        getParams: function () {
            if (typeof this.data === 'string') {
                return this.data;
            }
            var params = '';
            if (typeof this.data === 'object') {
                var i = 0;
                for (var k in this.data) {/*todo: k need encode? bug)*/
                    params += ((i == 0 ? '' : '&') + k + '=' + encodeURIComponent(this.data[k]));
                    i++;
                }
            }
            return params;
        },
        success: function (func) {
            if (typeof func === 'function') {
                this._success = func;
            }
            if (this.result) {
                this._success(this.result);
            }
        },
        request: {
            req: null,
            result: false,
            obj: {},
            init: function () {
                if (window.XMLHttpRequest) {
                    this.req = new XMLHttpRequest();
                } else if (window.ActiveXObject) {
                    this.req = new ActiveXObject('Microsoft.XMLHTTP');
                }
            },
            prepare: function (type, url) {
                if (!this.req) {
                    this.init();
                }
                var self = this;
                if (this.req) {
                    this.req.onreadystatechange = function () {
                        if (self.req.readyState === 4 && self.req.status === 200) {
                            var b = document.getElementById('load_bar_fill');
                            b.style = ' width: 100%; background-color:#55e844  !important;';
                            b.innerHTML = 'render';
                            try {
                                self.obj.onload(self.req.responseText);
                            } catch (e) {
                                console.log(e);
                            }

                            setTimeout(function () {
                                var d = ga.d.getElementById('load_bar');
                                if (d) {
                                    d.remove();
                                }
                            }, 50);
                        } else {
                            self.req.onprogress(null, self.req);
                        }

                    };
                    this.req.onerror = function () {
                        self.obj.onerror();
                    };
                    this.req.open(type, url, true);
                    this.req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
                }
            },
            progress: function (p) {
                this.req.onprogress = p;
            },
            header: function (k, v) {
                if (!this.req) {
                    return;
                }
                this.req.setRequestHeader(k, v);
            },
            send: function (params, obj) {
                if (!this.req) {
                    return;
                }
                console.log(params);
                this.req.send(params);
                this.obj = obj;
            }
        }

    };
    jxnr.init(data);
    return jxnr;
};
$.serialize = function (form, to_string) {
    t = to_string;
    if (!form || form.nodeName !== "FORM") {
        return;
    }
    var i, j = [], q = (t) ? [] : {};
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        var field = form.elements[i];

        if (field.name === "") {
            continue;
        }

        var value = false;
        var val = field.value;
        switch (field.nodeName) {
            case 'INPUT':
            case 'BUTTON':
            case 'SELECT':
                switch (field.type) {
                    case 'text':
                    case 'tel':
                    case 'email':
                    case 'hidden':
                    case 'password':
                    case 'button':
                    case 'reset':
                    case 'submit':
                        value = val;
                        break;
                    case 'checkbox':
                    case 'radio':
                        if (field.checked) {
                            value = val;
                        }
                        break;
                    /*SELECT*/
                    case 'select-one':
                        value = val;
                        break;
                    case 'select-multiple':
                        value = {};
                        for (j = field.options.length - 1; j >= 0; j = j - 1) {
                            if (field.options[j].selected) {/*TODO: наоборот*/
                                value[j] = field.options[j].value;
                            }
                        }
                        break;
                }
                break;
            case 'TEXTAREA':
                value = val;
                break;
            case 'file':
                break;
        }
        if (value !== false) {
            if (t) {
                q.push(field.name + "=" + encodeURIComponent(value));
            } else {
                var m = field.name.toString().match(
                    new RegExp('(.+)\\\\[([a-zA-z0-9_\.\s]*)\\\\]')
                );
                if (m !== null) {
                    /*todo: field[n][subn]????!*/
                    if (!q.hasOwnProperty(m[1])) {
                        q[m[1]] = {};
                    }
                    if (m[2] == '') {
                        m[2] = count(q[m[1]]);
                    }
                    q[m[1]][m[2]] = php.utf8_encode(value);
                } else {
                    q[field.name] = value;
                }
            }

        }
    }
    return (t) ? q.join("&") : q;
};
var php = {
    b64: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    utf8_encode: function (a) {
        a = a.replace(/\r\n/g, "\n");
        for (var b = "", c = 0; c < a.length; c++) {
            var d = a.charCodeAt(c);
            128 > d ? b += String.fromCharCode(d) : (127 < d && 2048 > d ? b += String.fromCharCode(d >> 6 | 192) : (b += String.fromCharCode(d >> 12 | 224), b += String.fromCharCode(d >> 6 & 63 | 128)), b += String.fromCharCode(d & 63 | 128))
        }
        return b
    },
    utf8_decode: function (a) {
        var b = "", c = 0;
        for (c1 = c2 = 0; c < a.length;) {
            var d = a.charCodeAt(c);
            128 > d ? (b += String.fromCharCode(d), c++) :
                191 < d && 224 > d ? (c2 = a.charCodeAt(c + 1), b += String.fromCharCode((d & 31) << 6 | c2 & 63), c += 2) : (c2 = a.charCodeAt(c + 1), c3 = a.charCodeAt(c + 2), b += String.fromCharCode((d & 15) << 12 | (c2 & 63) << 6 | c3 & 63), c += 3)
        }
        return b
    },
    array_map: function (a, b) {
        b = "object" == typeof b ? b : {};
        for (var c in b) b.hasOwnProperty(c) && (b[c] = null === b[c] ? a(b[c]) : "object" == typeof b[c] ? php.array_map(a, b[c]) : a(b[c]));
        return b
    },
    urldecode: function (a) {
        if (a = a || null) try {
            a = decodeURIComponent(a.toString()).replace(/\+/g, " ")
        } catch (b) {
        }
        return a
    },
    /*
    base64_encode: function (a) {
         var b = "", c = 0, d = php.b64;
         for (a = php.utf8_encode(a); c < a.length;) {
             var e = a.charCodeAt(c++);
             var f = a.charCodeAt(c++);
             var g = a.charCodeAt(c++);
             var h = e >> 2;
             e = (e & 3) << 4 | f >> 4;
             var k = (f & 15) << 2 | g >> 6;
             var l = g & 63;
             isNaN(f) ? k = l = 64 : isNaN(g) && (l = 64);
             b = b + d.charAt(h) + d.charAt(e) + d.charAt(k) + d.charAt(l)
         }
         return b
     },
     base64_decode: function (a) {
         var b = "", c = 0, d = php.b64;
         for (a = a.replace(/[^A-Za-z0-9\+\/=]/g, ""); c < a.length;) {
             var e = d.indexOf(a.charAt(c++));
             var f = d.indexOf(a.charAt(c++));
             var g = d.indexOf(a.charAt(c++));
             var h = d.indexOf(a.charAt(c++));
             e = e << 2 | f >> 4;
             f = (f & 15) << 4 | g >> 2;
             var k = (g & 3) << 6 | h;
             b += String.fromCharCode(e);
             64 != g && (b += String.fromCharCode(f));
             64 != h && (b += String.fromCharCode(k))
         }
         return php.utf8_decode(b)
     },
     */
    bin2hex: function (a) {
        a = php.utf8_encode(a) + "";
        var b, c = a.length, d = "";
        for (b = 0; b < c; b++) {
            var e = a.charCodeAt(b).toString(16);
            d += 2 > e.length ? "0" + e : e;
        }
        return d
    },
    hex2bin: function (a) {
        var b = [], c = a.length;
        a += "";
        var r = '';
        for (i = 0; i < c; i += 2) {
            var d = parseInt(a.substr(i, 1), 16);
            var e = parseInt(a.substr(i + 1, 1), 16);
            if (isNaN(d) || isNaN(e)) return !1;
            r += String.fromCharCode(d << 4 | e);
        }
        return php.utf8_decode(r);
    }
};

function count(mixed_var, mode) {
    var key, cnt = 0;
    if (mode == 'COUNT_RECURSIVE') mode = 1;
    if (mode != 1) mode = 0;
    for (key in mixed_var) {
        cnt++;
        if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
            cnt += count(mixed_var[key], 1);
        }
    }
    return cnt;
}

var ga = {
    data: {},
    setData: function (c) {
        for (var k in c) {
            if (this.hasOwnProperty(k) && this[k].hasOwnProperty('setData')) {
                this[k].setData(c[k]);
            } else {
                this.data[k] = c[k];
            }
        }
    },
    crypt: {
        data: {
            marker: '__ee__',
            key: '',
            encode: function (data, key) {
                return data
            },
            decode: function (data, key) {
                return data
            }
        },
        isEncode: function (data) {
            return (data.toString().match(new RegExp('^' + this.data.marker + '')) !== null);
        },
        encode: function (data) {
            if (!this.isEncode(data)) {
                data = this.data.marker + '' + this.data.encode(data, this.data.key);
            }
            return data;
        },

        decode: function (data) {
            if (this.isEncode(data)) {
                data = this.data.decode(data.replace(new RegExp('^' + this.data.marker + ''), ''), this.data.key);
            }
            return data;
        },
        form: function (f, t) {
            t = t || false;
            self = this;
            var els = f.elements, el, val, ov;
            if (ga.cookie.get('system_crypt_form') && t) {
                for (var k in els) {
                    if (els.hasOwnProperty(k)) {
                        el = els[k];
                        val = el.value;
                        if (el.nodeName === 'SELECT') {
                            for (j = el.options.length - 1; j >= 0; j = j - 1) {
                                ov = el.options[j].value;
                                if (ov !== '' && !self.isEncode(ov)) {
                                    el.options[j].value = _e(ov);
                                }
                            }
                        }
                        if (val !== '' && !self.isEncode(val)) {
                            el.value = _e(val);
                        }
                    }
                }
            }
            if (!t) {
                /*декодируем без проверок */
                for (var k in els) {
                    if (els.hasOwnProperty(k)) {
                        el = els[k];
                        val = el.value;
                        if (el.nodeName === 'SELECT') {
                            for (j = el.options.length - 1; j >= 0; j = j - 1) {
                                ov = el.options[j].value;
                                if (self.isEncode(ov)) {
                                    el.options[j].value = _d(ov);
                                }
                            }
                        }
                        if (self.isEncode(val)) {
                            el.value = _d(val);
                        }
                    }
                }
            }
        }
    },
    /* getter: function (o) {
         o = typeof (o) == 'object' ? o : {};
         return {
             'd': o, g: function (k) {
                 return this.d.hasOwnProperty(k) ? this.d[k] : k;
             }
         }
     },*/
    request: {
        data: {},
        setData: function (d) {
            d = typeof d === 'object' ? d : {};
            this.data = php.array_map(php.urldecode, d);
        },
        keys: ['Module', 'Action', 'Directory', 'param1', 'param2', 'param3', 'Charset'],
        setParam: function (n, value) {
            if (this.keys.hasOwnProperty(n)) {
                var name = this.keys[n];
                if (value == null) {
                    value = this.data[name];
                }
                ga.d.gaf[name].value = value;
            }
        },
        form: function (form, m, a, d, p1, p2, p3, c) {

            var aj = false;
            if (form.hasOwnProperty('ajax')) {
                aj = form['ajax'];
            } else if (form.hasOwnProperty(_e('ajax'))) {
                aj = form[_e('ajax')];
            }

            var func = this.__prepareArgs(arguments, 1);

            if (aj && ((aj.type === 'checkbox' && aj.checked) || (aj.type === 'hidden' && aj.value == '1'))) {/*TODO: Надо сделать проверку что есть, и без шафрования проверять */
                var i = ga.d.createElement('input');
                i.type = 'hidden';
                i.name = 'ajax';
                i.value = '1';
                ga.d.gaf.appendChild(i);
                this.submit(ga.d.forms['gaf'], func);
                ga.d.gaf['ajax'].remove();
            } else {
                this.submit(ga.d.forms['gaf']);
            }
            return false;
        },
        post: function (m, a, d, f, p1, p2, p3, c) {
            this.__prepareArgs(arguments);
            ga.d.forms['gaf'].submit();

        },
        apost: function (m, a, d, f, p1, p2, p3, c) {
            this.__prepareArgs(arguments);
            this.submit(ga.d.forms['gaf']);
        },
        __prepareArgs: function (args, form) {
            var f = null;
            if (args.length > 0) {
                var l = args.length;
                f = args[(l - 1)];
                if (typeof f === 'function') {
                    l = l - 1;
                }
                for (var i = 0; i < l; i++) {
                    if (form && i == 0) {
                        continue;
                    }
                    var arg = args[i];
                    this.setParam(form ? (i - 1) : i, php.urldecode(arg));
                }
            }
            ga.d.forms['gaf'].target = this.data['_target'];
            ga.d.forms['gaf'].action = this.data['_action'];

            return f;
        },

        submit: function (form, func, force) {
            if ((typeof form.onsubmit === 'function' && !func && !force) || form.enctype === 'multipart/form-data') {
                return true;
            }
            var j = _e(JSON.stringify($.serialize(form)));
            if ((j.length < 3000 && ga.cookie.get('system_header_request'))) {
                type = 'html';
                if (typeof(func) !== 'function') {
                    func = function (r) {
                        window.document.open("text/html", "replace");
                        window.document.write(r);  // htmlCode is the variable you called newDocument
                        window.document.close();
                    };
                } else {
                    var type = 'json';
                }
                $.ajax({
                    url: this.data['_action'],
                    headers: {'Authorization': j},
                    success: func,
                    dataType: type
                });
            } else if (func) {

                ga.crypt.form(form, true);
                var fg = $.serialize(form, 1);
                $.ajax({
                    method: 'post',
                    url: this.data['_action'],
                    success: func,
                    data: fg,
                    dataType: 'json'
                });
                ga.crypt.form(form);
            }else{
                ga.crypt.form(form, true);
                form.submit();
                setTimeout(function () {
                    ga.crypt.form(form);
                }, 10);
            }
            return false;
        }
    },
    cookie: {
        get: function (name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : null;
        },
        set: function (name, value, props) {
            props = props || {};
            var exp = props.expires;
            if (typeof exp == "number" && exp) {
                var d = new Date();
                d.setTime(d.getTime() + exp * 10000);
                exp = props.expires = d;
            }
            if (exp && exp.toUTCString) {
                props.expires = exp.toUTCString()
            }
            value = encodeURIComponent(value);
            var updatedCookie = name + "=" + value;
            for (var propName in props) {
                updatedCookie += "; " + propName;
                var propValue = props[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue
                }
            }
            document.cookie = updatedCookie;

        },
        delete: function (name) {
            this.set(name, null, {expires: -1});
        }
    },

    d: document

};
_e = function (s) {
    return ga.crypt.encode(s);
};
_d = function (s) {
    return ga.crypt.decode(s);
};
document.addEventListener('DOMContentLoaded', function () {
    window.document.body.addEventListener('submit', function (event) {
        ga.request.submit(event.target);
    }, false);
});

function p(Module, Action, Directory, File, param1, param2, param3, Charset) {
    ga.request.post(Module, Action, Directory, File, param1, param2, param3, Charset);
}

function a(Module, Action, Directory, File, param1, param2, param3, Charset) {
    ga.request.apost(Module, Action, Directory, File, param1, param2, param3, Charset);
}


// EOF