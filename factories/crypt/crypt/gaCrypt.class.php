<?php

class gaCrypt
{
    protected $key = 'fa634b3d300d22d6cb9f16cfe964cd';
    protected $marker = '__ee__';
    protected $methods = [];
    protected $saved = [];

    public function __construct($data = null)
    {

        $this->key = ifempty($data, 'key', $this->key);
        if (arkex(['encode', 'decode', 'js_encode', 'js_decode'], ifset($data, 'methods', false))) {
            $this->methods = $data['methods'];
        } else {
            $this->methods = [
                'encode'    => function ($data, $key) {
                    $key = bin2hex($key);
                    $result = '';
                    $data = bin2hex($data);
                    $i = 0;
                    while ($i < strlen($data)) {
                        for ($j = 0; $j < strlen($key); $j++) {
                            $enc_chr = chr(ord($data[$i]) ^ ord($key[$j]));
                            $result .= $enc_chr;
                            $i++;
                            if ($i >= strlen($data)) break;
                        }
                    }

                    return bin2hex($result);
                },
                'decode'    => function ($data, $key) {
                    $key = bin2hex($key);
                    $data = hex2bin($data);
                    $result = '';
                    $i = 0;
                    while ($i < strlen($data)) {
                        for ($j = 0; $j < strlen($key); $j++) {
                            $enc_chr = chr(ord($data[$i]) ^ ord($key[$j]));
                            $result .= $enc_chr;
                            $i++;
                            if ($i >= strlen($data)) break;
                        }
                    }

                    return hex2bin($result);
                },
                'js_encode' => ' function (data, key) {
                    if (key == null || key.length <= 0) {
                        return null;
                    }
                    key = php.bin2hex(key);
                    data = php.bin2hex(data);
                    var enc_str = \'\';
                    var i = 0;
                    while (i < data.length) {
                        for (var j = 0; j < key.length; j++) {
                            var enc_chr = data.charCodeAt(i) ^ key.charCodeAt(j);
                            enc_str += String.fromCharCode(enc_chr);
                            i++;
                            if (i >= data.length) break;
                        }
                    }
                    return php.bin2hex(enc_str);
                }
                ',
                'js_decode' => '  function (data, key) {
                    if (key == null || key.length <= 0) {
                        return null;
                    }
                    key = php.bin2hex(key);
                    data =php.hex2bin(data);
                    var enc_str = \'\',i = 0;
                    while (i < data.length) {
                        for (var j = 0; j < key.length; j++) {
                            var enc_chr = data.charCodeAt(i) ^ key.charCodeAt(j);
                            enc_str += String.fromCharCode(enc_chr);
                            i++;
                            if (i >= data.length) break;
                        }
                    }
                    return php.hex2bin(enc_str);
                }
                ',
            ];
        }
    }

    public function key()
    {
        return $this->key;
    }

    public function marker()
    {
        return $this->marker;
    }

    public function js()
    {
        $h = new gaHelper();

        return '
       ga.crypt.data = {
           key:' . $h->jsParam($this->key) . ',
           marker:' . $h->jsParam($this->marker) . ',
           encode : ' . $this->methods['js_encode'] . ',
           decode : ' . $this->methods['js_decode'] . '
       };
        ';
    }

    public function encode($data)
    {
        if (!$this->isEncode($data)) {
            $e = $this->marker . $this->methods['encode']($data, $this->key);

            return $e;
        }

        return $data;

    }

    public function decode($data)
    {
        if ($this->isEncode($data)) {
            return $this->methods['decode'](mb_substr($data, mb_strlen($this->marker)), $this->key);
        }

        return $data;
    }

    public function isEncode($data)
    {
        return (@mb_substr($data, 0, mb_strlen($this->marker)) == $this->marker);
    }

    public function decodeArrayKeys(&$data, $recursive = false)
    {
        $decoded = [];
        if (isar($data)) {
            foreach ($data as $k => $v) {
                $decoded[$this->decode($k)] = ($recursive && isar($v)) ? $this->decodeArrayKeys($v, $recursive) : $v;
            }
        }
        $data = $decoded;

        return $data;
    }

    public function decodeArray(&$data, $recursive = false)
    {
        $decoded = [];
        if (isar($data)) {
            foreach ($data as $k => $v) {
                $decoded[$this->decode($k)] = ($recursive && isar($v)) ? $this->decodeArray($v, $recursive) : $this->decode($v);
            }
        }
        $data = $decoded;

        return $data;
    }
}