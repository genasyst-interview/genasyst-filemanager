<?php

class gaFilesystemPhp extends gaFilesystemAbstract implements gaFilesystemInterface
{
    /**
     * @see gaSingletonTrait::$_instance
     */
    protected static $_instance;

    /**
     * Возвращает нумерованный список файлов директории или false
     * [
     *    0 => '..',
     *    1 => 'file.php',
     *    2 => 'link.php',
     *    ....
     * ] : false
     *
     * @param string $dir_path - Полный путь к директории от корня сервера
     *
     * @return array|false
     */
    public function listDir($dir_path = '')
    {
        $files = [];
        if (!funex("scandir")) {
            $h = @opendir($dir_path);
            while (false !== ($filename = @readdir($h))) {
                $files [] = $filename;
            }
        } else {
            $files = @scandir($dir_path);
        }

        return $files;
    }

    /**
     * Возвращает информацию о файлах директории в нумерованном массиве или false
     * [
     *    0 => ['name'=>'..','type'=> 'dir'...],
     *    1 => ['name'=>'file.php','type'=> 'file'...],
     *    2 => ['name'=>'link.php','type'=> 'link','link'=>'/home/d/file.php'...]
     * ] : false
     * Массив данных файла - @see gaFilesystemAbstract::$finfo
     *
     * @param string $dir_path - Полный путь к директории от корня сервера
     *
     * @return array|false
     */
    public function getDirFiles($dir_path = '', $sort = null)
    {
        $this->setSort($sort);
        $dir_path = $this->setDir($dir_path);
        $dirs = $files = [];
        foreach (ifempty($this->listDir($dir_path), []) as $filename) {
            if ($filename != '.' && !empty($filename)) {
                $info = $this->fileinfo($dir_path, $filename);
                if ($info['type'] == 'dir') {
                    $dirs [] = $info;
                } else {
                    $files[] = $info;
                }
            }
        }

        return array_merge($this->sort($dirs), $this->sort($files));
    }

    /**
     * Создает директорию по указанному пути
     *
     * @param string $dir_path - Полный путь к директории от корня сервера
     *
     * Возвращает адрес созданной директории или false
     *
     * @return false|string
     */
    public function createDir($dir_path = '')
    {
        if (fiex($dir_path)) {
            chmod($dir_path, 0750);

            return $dir_path;
        }

        return (mkdir($dir_path, 0750, true)) ? $dir_path : false;
    }

    /**
     * Проверяет права на запись файла или директории
     *
     * @param $filepath (array созданный в fileinfo) или - Полный путь от корня сервера к директории или файлу
     *
     * @return bool
     */
    public function isWriteable($filepath)
    {
        return arkex('writeable', $filepath) ? $filepath['writeable']
            : iswritable((string)arget($filepath, 'path', $filepath));
    }

    /**
     * @param $filepath
     *
     * @return bool
     */
    public function isReadable($filepath)
    {
        return arkex('readable', $filepath) ? $filepath['readable']
            : isreadable((string)arget($filepath, 'path', $filepath));
    }

    /**
     * @param string $file_dir
     * @param null   $filename
     *
     * @return array|string
     */
    public function fileinfo($file_dir = '', $filename = null)
    {
        if (arkex(['path', 'name', 'type'], $file_dir)) {
            return $file_dir;
        }
        if ($filename === null) {
            $filename = basename($file_dir);
            $dir = $this->cast(dirname($file_dir));
        } else {
            $dir = $this->cast($file_dir);
        }
        $file_dir = in_array(trim((string)$filename), ['..', '.']) ? $this->cast($dir . $filename)
            : $this->cast($dir) . $filename;

        $info = $this->finfo;
        $perms = @fileperms($file_dir);
        $uid = funex('posix_getpwuid') ? @posix_getpwuid(@fileowner($file_dir)) : [];
        $info = array_merge($info, [
            'path'      => $file_dir,
            'name'      => $filename,
            'modify'    => @filemtime($file_dir),
            'perms'     => $perms,
            'size'      => @filesize($file_dir),
            'owner'     => ifset($uid, 'uid', @fileowner($file_dir)),
            'group'     => ifset($uid, 'gid', @filegroup($file_dir)),
            'str_perms' => $this->permsFormat($perms, 'base', 'str'),
            'oct_perms' => $this->permsFormat($perms, 'base', 'oct'),
            'writeable' => $this->isWriteable($file_dir),
            'readable'  => $this->isReadable($file_dir),
        ]);
        if (isdir($file_dir) || in_array(basename($file_dir), ['..', '.'])) {
            $info['type'] = 'dir';
        } elseif (@is_link($file_dir)) {
            $info['type'] = 'link';
            $info['link'] = readlink($file_dir);
        }

        return $info;
    }

    /**
     * @param $filepath
     */
    public function downloadFile($filepath)
    {
        if (ob_get_level()) {
            ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($filepath));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        ga()->sendHeaders();
        $this->printFile($filepath);
        exit;
    }

    /**
     * @param string $archive_save_dir
     * @param string $archive_name
     * @param string $files_root_dir
     * @param array  $files
     */
    public function createZip($archive_save_dir = '', $archive_name = '', $files_root_dir = '', $files = [])
    {
        if (class_exists('ZipArchive')) {
            $z = new ZipArchive();
            $archive_name .= (substr($archive_name, -4) == '.zip') ? '' : '.zip';
            if (($op = $z->open($archive_save_dir . $archive_name, ZipArchive::CREATE)) === true) {
                chdir($files_root_dir);
                $this->unsetIgnore($files);
                foreach ($files as $f) {
                    if (is_file($files_root_dir . $f)) {
                        $z->addFile($f, str_replace($files_root_dir, '', $f));
                    } elseif (isdir($files_root_dir . $f)) {
                        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($f));
                        foreach ($iterator as $k => $v) {
                            if (isdir($k)) {
                                $z->addEmptyDir(str_replace($files_root_dir, '', $k));
                            } else {
                                $z->addFile(realpath($k), str_replace($files_root_dir, '', $k));
                            }
                        }
                    }
                }
                $z->close();
            }
        } else {
            ga()->log("Zip Archive not support!");
        }
    }


    /**
     * @param string $extract_to_dir
     * @param string $archives_root_dir
     * @param array  $archive_files
     */
    public function unZip($extract_to_dir = '', $archives_root_dir = '', $archive_files = [])
    {
        $extract_to_dir = $this->cast($extract_to_dir);
        if (class_exists('ZipArchive') && !empty($extract_to_dir)) {
            $archives_root_dir = $this->cast($archives_root_dir);
            $zip = new ZipArchive();
            foreach (ifisar($archive_files, []) as $archive) {
                if ($zip->open($archives_root_dir . $archive)) {
                    $zip->extractTo($extract_to_dir);
                    $zip->close();
                }
            }
        }
    }

    /**
     * @param string $archive_save_dir
     * @param        $archive_name
     * @param string $files_root_dir
     * @param array  $files
     *
     * @return mixed|null
     */
    public function createTar($archive_save_dir = '', $archive_name, $files_root_dir = '', $files = [])
    {
        chdir($files_root_dir);

        return ga()->ex('tar cfzv ' . $archive_save_dir . escapeshellarg($archive_name) . ' ' . implode(' ', array_map('escapeshellarg', $files)));
    }

    /**
     * @param string $temp_path
     * @param string $save_to_dir
     * @param string $filename
     *
     * @return bool|string
     */
    public function moveUploadFile($temp_path = '', $save_to_dir = '', $filename = '')
    {
        $filepath = $this->cast($save_to_dir) . $filename;

        return move_uploaded_file($temp_path, $filepath) ? $filepath : false;
    }

    /**
     * @return array
     */
    public function uploadFiles()
    {
        $r = [];
        foreach ($_FILES as $key => $files) {
            $save_dir = ga('r', $key . '_patch');
            if (!isar($files['error'])) {
                array_walk($files, function (&$v) {
                    $v = (array)$v;
                });
                $save_dir = (array)$save_dir;
            }
            foreach ($files['error'] as $file => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $file_dir = ifempty($save_dir, $file, $this->getDir());
                    $this->createDir($file_dir);
                    $r[] = $this->moveUploadFile($files['tmp_name'][$file], $file_dir, $files['tmp_name'][$file]);
                } else {
                    ga()->log($this->uploadError($error));
                }
            }
        }

        return $r;
    }

    /**
     * @param string $filepath
     * @param string $time
     *
     * @return bool
     */
    public function touch($filepath = '', $time = '')
    {
        return touch($filepath, $time, $time);
    }

    /**
     * @param      $dir_from
     * @param      $dir_to
     * @param      $filename
     * @param bool $del
     *
     * @return bool
     */
    public function copy($dir_from, $dir_to, $filename, $del = false)
    {
        $dir_from = $this->cast($dir_from);
        $dir_to = $this->cast($dir_to);
        $r = true;
        if (isdir($this->cast($dir_from . $filename))) {
            $path_from = $this->cast($dir_from . $filename);
            $path_to = $this->cast($dir_to . $filename);
            if (!fiex($path_to)) {
                if (!($r = mkdir($path_to, 0750, true))) {
                    ga()->log("Can't create new dir: " . $path_to . '!');
                }
            }
            foreach ($this->listDir($path_from) as $file) {
                if (($file != ".") and ($file != "..")) {
                    if (!($r = $this->copy($path_from, $path_to, $file, $del))) {
                        ga()->log("Can't create new path: " . $path_to . '!');
                    }
                }
            }
            if ($r && $del) {
                if (!($r = rmdir($path_from))) {
                    ga()->log("Can't delete old path: " . $path_from . '!');
                }
            }
        } elseif (fiex($dir_from . $filename)) {
            if (($r = copy($dir_from . $filename, $dir_to . $filename)) && $del) {
                if (!($r = unlink($dir_from . $filename))) {
                    ga()->log("Can't delete old file: " . $dir_from . $filename . '!');
                }
            }
        } else {
            ga()->log("Old file path not exists: " . $dir_from . $filename . '!');
        }

        return $r;
    }

    /**
     * @param $dir_from
     * @param $dir_to
     * @param $filename
     *
     * @return bool
     */
    public function move($dir_from, $dir_to, $filename)
    {
        return $this->copy($dir_from, $dir_to, $filename, true);
    }

    /**
     * @param string $filepath
     *
     * @return bool
     */
    public function delete($filepath = '')
    {
        $result = true;
        if (isdir($filepath)) {
            $filepath = $this->cast($filepath);
            $h = opendir($filepath);
            while (($file = readdir($h)) !== false) {
                if (($file == "..") || ($file == ".")) {
                    continue;
                } else {
                    $result = $this->delete($filepath . $file);
                }
            }
            closedir($h);
            if ($result) {
                if (($result = @rmdir($filepath)) == false) {
                    ga()->log("Can't delete base dir: " . $filepath . '!');
                }
            } else {
                ga()->log("Can't delete  dir files: " . $filepath . '!');
            }
        } elseif (fiex($filepath)) {
            if (($result = @unlink($filepath)) == false) {
                ga()->log("Can't delete file: " . $filepath . '!');
            }
        } else {
            ga()->log("File path not exists: " . $filepath . '!');
        }

        return $result;
    }

    /**
     * @param $filepath
     *
     * @return bool|string
     */
    public function getFile($filepath)
    {
        $r = '';
        if (($f = @fopen($filepath, 'r'))) {
            while (!@feof($f)) {
                $r .= @fread($f, 1024);
            }
            @fclose($f);

            return $r;
        }

        return false;

    }

    /**
     * @param $filepath
     *
     * @return bool
     */
    public function printFile($filepath)
    {
        if (($f = @fopen($filepath, 'r'))) {
            while (!@feof($f)) {
                echo @fread($f, 1024);
            }
            @fclose($f);

            return true;
        }

        return false;
    }

    /**
     * @param $filepath
     * @param $content
     *
     * @return bool|int
     */
    public function saveFile($filepath, $content)
    {
        $t = fiex($filepath) ? filemtime($filepath) : time();
        $r = false;
        if (($f = @fopen($filepath, "w"))) {
            $r = fwrite($f, $content);
            fclose($f);
        }
        @touch($filepath, $t, $t);

        return $r;
    }

    public function rename($old_path, $new_path)
    {
        return @rename($old_path, $new_path);
    }

    /**
     * @param        $file_data
     * @param string $format
     *
     * @return bool|float|int|string
     */
    public function getPerms($file_data, $format = 'oct')
    {
        if (arkex($format . '_perms', $file_data)) {
            return $file_data[$format . '_perms'];
        }
        if (!isar($file_data)) {
            $file_data = $this->fileinfo($file_data);
        }

        return $this->permsFormat(ifset($file_data, 'perms', 0), 'base', $format);
    }

    /**
     * @param        $filepath
     * @param string $oct_perms
     *
     * @return bool
     */
    public function setPerms($filepath, $oct_perms = '')
    {
        if (!empty($oct_perms)) {
            $pm = 0;
            for ($i = strlen($oct_perms) - 1; $i >= 0; --$i) {
                $pm += (int)$oct_perms[$i] * pow(8, (strlen($oct_perms) - $i - 1));
            }

            return chmod($filepath, $pm);
        }

        return false;
    }
}