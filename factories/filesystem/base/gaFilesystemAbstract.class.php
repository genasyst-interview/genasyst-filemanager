<?php

abstract class gaFilesystemAbstract extends gaStorage implements gaFilesystemInterface
{
    use gaSingletonTrait;

    /**
     * Ключ и тип сортировки файлов
     * @var array
     */
    protected $sort = ['name', 1];

    /**
     * Базовый массив данных файла
     * @var array
     */
    protected $finfo = [
        'path'      => '',
        'name'      => '',
        'size'      => 0,
        'type'      => 'file',
        'link'      => 'file',
        'owner'     => 0,
        'group'     => 0,
        'modify'    => 0,
        'perms'     => 0,
        'str_perms' => 'u---------',
        'oct_perms' => '0000',
        'writeable' => false,
        'readable'  => false,
    ];


    /**
     * Список кодов ошибок при загрузке файлов
     * @var array
     */
    /* protected $upload_error = [
       1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
       2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
       3 => 'The uploaded file was only partially uploaded',
       4 => 'No file was uploaded',
       6 => 'Missing a temporary folder',
       7 => 'Failed to write file to disk',
       8 => 'File upload stopped by extension'
   ];*/
    protected $upload_error = [
        1 => 'Размер файла больше разрешенного директивой upload_max_filesize в php.ini',
        2 => 'Размер файла превышает указанное значение в MAX_FILE_SIZE',
        3 => 'Файл был загружен только частично',
        4 => 'Не был выбран файл для загрузки',
        6 => 'Не найдена папка для временных файлов',
        7 => 'Ошибка записи файла на диск',
        8 => 'PHP-расширение остановило загрузку файла',
    ];

    /**
     * @param null $data
     */
    protected function init($data = null)
    {
    }

    /**
     * @see gaFilesystemInterface::getDir()
     * @return string
     */
    public function getDir()
    {
        return $this->cast($this->get('Directory'));
    }

    /**
     * @see gaFilesystemInterface::getDomainDir()
     * @return string
     */
    public function getDomainDir()
    {
        return $this->cast(realpath($_SERVER['DOCUMENT_ROOT']));
    }

    /**
     * @see gaFilesystemInterface::getHomeDir()
     * @return string
     */
    public function getHomeDir()
    {
        return $this->cast(dirname($_SERVER['SCRIPT_FILENAME']));
    }

    /**
     * @see gaFilesystemInterface::setDir()
     *
     * @param string $path
     * @param bool   $config
     *
     * @return string
     */
    public function setDir($path = '', $config = false)
    {
        $path = trim($path);
        if (!empty($path)) {
            $path = $this->cast($path);
        }
        $path = $this->cast((isdir($path)) ? realpath($path) : $this->cast($path));
        $this->set('Directory', $path);
        @chdir($path);
        if ($config) {
            ga()->config()->set('Directory', $path);
        }

        return $path;
    }

    /**
     * @see gaFilesystemInterface::sort()
     *
     * @param array $files
     *
     * @return array
     */
    public function sort(&$files)
    {
        $s = $this->sort;

        $func = function ($a, $b) use ($s) {
            if ($s[0] == 'oct_perms') {
                $a[$s[0]] = substr($a[$s[0]], 1);
                $b[$s[0]] = substr($b[$s[0]], 1);
            }

            return ($s[0] != 'size' && arkex($s[0], $a)) ?
                strcmp(strtolower($a[$s[0]]), strtolower($b[$s[0]])) * ($s[1] ? 1 : -1)
                : (($a['size'] < $b['size']) * ($s[1] ? -1 : 1));
        };
        usort($files, $func);

        return $files;
    }

    /**
     * @see gaFilesystemInterface::setSort()
     *
     * @param array $sort
     *
     * @return void
     */
    public function setSort($sort = [0 => 'name', 1 => 1])
    {
        if (arkex([0, 1], $sort)) {
            $this->sort = $sort;
        }
    }

    public function downloadFile($path)
    {
        if (ob_get_level()) {
            ob_end_clean();
        }
        ga()->config()->set('Action', '', true);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($path));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        ga()->sendHeaders();
        $this->printFile(func_get_args());

        exit;
    }


    /**
     * @see gaFilesystemInterface::uploadFiles()
     * @return array
     */
    public function uploadFiles()
    {
        $return = [];
        foreach ($_FILES as $k => $v) {
            $paths = ga('r', $k . '_patch');
            if (is_array($_FILES[$k]['error'])) {
                foreach ($_FILES[$k]['error'] as $kf => $vf) {
                    if ($vf == UPLOAD_ERR_OK) {
                        $dir = ifempty($paths, $kf, $this->getDir());
                        $this->createDir($dir);
                        $return[] = $this->moveUploadFile($_FILES[$k]['tmp_name'][$kf], $dir, $_FILES[$k]['name'][$kf]);
                    } else {
                        ga()->log($this->uploadError($vf));
                    }
                }
            } else {
                if ($_FILES[$k]['error'] == UPLOAD_ERR_OK) {
                    $dir = ifempty($paths, $this->getDir());
                    $this->createDir($dir);
                    $return[] = $this->moveUploadFile($_FILES[$k]['tmp_name'], $dir, $_FILES[$k]['name']);
                } else {
                    ga()->log($this->uploadError($_FILES[$k]['error']));

                }
            }
        }

        return $return;
    }

    /**
     * Возвращает права файла в формате $format_out
     *
     * Форматы прав str (dwr-wr-w--)  и oct ('0777','0644')
     *
     * @param string|int $perms      Права файла в формате $format_in
     * @param string     $format_in  Формат прав файла
     * @param string     $format_out Формат в котором нужно отдать права
     *
     * @return int|string
     */
    public function permsFormat($perms, $format_in, $format_out)
    {
        $m = [0 => ['s' => 0xC000, 'l' => 0xA000, '-' => 0x8000, 'b' => 0x6000, 'd' => 0x4000, 'c' => 0x2000, 'p' => 0x1000],
              1 => ['r' => 0400], 2 => ['w' => 0200], 3 => ['x' => 0100, 's' => 04100, 'S' => 04000], 4 => ['r' => 040], 5 => ['w' => 020],
              6 => ['x' => 010, 's' => 02010, 'S' => 02000], 7 => ['r' => 04], 8 => ['w' => 02], 9 => ['x' => 01, 't' => 01001, 'T' => 01000],];
        $pm = 0;
        if ($format_in == 'str') {
            for ($i = 0; $i < 10; $i++) {
                foreach ($m[$i] as $s => $b) {
                    if ($perms[$i] == $s) {
                        $pm += $b;
                        break;
                    }
                }
            }
        } elseif ($format_in == 'oct') {
            for ($i = strlen($perms) - 1; $i >= 0; --$i) {
                $pm += (int)$perms[$i] * pow(8, (strlen($perms) - $i - 1));
            }
        } else {
            $pm = $perms;
        }
        if ($format_out == 'str') {
            $r = '';
            for ($i = 0; $i < 10; $i++) {
                $n = false;
                foreach ($m[$i] as $s => $b) {
                    if (($pm & $b) == $b) {
                        $n = $s;;
                        break;
                    }
                }
                $r .= ($n) ? $n : ($i == 0 ? 'u' : '-');
            }

            return $r;
        }
        if ($format_out == 'oct') {
            return substr(sprintf('%o', $pm), -4);
        }

        return $pm;
    }

    /**
     * @see gaFilesystemInterface::filepermsHtml()
     *
     * @param array|string $file_data
     *
     * @return string
     */
    public function filepermsHtml($file_data)
    {
        $info = $this->fileinfo($file_data);
        $perms = $this->getPerms($info, 'str');
        if (!$this->isReadable($info))
            return '<font color="#f01">' . $perms . '</font>';
        elseif (!$this->isWriteable($info))
            return '<font color="#fff">' . $perms . '</font>';
        else
            return '<font color="#25ff01">' . $perms . '</font>';
    }

    /**
     * Возвращает сообщение ошибки
     *
     * @param integer $code Номер ошибки
     *
     * @return string
     */
    protected function uploadError($code)
    {
        return arget($this->upload_error, $code, "Unknown upload error");
    }

    /**
     * @see gaFilesystemInterface::unsetIgnore()
     *
     * @param array $files
     * @param array $ignore
     *
     * @return array
     */
    public function unsetIgnore(&$files, $ignore = ['.', '..'])
    {
        foreach ($files as $k => $v) {
            $name = isar($v) ? $v['name'] : $v;
            if (in_array($name, $ignore)) {
                unset($files[$k]);
            }
        }

        return $files;
    }


    /**
     * @see   gaFilesystemInterface::cast()
     *
     * @param string $path
     * @param bool   $is_dir
     *
     * @todo  надо протестировать при передаче пустой папки для винды
     * @return string
     */
    public function cast($path = '', $is_dir = true)
    {
        $path = rtrim(str_replace("\\", "/", trim($path)), '/');
        $unx = (strlen($path) > 0 && $path{0} == '/');
        $parts = array_filter(explode('/', $path), 'strlen');
        $absolutes = [];
        foreach ($parts as $part) {
            if ('.' == $part) continue;
            if ('..' == $part) {
                array_pop($absolutes);
            } else {
                $absolutes[] = $part;
            }
        }
        $path = implode('/', $absolutes);
        $path = $unx ? '/' . $path : $path;
        if ($is_dir) {
            $path .= (substr($path, -1) != '/') ? '/' : '';
        }

        return $path;
    }

    public function stripBaseDir($dir, $name = null)
    {
        if (!$name) {
            return basename($dir);
        } else {
            $d = preg_replace("~^$dir~is", '', $name);

            return strlen($d) ? $d : '..';
        }
    }

    /**
     * @see gaFilesystemInterface::tempDir()
     * @return string
     */
    public function tempDir()
    {
        foreach (['/tmp', '/usr/tmp', '/var/tmp', ini_get('upload_tmp_dir'), (getenv('TMP')) ? getenv('TMP') : getenv('TEMP')] as $d) {
            if (isdir($d) && iswritable($d)) {
                return $d;
            }
        }

        return iswritable(".") ? realpath(".") : false;
    }
}