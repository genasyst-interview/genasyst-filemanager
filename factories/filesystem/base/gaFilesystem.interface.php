<?php

interface gaFilesystemInterface
{

    /**
     * Возвращает рабочую директорию установленную ранее в setDir()
     * @see gaFilesystemInterface::setDir()
     *
     * @return string
     */
    public function getDir();

    /**
     * Возвращает корневую директорию домена
     *
     * @return string
     */
    public function getDomainDir();

    /**
     * Возвращает корневую директорию в которой находится файл
     *
     * @return mixed
     */
    public function getHomeDir();

    /**
     * Устанавливает рабочую директорию
     *
     * @param string $dir    Путь к директории
     * @param bool   $config Записать в системные настройки
     *
     * @return string
     */
    public function setDir($dir = '', $config = false);

    /**
     * Пытается создать директорию
     *
     * @param string $path путь к создаваемой директории
     *
     * @return string|false
     */
    public function createDir($path = '');

    /**
     * Возвращает список файлов директории
     *
     * @param string $path      Путь к директории файлы которой надо получить
     * @param bool   $recursive Обход вложенных директорий
     * @param int    $max_depth Глубина облхода
     * @param int    $depth     Внутренныя переменная
     *
     * @return array|false
     */
    public function listDir($path = '', $recursive = false, $max_depth = 999, $depth = 1);

    /**
     * Возвращает список файлов директории с подробной информацией о каждом файле
     *
     * @param string $path
     * @param bool   $recursive Обход вложенных директорий
     * @param int    $max_depth Глубина обхода
     *
     * @uses gaFilesystemInterface::listDir()
     *
     * @return array|false
     */
    public function getDirFiles($path = '', $recursive = false, $max_depth = 999);

    /**
     * Проверяет возможность перезаписи файла
     *
     * @param array|string $file_data Путь к файлу (/home/file.php) или массив данных файла из функции gaFilesystemInterface::fileinfo()
     *
     * @uses gaFilesystemInterface::fileinfo()
     *
     * @return bool
     */
    public function isWriteable($file_data);

    /**
     * Проверяет возможность чтения файла
     *
     * @param array|string $file_data Путь к файлу (/home/file.php) или массив данных файла из функции gaFilesystemInterface::fileinfo()
     *
     * @uses gaFilesystemInterface::fileinfo()
     *
     * @return bool
     */
    public function isReadable($file_data);

    /**
     * Возвращает информацию о файле или директории
     *
     * @param   string    $dir      Директория или полный путь к файлу
     * @param null|string $filename название файла
     *
     * @return mixed
     */
    public function fileinfo($dir, $filename = null);

    /**
     * Возвращает html код цветовой  прав
     *
     * @param array|string $file_data Путь к файлу (/home/file.php) или массив данных файла из функции gaFilesystemInterface::fileinfo()
     *
     * @return mixed
     */
    public function filepermsHtml($file_data);


    /**
     * Переименование файла
     *
     * @param string $old_path Путь к файлу
     * @param string $new_path Новый путь к файлу
     *
     * @return mixed
     */
    public function rename($old_path, $new_path);

    /**
     * Создает архив ZIP
     *
     * @param string $dir       Директория в которой будет создан архив (/home/folder)
     * @param string $name      Название файла создаваемого архива (name.zip)
     * @param string $files_dir Корневая директория для архивируемых файлов и директорий
     * @param array  $files     Массив файлов с относительными путями от корневой директории файлов
     *
     * @return mixed
     */
    public function createZip($dir = '', $name, $files_dir = '', $files);

    /**
     * Распаковывает ZIP архивы в директорию
     *
     * @param string $dir     - Директория в которую будут распакованы архивы
     * @param string $zip_dir - Корневая директория для архивов
     * @param array  $files   - Массив путей относительно корневой директории архивов
     *
     * @return bool
     */
    public function unZip($dir = '', $zip_dir = '', $files = []);

    /**
     * Создание архива Tar.GZ
     *
     * @param string   $dir       Директория в которой будет создан архив (/home/folder)
     * @param   string $name      Название файла создаваемого архива (name.tar.gz)
     * @param string   $files_dir Корневая директория для архивируемых файлов и директорий
     * @param    array $files     Массив файлов с относительными путями от корневой директории файлов
     */
    public function createTar($dir = '', $name, $files_dir = '', $files);

    /**
     * Инициализация загрузки файлов
     *
     * @return mixed
     */
    public function uploadFiles();

    /**
     * Возвращает контент файла
     *
     * @param string $path Полный путь к файлу
     *
     * @return string|false
     */
    public function getFile($path);

    /**
     * Потоковая отдача файла в браузер c завершением работы интерпретатора
     *
     * @param string $path Полный путь к файлу
     *
     * @return void
     */
    public function downloadFile($path);

    /**
     * Сохранение загруженного файла
     *
     * @param string $temp_path Временный путь к файлу
     * @param string $dir       Директория в которую надо переместить файл
     * @param string $name      Название файла
     *
     * @return string|false
     */
    public function moveUploadFile($temp_path, $dir, $name);

    /**
     * Печатает контент файла в браузер
     *
     * @param string $path Полный путь к файлу
     *
     * @return bool
     */
    public function printFile($path);

    /**
     * Пеерезапись (сохранение) файла
     *
     * @param string $path    Полный путь к файлу
     * @param string $content данные
     *
     * @return mixed
     */
    public function saveFile($path, $content);


    /**
     * Пытается изменрить метку времени файлы
     *
     * @param string $path Полный путь к файлу
     * @param string $time Время в формате Y-m-d H:i:s (2018-01-22 17:47:33)
     *
     * @return bool
     */
    public function touch($path, $time);

    /**
     * Копирует файл или директорию в другую директорию
     *
     * @param string $old_dir Текущая директория файлa
     * @param string $new_dir Директория в которую надо переместить файл
     * @param string $name    Название файла
     *
     * @return mixed
     */
    public function copy($old_dir, $new_dir, $name);

    /**
     * Копирует файл или директорию в другую директорию
     *
     * @param string $old_dir Текущая директория файлa
     * @param string $new_dir Директория в которую надо переместить файл
     * @param string $name    Название файла
     *
     * @return mixed
     */
    public function move($old_dir, $new_dir, $name);

    /**
     * Удаляет файл или директорию
     *
     * @param string $path Полный путь к файлу
     *
     * @return mixed
     */
    public function delete($path = '');

    /**
     * Возвращает права файла в заданном формате
     *
     * @param array|string $file_data Путь к файлу или массив данных файла из метода gaFilesystemInterface::fileinfo()
     * @param string       $format
     *
     * @uses gaFilesystemInterface::fileinfo()
     *
     * @return mixed
     */
    public function getPerms($file_data, $format = 'oct');

    /**
     * Устанавливает права файла или директории
     *
     * @param string $path  Полный путь к файлу
     * @param string $chmod Права в строковом представлении OCT ('0644', '0777'....)
     *
     * @return bool
     */
    public function setPerms($path, $chmod);

    /**
     * Удаление файлов . и .. из массива файлов
     *
     * @param array $files  Массив файлов
     * @param array $ignore Массив исключаемых файлов
     *
     * @return array
     */
    public function unsetIgnore(&$files, $ignore = ['.', '..']);

    /**
     * Раскрытие путей директории или файла
     *
     * @param string $path   Полный путь к файлу
     * @param bool   $is_dir - флаг переданной директории
     *
     * @return mixed
     */
    public function cast($path = '', $is_dir = true);

    /**
     * Сортировка массива файлов
     *
     * @param array $files
     *
     * @return array
     */
    public function sort(&$files);

    /**
     * Устанавливает тип сортировки файлов
     *
     * @param array $sort - массив данных сортировки
     *
     * @return array
     */

    public function setSort($sort = ['name', 1]);

    public function stripBaseDir($dir, $name = null);

    /**
     * Возвращает директорию временных файлов
     *
     * @return string
     */
    public function tempDir();

}