<?php

class gaFilesystemCallBack extends gaFilesystemAbstract implements gaFilesystemInterface
{
    /**
     * Экхемпляр класса
     * @var
     */
    protected static $_instance;

    /**
     * Родительские объекты для работы с файловой системой
     * @see gaFilesystemCallBack::init()
     * @var array
     */
    protected $callbacks = [];

    /**
     * Инициализация объектов файловой системы
     *
     * Также возможно добавтьи свой объект через любой модуль по хуку  filesystem_adapter
     *
     * @param null $data
     */
    public function init($data = null)
    {
        $this->callbacks = [
            0 => gaFilesystemCallBackPhp::getInstance($data),
            1 => gaFilesystemCallBackConsole::getInstance($data),
        ];
        foreach (ga()->event('filesystem_adapter') as $a) {
            if (class_exists($a) && $a instanceof gaFilesystemAbstract) {
                $this->callbacks[] = $a::getInstance($data);
            }
        }
    }

    /**
     * Вызов метода в дочерних объектах файловой системы
     *
     * Обходит поочередно дочерние объекты вызывая метод $name до получения результата,
     * если метод одного из объектов возвращает не false, то метод считается выполненным и возвращается результат
     *
     * @param   string $name    Название метода
     * @param   array  $args    Аргумены передаваемые в метод
     * @param null     $default Вернуть  при отсутствии результата
     *
     * @uses gaFilesystemCallBackPhp
     * @uses gaFilesystemCallBackConsole
     *
     * @return mixed|null
     */
    protected function callArray($name, $args, $default = null)
    {
        foreach ($this->callbacks as $class) {
            if (method_exists($class, $name)) {
                $r = call_user_func_array([$class, $name], $args);
                if ($r !== false) {
                    return $r;
                }
            }
        }

        return $default;
    }

    /**
     * @see gaFilesystemInterface::listDir()
     *
     * @param string $path
     * @param bool   $recursive
     * @param int    $max_depth
     * @param int    $depth
     *
     * @return mixed|null
     */
    public function listDir($path = '', $recursive = false, $max_depth = 0, $depth = 1)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), []);
    }

    /**
     * @see gaFilesystemInterface::createDir()
     *
     * @param string $dir
     *
     * @return mixed|null
     */
    public function createDir($dir = '')
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::getDirFiles()
     *
     * @param string $path
     * @param bool   $recursive
     * @param int    $max_depth
     *
     * @return array
     */
    public function getDirFiles($path = '', $recursive = false, $max_depth = 999)
    {
        $path = $this->setDir($path);
        $data = $this->callArray(__FUNCTION__, [$path, $recursive, $max_depth], []);
        $files = ifisar($data, 'files', []);
        $dirs = ifisar($data, 'dirs', []);
        $this->sort($files);
        $this->sort($dirs);
        $files = array_merge($dirs, $files);

        return $this->unsetIgnore($files, ['.']);
    }

    /**
     * @see gaFilesystemInterface::isWriteable()
     *
     * @param array|string $file_data
     *
     * @return mixed|null
     */
    public function isWriteable($file_data)
    {
        return (arkex('writeable', $file_data)) ?
            $file_data['writeable'] : $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::isReadable()
     *
     * @param array|string $file_data
     *
     * @return mixed|null
     */
    public function isReadable($file_data)
    {
        return (arkex('readable', $file_data)) ?
            $file_data['readable'] : $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::fileinfo()
     *
     * @param string      $dir
     * @param null|string $filename
     *
     * @return array|mixed|null|string
     */
    public function fileinfo($dir, $filename = null)
    {
        if (arkex(['path', 'name', 'type'], $dir)) {
            return $dir;
        }
        if ($filename === null) {
            $filename = basename($dir);
            $dir = $this->cast(dirname($dir));
        } else {
            $dir = $this->cast($dir);
        }
        $path = in_array(trim((string)$filename), ['..', '.']) ? $this->cast($dir . $filename) : $this->cast($dir) . $filename;

        if ($r = $this->callArray(__FUNCTION__, [$path, $filename], false)) {
            return $r;
        }
        $p = @fileperms($dir);

        return [
            'owner'     => 0,
            'group'     => 0,
            'type'      => 'file',
            'link'      => 'file',
            'path'      => $path,
            'name'      => $filename,
            'modify'    => @filemtime($path),
            'perms'     => $p,
            'size'      => @filesize($path),
            'str_perms' => $this->permsFormat($p, 'base', 'str'),
            'oct_perms' => $this->permsFormat($p, 'base', 'oct'),
            'writeable' => iswritable($path),
            'readable'  => isreadable($path),
        ];
    }

    /**
     * @see gaFilesystemInterface::printFile()
     *
     * @param string $path
     *
     * @return void
     */
    public function printFile($path)
    {
        $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::createZip()
     *
     * @param string $dir
     * @param string $name
     * @param string $files_dir
     * @param array  $files
     *
     * @return mixed|null
     */
    public function createZip($dir = '', $name, $files_dir = '', $files)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::unZip()
     *
     * @param string $dir
     * @param string $zip_dir
     * @param array  $files
     *
     * @return mixed|null
     */
    public function unZip($dir = '', $zip_dir = '', $files = [])
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::createTar()
     *
     * @param string $dir
     * @param string $name
     * @param string $files_dir
     * @param array  $files
     *
     * @return mixed|null
     */
    public function createTar($dir = '', $name, $files_dir = '', $files)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::moveUploadFile()
     *
     * @param string $temp_path
     * @param string $dir
     * @param string $name
     *
     * @return mixed|null
     */
    public function moveUploadFile($temp_path, $dir, $name)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }


    /**
     * @see gaFilesystemInterface::touch()
     *
     * @param string $file
     * @param string $time
     *
     * @return mixed|null
     */
    public function touch($file, $time)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::copy()
     *
     * @param string $old_dir
     * @param string $new_dir
     * @param string $name
     *
     * @return mixed|null
     */
    public function copy($old_dir, $new_dir, $name)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::move()
     *
     * @param string $old_dir
     * @param string $new_dir
     * @param string $name
     *
     * @return mixed|null
     */
    public function move($old_dir, $new_dir, $name)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::delete()
     *
     * @param string $path
     *
     * @return mixed|null
     */
    public function delete($path = '')
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::getFile()
     *
     * @param string $link
     *
     * @return mixed|null
     */
    public function getFile($link)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::saveFile()
     *
     * @param string $path
     * @param string $content
     *
     * @return mixed|null
     */
    public function saveFile($path, $content)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::rename()
     *
     * @param string $old_path
     * @param string $new_path
     *
     * @return mixed|null
     */
    public function rename($old_path, $new_path)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

    /**
     * @see gaFilesystemInterface::getPerms()
     *
     * @param array|string $file_data
     * @param string       $format
     *
     * @return bool|int|mixed|string
     */
    function getPerms($file_data, $format = 'oct')
    {
        if (!isar($file_data)) {
            $file_data = $this->fileinfo($file_data);
        }
        if (isar($file_data)) {
            if (arkex($format . '_perms', $file_data)) {
                return $file_data[$format . '_perms'];
            }
            if (!arkex('perms', $file_data) && isset($file_data['path'])) {
                $file_data = $this->fileinfo($file_data['path']);
            }

            return $this->permsFormat(ifset($file_data, 'perms', 0), 'base', $format);
        }

        return false;
    }

    /**
     * @see gaFilesystemInterface::setPerms()
     *
     * @param string $path
     * @param string $chmod
     *
     * @return mixed|null
     */
    function setPerms($path, $chmod)
    {
        return $this->callArray(__FUNCTION__, func_get_args(), false);
    }

}