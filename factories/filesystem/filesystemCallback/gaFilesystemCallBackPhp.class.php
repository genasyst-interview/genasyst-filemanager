<?php

class gaFilesystemCallBackPhp extends gaFilesystemAbstract
{
    protected static $_instance;

    /**
     * @param string $path      Путь к директории файлы которой надо получить
     * @param bool   $recursive - Обход вложенных директорий
     * @param int    $max_depth - Глубина облхода
     * @param int    $depth     - Внутренныя переменная
     *
     * @return bool|mixed|null
     */
    public function listDir($path = '', $recursive = false, $max_depth = 999, $depth = 1)
    {
        $path = $this->cast($path);
        if (!isdir($path)) {
            return false;
        }
        $files = [];
        if (($dh = @opendir($path))) {
            while ((false !== ($file = readdir($dh)))) {
                if ($recursive) {
                    if (in_array($file, ['..', '.'])) {
                        continue;
                    }
                    if (is_dir($path . $file) && ($depth <= $max_depth)) {
                        $files = array_merge($files, ifisar($this->listDir($path . $file, $recursive, $max_depth, ($depth + 1)), []));
                        continue;
                    }
                    $files[] = $path . $file;
                } else {
                    $files[] = $file;
                }

            }
            closedir($dh);
        }

        return ifempty(ifisar($files, false), false);
    }

    /**
     * @param string $path - Путь к создаваемой директории
     *
     * @return bool|string
     */
    public function createDir($path = '')
    {
        if (fiex($path)) {
            chmod($path, 0750);

            return $path;
        }

        return mkdir($path, 0750, true) ? $path : false;
    }

    /**
     * @param string $path Путь к директории файлы которой надо получить
     *
     * @return array|false Заполненный массив директорий и файлов ['dirs'=>$dirs,'files'=>$files], либо false
     */
    public function getDirFiles($path = '', $recursive = false, $max_depth = 999)
    {
        $path = $this->cast($path);
        $dirs = $files = [];
        foreach (ifempty($this->listDir($path, $recursive, $max_depth), []) as $v) {
            if (!empty($v)) {
                $name = $this->stripBaseDir($path, $v);
                $f = $this->fileinfo($path . $name, $name);
                if (empty($f)) {
                    continue;
                }
                if ($f['type'] == 'dir') {
                    $dirs[] = $f;
                } else {
                    $files[] = $f;
                }
            }
        }

        return (!empty($dirs) || !empty($files)) ? ['dirs' => $dirs, 'files' => $files] : false;
    }

    /**
     * @param array|string $file_data массив данных файла, полученный через obj->fileinfo($path) или путь к файлу от
     *                                корня сервера
     *
     * @return bool
     */
    public function isWriteable($file_data)
    {
        if (isar($file_data)) {
            if (arkex('writeable', $file_data)) {
                return $file_data['writeable'];
            } elseif (isset($file_data['path'])) {
                $file_data = $file_data['path'];
            }
        }

        return iswritable((string)$file_data);
    }

    /**
     * @param array|string $file_data массив данных файла, полученный через obj->fileinfo($path) или путь к файлу от корня сервера
     *
     * @return bool
     */
    public function isReadable($file_data)
    {
        if (isar($file_data)) {
            if (arkex('readable', $file_data)) {
                return $file_data['readable'];
            } elseif (isset($file_data['path'])) {
                $file_data = $file_data['path'];
            }
        }

        return isreadable((string)$file_data);
    }

    /**
     *
     * @param string $path
     * @param string $name
     *
     * @return array|false
     */
    public function fileinfo($path, $name = null)
    {
        if ($name == null) {
            $name = basename($path);
        }
        if (fiex($path) && @fileperms($path)) {
            $r = array_merge($this->finfo, [
                'path'   => $path,
                'name'   => $name,
                'modify' => @filemtime($path),
                'perms'  => @fileperms($path),
                'size'   => @filesize($path),
            ]);

            if (ga()->getOS() == 'nix') {
                $uid = null;
                if (funex('posix_getpwuid')) {
                    $uid = @posix_getpwuid(@fileowner($path));
                }
                if ($uid) {
                    $r['owner'] = $uid['uid'];
                    $r['group'] = $uid['gid'];
                } else {
                    $r['owner'] = @fileowner($path);
                    $r['group'] = @filegroup($path);
                }
            }
            if (isdir($path)) {
                $r['type'] = 'dir';
            } elseif (@is_link($path)) {
                $r['type'] = 'link';
                $r['link'] = readlink($path);
            }

            return array_merge($r, [
                'str_perms' => $this->permsFormat($r['perms'], 'base', 'str'),
                'oct_perms' => $this->permsFormat($r['perms'], 'base', 'oct'),
                'writeable' => $this->isWriteable($path),
                'readable'  => $this->isReadable($path),
            ]);
        }

        return false;
    }

    /**
     * @param string $dir
     * @param        $name
     * @param string $files_dir
     * @param        $files
     */
    public function createZip($dir = '', $name, $files_dir = '', $files)
    {
        if (class_exists('ZipArchive')) {
            $zip = new ZipArchive();
            $name .= (substr($name, -4) == '.zip') ? '' : '.zip';
            if (($op = $zip->open($dir . $name, ZipArchive::CREATE)) === true) {
                chdir($files_dir);
                $this->unsetIgnore($files);
                foreach ($files as $file) {
                    if (is_file($files_dir . $file)) {
                        $zip->addFile($file, str_replace($files_dir, '', $file));
                    } elseif (isdir($files_dir . $file)) {
                        /* $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($file));*/
                        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($file));
                        foreach ($iterator as $key => $value) {
                            if (isdir($key)) {
                                $zip->addEmptyDir(str_replace($files_dir, '', $key));
                            } else {
                                $zip->addFile(realpath($key), str_replace($files_dir, '', $key));
                            }
                        }
                    }
                }
                $zip->close();
            }

            return (fiex($dir . $name) && filesize($dir . $name) > 0);
        } else {
            ga()->log("Zip Archive not support!");

            return false;
        }
    }

    /**
     * Распаковывает ZIP архивы в директорию
     *
     * @param string $dir     - Директория в которую будут распакованы архивы
     * @param string $zip_dir - Корневая директория для архивов
     * @param array  $files   - Массив путей относительно корневой директории архивов
     *
     * @return bool
     */
    public function unZip($dir = '', $zip_dir = '', $files = [])
    {
        $dir = $this->cast($dir);
        $zip_dir = $this->cast($zip_dir);
        $r = true;
        if (class_exists('ZipArchive') && !empty($dir)) {
            $zip = new ZipArchive();
            foreach (ifisar($files, []) as $f) {
                if ($zip->open($zip_dir . $f)) {
                    if (!$zip->extractTo($dir)) {
                        $r = false;
                    }
                    $zip->close();
                } else {
                    $r = false;
                }
            }
        } else {
            $r = false;
        }

        return $r;
    }

    /**
     * Создание архива Tar.GZ
     *
     * @param string $dir
     * @param        $name
     * @param string $files_dir
     * @param        $files
     */
    public function createTar($dir = '', $name, $files_dir = '', $files)
    {
        if (chdir($files_dir)) {
            $files = array_map('escapeshellarg', $files);

            return ga()->exr('tar cfzv ' . $dir . escapeshellarg($name) . ' ' . implode(' ', $files));
        }

        return false;
    }

    /**
     * @param $temp_path
     * @param $dir
     * @param $name
     *
     * @return bool|string
     */
    public function moveUploadFile($temp_path, $dir, $name)
    {
        $p = $this->cast($dir) . $name;

        return move_uploaded_file($temp_path, $p) ? $p : false;
    }

    /**
     * @param $file
     * @param $time
     *
     * @return bool
     */
    public function touch($file, $time)
    {
        return touch($file, $time, $time);
    }

    /**
     * @param      $old_dir
     * @param      $name
     * @param      $new_dir
     * @param bool $delete_old
     *
     * @return bool
     */
    public function copy($old_dir, $new_dir, $name, $delete_old = false)
    {
        $old_dir = $this->cast($old_dir);
        $new_dir = $this->cast($new_dir);
        $r = true;
        if (isdir($this->cast($old_dir . $name))) {
            $old_path = $this->cast($old_dir . $name);
            $new_path = $this->cast($new_dir . $name);
            if (!fiex($new_path)) {
                $r = mkdir($new_path, 0777, true);
                if (!$r) {
                    ga()->log("Can't create new dir: " . $new_path . '!');
                }
            }
            $h = opendir($old_path);
            while (($file = readdir($h)) !== false) {
                if (($file != ".") and ($file != "..")) {
                    if (!$this->copy($old_path, $new_path, $file, $delete_old)) {
                        ga()->log("Can't create new path: " . $new_path . '!');
                        $r = false;
                    }
                }
            }
            if ($r && $delete_old) {
                $r = rmdir($old_path);
                if (!$r) {
                    ga()->log("Can't delete old path: " . $old_path . '!');
                }
            }
        } elseif (fiex($old_dir . $name)) {
            $r = copy($old_dir . $name, $new_dir . $name);
            if ($r && $delete_old) {
                $r = unlink($old_dir . $name);
                if (!$r) {
                    ga()->log("Can't delete old file: " . $old_dir . $name . '!');
                }
            }
        } else {
            ga()->log("Old file path not exists: " . $old_dir . $name . '!');
        }

        return $r;
    }

    /**
     * @param $old_dir
     * @param $name
     * @param $new_dir
     *
     * @return bool
     */
    public function move($old_dir, $new_dir, $name)
    {
        return $this->copy($old_dir, $new_dir, $name, true);
    }


    /**
     * @param string $path
     *
     * @return bool
     */
    public function delete($path = '')
    {
        $r = false;
        if (isdir($path)) {
            $dh = opendir($path);
            $path = $this->cast($path);
            while (($file = readdir($dh)) !== false) {
                if (($file == "..") || ($file == ".")) {
                    continue;
                } else {
                    $r = $this->delete($path . $file);
                }
            }
            closedir($dh);
            if ($r) {
                if (($r = @rmdir($path)) == false) {
                    ga()->log("Can't delete base dir: " . $path . '!');
                }
            } else {
                ga()->log("Can't delete  dir files: " . $path . '!');
            }
        } elseif (fiex($path)) {
            if (($r = @unlink($path)) == false) {
                ga()->log("Can't delete file: " . $path . '!');
            }
        } else {
            ga()->log("File path not exists: " . $path . '!');
        }

        return $r;
    }


    /**
     * @param $path
     *
     * @return false|string
     */
    public function getFile($path)
    {
        $r = '';
        if (isreadable($path) && ($fp = @fopen($path, 'r'))) {
            while (!@feof($fp)) {
                $r .= @fread($fp, 1024);
            }
            @fclose($fp);

            return $r;
        }

        return false;
    }

    /**
     * Специальный метод для вывода больших файлов
     *
     * @param $path
     *
     * @return bool
     */
    public function printFile($path)
    {
        if (isreadable($path) && ($fp = @fopen($path, 'r'))) {
            while (!@feof($fp)) {
                echo @fread($fp, 1024);
            }
            @fclose($fp);

            return true;
        }

        return false;
    }

    /**
     * @param $path
     * @param $content
     *
     * @return bool|int
     */
    public function saveFile($path, $content)
    {
        $time = fiex($path) ? filemtime($path) : time();
        if (funex('file_put_contents')) {
            if (!($r = file_put_contents($path, $content))) {
                return false;
            }
        } else {
            $fp = fopen($path, "w");
            if ($fp) {
                $r = fwrite($fp, $content);
                fclose($fp);
            } else {
                return false;
            }
        }
        @touch($path, $time, $time);

        return $r;
    }

    /**
     * @param $old_path
     * @param $new_path
     *
     * @return bool
     */
    public function rename($old_path, $new_path)
    {
        return @rename($old_path, $new_path);
    }

    /**
     * @param        $file_data
     * @param string $format
     *
     * @return bool|float|int|mixed|string
     */
    public function getPerms($file_data, $format = 'oct')
    {
        if (!isar($file_data)) {
            $file_data = $this->fileinfo($file_data);
        }
        if (isar($file_data)) {
            if (arkex($format . '_perms', $file_data)) {
                return $file_data[$format . '_perms'];
            }
            if (!arkex('perms', $file_data) && isset($file_data['path'])) {
                $file_data = $this->fileinfo($file_data['path']);
            }

            return $this->permsFormat(ifset($file_data, ['perms'], 0), 'base', $format);
        }

        return false;
    }

    /**
     * @param        $path  - Путь к файлу или директории
     * @param string $perms - Строка прав в формате '0777' '0755' '0655'...
     *
     * @return bool
     */
    public function setPerms($path, $perms)
    {
        if (!empty($perms)) {
            $p = 0;
            for ($i = strlen($perms) - 1; $i >= 0; --$i) {
                $p += (int)$perms[$i] * pow(8, (strlen($perms) - $i - 1));
            }

            return chmod($path, $p);
        }

        return false;
    }
}