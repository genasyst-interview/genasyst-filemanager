<?php

class gaFilesystemCallBackConsole extends gaFilesystemAbstract
{
    protected static $_instance;

    protected $finfo_pattern = [
        'nix' => '@^(?<str_perms>[-dlsbpcrwxst]{10})\s+(?:\d+)\s+(?<owner>\S+)\s+(?<group>\S+)\s+(?<size>\d+)\s+(?<month>\w+)\s+(?<day>\d{1,2})\s+(((?<hour>\d{1,2}):(?<minutes>\d{1,2}))|(?<y>\d{4}))\s+(?<name>.*)$@is',
        'win' => '@^(?<month>d{1,2})-(?<day>d{1,2})-(?<y>d{1,2})s+(?<hour>d{1,2}):(?<minutes>d{1,2})(?<ampm>am|pm)s+(?<dir>[<]dir[>])?s+(?<size>d+)?s+(?<name>.*)$@is',
    ];


    public function listDir($path = '', $recursive = false, $max_depth = 999, $depth = 1)
    {
        return $this->_listDir($path, $recursive, $max_depth, false);
    }

    protected function _listDir($path = '', $recursive = false, $max_depth = 999, $params = false)
    {
        $path = $this->cast($path);
        $max_depth = intval($max_depth) < 1 ? 1 : intval($max_depth);
        $path = escapeshellarg($path);

        $flags = $params ? ['-ld', '-lna'] : ['', ''];

        $c = ga()->ex((
        ($recursive && $max_depth > 1) ?
            'find  ' . $path . ' -maxdepth ' . escapeshellarg($max_depth) . ' -exec ls ' . $flags[0] . ' "{}" \;' :
            'ls ' . $flags[1] . ' ' . $path
        ));
        $r = [];
        if (!empty($c)) {
            $c = explode("\n", (string)$c);
            foreach (ifisar($c, []) as $f) {
                if (!empty($f)) {
                    $f = str_replace($path, '', $f);
                    $r[] = $f;
                }
            }
        }

        return ifempty($r, false);
    }

    public function getDirFiles($path = '', $recursive = false, $max_depth = 999)
    {
        $recursive = true;
        $max_depth = 2;
        $dirs = $files = [];
        $c = $this->_listDir($path, $recursive, $max_depth, true);

        if (!empty($c)) {
            foreach (ifisar($c, []) as $f) {
                if (($d = $this->parseFileinfo($path, $f))) {
                    if ($d['type'] == 'dir') {
                        $dirs[] = $d;
                    } else {
                        $files[] = $d;
                    }
                }
            }
        }

        return (!empty($dirs) || !empty($files)) ? ['dirs' => $dirs, 'files' => $files] : false;
    }

    public function createDir($path = '')
    {
        $path = $this->cast($path);
        $d = ga()->exr('mkdir -p ' . escapeshellarg($path));

        return (strpos('mkdir: cannot', $d) === false && $this->isReadable($path));
    }

    protected function isRight($fdata, $r = 'r')
    {
        if (isar($fdata) && arkex('path', $fdata)) {
            if (!arkex(['str_perms', 'owner', 'group'], $fdata)) {
                $fdata = $fdata['path'];
            }
        }
        if (is_string($fdata)) {
            $fdata = $this->fileinfo($fdata);
        }
        if (!arkex(['str_perms', 'owner', 'group'], $fdata)) {
            return false;
        }
        $us = ga('s', 'os_user');
        $p = str_split(str_pad(substr($fdata['str_perms'], 1, 9), 9, '-', 1), 3);
        if (($us['uid'] == $fdata['owner'] && strpos($p[0], $r) !== false)
            || ($us['gid'] == $fdata['group'] && strpos($p[1], $r) !== false)
        ) {
            return true;
        } else {
            return !(strpos($p[2], $r) === false);
        }
    }

    public function isReadable($fdata)
    {
        if (isar($fdata) && arkex('readable', $fdata)) {
            return $fdata['readable'];
        }

        return $this->isRight($fdata, 'r');
    }

    public function isWriteable($fdata)
    {
        if (isar($fdata) && arkex('writeable', $fdata)) {
            return $fdata['writeable'];
        }

        return $this->isRight($fdata, 'w');
    }

    public function getPerms($file_data, $format = 'oct')
    {
        if (!isar($file_data)) {
            $file_data = $this->fileinfo($file_data);
        }
        if (isar($file_data)) {
            if (arkex($format . '_perms', $file_data)) {
                return $file_data[$format . '_perms'];
            }
            if (!arkex('perms', $file_data) && isset($file_data['path'])) {
                $file_data = $this->fileinfo($file_data['path']);
            }

            return $this->permsFormat(ifset($file_data, ['perms'], 0), 'base', $format);
        }

        return false;
    }

    protected function parseFileinfo($dir, $string_data = '')
    {
        $dir = $this->cast($dir);
        if (!empty($string_data) && preg_match($this->finfo_pattern[ga()->getOS()], $string_data, $data)) {
            $p = $dir . $this->stripBaseDir($dir, $data['name']);
            $prm = $this->permsFormat(ifempty($data, ['str_perms'], $this->finfo['str_perms']), 'str', 'base');
            $r = array_merge($this->finfo, [
                'owner'     => ifset($data, 'owner', 0),
                'group'     => ifset($data, 'owner', 0),
                'path'      => $p,
                'name'      => $this->stripBaseDir($dir, $data['name']),
                'modify'    => (@filemtime($p)) ? filemtime($p) :
                    strtotime(
                        str_pad(ifempty($data, 'day', '01'), 2, "0", STR_PAD_LEFT) . ' '
                        . ifset($data, 'month', 'Jan') . ' '
                        . ifempty($data, 'y', date('Y', time())) . ' '
                        . ifempty($data, 'hour', '00') . ':'
                        . ifempty($data, 'minutes', '00') . ':01'
                    ),
                'str_perms' => $data['str_perms'],
                'perms'     => $prm,
                'oct_perms' => $this->permsFormat($prm, 'base', 'oct'),
                'size'      => ifset($data, 'size', 0),
            ]);
            $ft = substr($r['str_perms'], 0, 1);
            if ($ft == 'd') {
                $r['type'] = 'dir';
            } elseif ($ft == 'l') {
                $r['type'] = 'link';
                $r['path'] = trim(strstr($r['path'], '->', true));
                $r['name'] = $this->stripBaseDir($dir, trim(strstr($data['name'], '->', true)));
                $r['link'] = $this->readLink($dir, $r['name'], trim(str_replace('->', '', strstr($data['name'], '->'))));
                $r['name'] .= ' -> ' . $r['link'];
                $r['path'] = $r['link'];
            }
            $r['writeable'] = $this->isRight($r, 'w');
            $r['readable'] = $this->isRight($r, 'r');

            return $r;
        }

        return false;
    }

    /**
     * @param $dir
     * @param $link
     *
     * @return string
     */
    protected function readLink($dir, $file, $link)
    {
        if (substr($link, 0, 1) == '/') {
            return $link;
        } else {
            $path = dirname($file);

            return $this->cast($dir . $path . '/' . $link, false);
        }
    }

    public function fileinfo($path, $name = null)
    {
        if (isar($path) && arkex(['path', 'name', 'type'], $path)) {
            return $path;
        }
        $d = ga()->exr('ls -lna ' . escapeshellarg($path));
        if (strpos((string)$d, 'ls: cannot') !== false) {
            return false;
        }
        /* if multistring this is dir! */
        if (count(explode("\n", $d)) > 1) {
            foreach (explode("\n", $d) as $v) {
                $i = $this->parseFileinfo(dirname($path), $v);
                if ($i && $i['name'] == '.') {
                    $i['name'] = $name;
                    $i['path'] = $path;

                    return $i;
                }
            }
        } else {
            return $this->parseFileinfo(dirname($path), $d);
        }
    }

    public function createTar($dir = '', $name, $files_dir = '', $files)
    {
        /*TODO: НАдо сделать проверку создания архива
        $files = array_map(function($v)use($files_dir){
            return rtrim($files_dir,'\\/').'/'.ltrim($v,'\\/');
        }, $files);*/
        $files = array_map('escapeshellarg', $files);

        return ga()->exr('cd ' . escapeshellarg($files_dir) . '; tar cfzv ' . $dir . escapeshellarg($name) . ' ' . implode(' ', $files));

    }

    function getFile($path)
    {
        if ($this->isReadable($path)) {
            return ga()->ex('cat ' . escapeshellarg($path));
        } else {
            return false;
        }
    }

    public function printFile($path)
    {
        if ($this->isReadable($path)) {
            echo ga()->ex('cat ' . escapeshellarg($path));
        } else {
            return false;
        }
    }

    /**
     * @see gaFilesystemInterface::downloadFile()
     *
     * @param string $path
     */
    public function downloadFile($path)
    {
        if (ob_get_level()) {
            ob_end_clean();
        }
        ga()->config()->set('Action', '', true);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($path));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        ga()->sendHeaders();
        $this->callArray('printFile', func_get_args());

        exit;
    }


    function saveFile($path, $content)
    {
        if ($this->createDir($this->cast(dirname($path)))) {
            ga()->ex('echo ' . escapeshellarg($content) . ' > ' . escapeshellarg($path));
            $this->setPerms($path, '0755');

            return true;
        }

        return false;
    }

    public function delete($path = '')
    {
        if (in_array($path, ['/', '/home', '/home/', '/root/'])) {
            echo 'ERROR PARSE PATH!!!!';
            exit;
        }
        $d = ga()->exr('rm -rf ' . escapeshellarg($path));

        return (strpos('rm: cannot', $d) === false);
    }

    public function setPerms($path, $perms)
    {
        $d = ga()->exr('chmod ' . escapeshellarg(substr($perms, 1, strlen($perms))) . '  ' . escapeshellarg($path));

        return (strpos('Operation not permitted', $d) === false);
    }

    public function rename($old_path, $new_path)
    {
        return false;
    }

    public function createZip($dir = '', $name, $files_dir = '', $files)
    {
        return false;
    }

    public function unZip($dir = '', $zip_dir = '', $files = [])
    {
        return false;
    }

    public function uploadFiles()
    {
        return false;
    }

    public function touch($path, $time)
    {
        return false;
    }

    public function copy($old_dir, $new_dir, $name, $delete_old = false)
    {
        return false;
    }

    public function move($old_dir, $new_dir, $name)
    {
        return false;
    }

    public function moveUploadFile($temp_path, $dir, $name)
    {
        return false;
    }


}