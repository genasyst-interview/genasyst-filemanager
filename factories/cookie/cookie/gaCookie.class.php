<?php

class gaCookie extends gaStorage
{
    use  gaSingletonTrait;
    protected $edit = [];
    protected $time = 0;
    protected $shipped = false;

    protected function init($data = null)
    {
        $this->data = ifisar($_COOKIE, []);
        $this->time = time() + 10000000;
    }

    public function set($key = '', $data)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }
        if (!(isset($this->data[$key]) && $this->data[$key] === $data)) {
            $this->edit[$key] = true;
        }
        if (is_array($data)) {
            $data = serialize($data);
        }
        $this->data[$key] = $data;
    }

    public function get($key = '', $default = null)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }
        $data = arget($this->data, $key, $default);
        $t = @unserialize($data);
        if ($t) {
            $data = $t;
        }

        return $data;
    }

    public function send()
    {
        if (!$this->shipped) {
            foreach ($this->data as $k => $v) {
                if (isset($this->edit[$k])) {
                    setcookie($k, (string)$v, $this->time);
                }
            }
            $this->shipped = true;
        }
    }
}