<?php

class gaRequest
{
    protected static $params = [];
    protected $get_method = 'post';

    public function __construct($o = [])
    {
        if (isset($o['get_method']) && method_exists($this, $o['get_method'])) {
            $this->get_method = $o['get_method'];
        }
    }

    public function set($key, $value)
    {
    }

    public function get($key = null, $default = null)
    {
        return $this->{$this->get_method}($key, $default);
    }

    public function post($key = null, $default = null, $type = null)
    {
        return self::getData($_POST, $key, $default, $type);
    }

    public function _get($key = null, $default = null, $type = null)
    {
        return $this->getData($_GET, $key, $default, $type);
    }

    public function request($key = null, $default = null, $type = null)
    {
        if ($key === null) {
            return $_POST + $_GET;
        }
        $r = $this->post($key, $default, $type);
        if ($r !== $default) {
            return $r;
        }

        return $this->_get($key, $default, $type);
    }

    public function isXMLHttpRequest()
    {
        return $this->server('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
    }

    public function getUserAgent()
    {
        return $this->server('HTTP_USER_AGENT');
    }

    public function server($key = null, $default = null)
    {
        return ga()->server($key, $default);
    }

    public function method()
    {
        return strtolower($this->server('REQUEST_METHOD'));
    }

    public static function getData($data, $key = null, $default = null, $type = null)
    {
        if (func_num_args() > 4) {
            $keys = func_get_args();
            $data = array_shift($keys);
            $type = array_pop($keys);
            $default = array_pop($keys);
        } else {
            if ($key === null) {
                return $data;
            }
            $keys = $key;
        }
        $value = ifset($data, $keys, $default);
        if (!is_null($value)) {
            return $type ? gaHelper::_castValue($value, $type) : $value;
        } else {
            return self::getDefault($default);
        }
    }

    protected static function getDefault(&$default)
    {
        return isar($default) && $default ? array_shift($default) : $default;
    }
}