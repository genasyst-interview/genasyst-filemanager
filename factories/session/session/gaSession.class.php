<?php

class gaSession
{
    use  gaStorageTrait, gaSingletonTrait;
    protected static $started = false;
    protected $shipped = false;

    protected function init($data = null)
    {
        if (isset($_COOKIE[session_name()])) {
            $this->open();
        }
        $h = ga()->config()->hash();
        $this->data = (@is_array($_SESSION) && arkex($h, $_SESSION)) ?
            ifisar(@json_decode(@_d($_SESSION[$h]), true), []) : [];
    }

    public function open()
    {
        if (!self::$started) {
            session_start();
            self::$started = true;
        }
    }

    public function set($key = '', $data)
    {
        $this->open();
        if (is_array($key) && count($key) == 2) {
            $this->data[$key[0]][$key[1]] = $data;
        } else {
            $this->data[$key] = $data;
        }
    }

    public function close()
    {
        $return = self::$started;
        self::$started = false;
        session_write_close();

        return $return;
    }

    public function destroy()
    {
        self::$started = false;
        session_unset();
        session_destroy();
    }

    public function send()
    {
        if (!$this->shipped) {
            $_SESSION[ga()->config()->hash()] = _e(json_encode($this->data));
            $this->shipped = true;
        }
    }
}