<?php

class gaDB
{
    protected $a = null;
    protected $query = [];

    public static function create($data)
    {
        return new static($data);
    }

    public function __construct($data)
    {
        $this->a = new gaDBMysql($data);
    }

    public function __call($name, $args)
    {
        return method_exists($this->a, $name) ? call_user_func_array([$this->a, $name], $args) : false;
    }

    public function query($query = null)
    {
        return $this->a->query((is_array($query) ? $this->buildSQL($query) : $query));
    }

    protected function buildSQL($q)
    {
        $q = array_merge([
            'select' => '*',
            'table'  => '',
            'where'  => [],
            'order'  => '',
            'limit'  => [0, 50],
        ], $q);
        $sql = "SELECT " . $q['select'] . " FROM " . $q['table'];
        $q['where'] = ifisar($q['where'], (array)$q['where']);
        $sql .= (!empty($q['where'])) ? " WHERE (" . implode(") AND (", $q['where']) . ") " : '';
        $sql .= (!empty($q['order'])) ? " ORDER BY " . $q['order'] : ' ';
        $q['limit'] = (isar($q['limit']) && count($q['limit']) == 2) ? $q['limit'][0] . ',' . $q['limit'][1] : '0,50';
        $sql .= (!empty($q['limit'])) ? " LIMIT " . $q['limit'] : ' ';

        return $sql;
    }
}


