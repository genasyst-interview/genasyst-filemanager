<?php

abstract class gaDBAdapter
{
    protected $st = null;
    protected $settings = [];
    protected $data = [];
    protected $errors = [];

    public function __construct($settings)
    {
        if (!empty($settings)) {
            $this->st = $this->connect($settings);
        }
        if ($this->st) {
            if (!empty($settings['db'])) {
                $select_db = $this->select_db($settings['db']);
            }
        } else {
            $this->errors[] = 'No connect to dbExtends!' . $this->error();
        }
    }

    public function isConnect()
    {
        return ($this->st);
    }

    protected function getError()
    {
        return !empty($this->errors) ? (isar($this->errors) ? implode(',', $this->errors) : $this->errors) : false;
    }

    public function version()
    {
        return false;
    }

    public function query($query)
    {
        return new gaDBResult($this, $query);
    }

    public function reconnect()
    {
        if ($this->st) {
            $this->close();
        }

        return $this->st = $this->connect($this->settings);
    }

    abstract public function connect($settings);

    abstract public function close();

    abstract public function error();

    abstract public function escape($data);

    abstract public function dump($table);

    abstract public function select_db($db);

    abstract public function databases();

    abstract public function tables();

    abstract public function schema($table);

    abstract protected function _query($query);
}