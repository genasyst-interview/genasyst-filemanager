<?php

class gaDBMysql extends gaDBAdapter
{
    protected $result_type = [
        'assoc' => MYSQLI_ASSOC,
        'num'   => MYSQLI_NUM,
        'both'  => MYSQLI_BOTH,
    ];

    public function connect($settings)
    {
        $this->settings = $settings;
        $settings['port'] = ifempty($settings, 'port', ini_get('mysqli.default_port'));
        $st = @mysqli_connect($settings['host'], $settings['user'], $settings['pass'], '', (isset($settings['port']) ? $settings['port'] : ''));
        if ($st) {
            @mysqli_set_charset($st, $this->settings['charset']);

            return $st;
        }

        return false;
    }

    public function dump($table, $fp = null)
    {
        $res = $this->query('SHOW CREATE TABLE `' . $table . '`');
        $create = $res->fetch();
        $sql = $create['Create Table'] . ";\n";
        if ($fp) fwrite($fp, $sql); else echo($sql);
        $r = $this->query('SELECT * FROM `' . $table . '`');
        $head = true;
        while ($item = $r->fetch()) {
            $columns = array();
            foreach ($item as $k => $v) {
                if ($v == null)
                    $item[$k] = "NULL";
                elseif (is_numeric($v))
                    $item[$k] = $v;
                else
                    $item[$k] = "'" . $this->escape($v) . "'";
                $columns[] = "`" . $k . "`";
            }
            if ($head) {
                $sql = 'INSERT INTO `' . $table . '` (' . implode(", ", $columns) . ") VALUES \n\t(" . implode(", ", $item) . ')';
                $head = false;
            } else
                $sql = "\n\t,(" . implode(", ", $item) . ')';
            if ($fp) fwrite($fp, $sql); else echo($sql);
        }
        if (!$head)
            if ($fp) fwrite($fp, ";\n\n"); else echo(";\n\n");
    }

    public function escape($data)
    {
        return mysqli_real_escape_string($this->st, $data);
    }

    public function error()
    {
        return @mysqli_error($this->st);
    }

    public function close()
    {
        return mysqli_close($this->st);
    }

    public function version()
    {
        $res = $this->query("SELECT VERSION()")->fetchField();

        return ($res) ? (float)$res : false;
    }

    public function databases()
    {
        $sql = ($this->version() >= 5) ? "SELECT SCHEMA_NAME FROM information_schema.SCHEMATA" : "SHOW DATABASES";

        return $this->query($sql)->fetchAll(false, true);
    }

    public function select_db($db)
    {
        return mysqli_select_db($this->st, $db);
    }

    public function tables()
    {
        return $this->query("SHOW TABLES")->fetchAll(false, 1);
    }

    public function schema($table)
    {
        return $this->query("SHOW COLUMNS FROM " . $table)->fetchAll('Field');
    }

    public function _query($query)
    {
        return ($this->st) ? mysqli_query($this->st, $query) : false;
    }

    public function fetch($r, $type = 'assoc')
    {

        return ($r) ? mysqli_fetch_array($r, ifset($this->result_type, $type, 'both')) : debug_print_backtrace(2, 4);
    }
}

