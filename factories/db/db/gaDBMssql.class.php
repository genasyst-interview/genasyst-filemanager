<?php

class gaDBMssql extends gaDBAdapter
{
    protected $result_type = [
        'assoc' => MSSQL_ASSOC,
        'num'   => MSSQL_NUM,
        'both'  => MSSQL_BOTH,
    ];

    public function connect($settings)
    {
        if (!@function_exists('mssql_connect')) return false;
        $this->settings = $settings;
        $settings['port'] = ifempty($settings, 'port', '1433');

        $st = @mssql_connect($settings['host'] . ',' . $settings['port'], $settings['user'], $settings['pass']);
        if ($st) {
            @mysqli_set_charset($st, $this->settings['charset']);

            return $st;
        }

        return false;
    }

    public function escape($data)
    {
        return addslashes($data);
    }

    public function error()
    {
        return @mysqli_error($this->st);
    }

    public function close()
    {
        return mysqli_close($this->st);
    }

    public function version()
    {
        $res = $this->query("SELECT VERSION()")->fetchField();

        return ($res) ? (float)$res : false;
    }

    public function databases()
    {
        return false;
    }

    public function dump($table)
    {
        // TODO: Implement dump() method.
    }

    public function select_db($db)
    {
        return @mssql_select_db($db, $this->st);
    }

    public function tables()
    {
        return false;
    }

    public function schema($table)
    {
        return false;
    }

    public function _query($query)
    {
        return ($this->st) ? mssql_query($query, $this->st) : false;
    }

    public function fetch($r, $type = 'assoc')
    {

        return ($r) ? mssql_fetch_array($r, ifset($this->result_type, $type, 'both')) : debug_print_backtrace(2, 4);
    }
}

