<?php

/*TODO ITERATOR */

class gaDBResult
{
    protected $r = false;
    protected $a = null;
    protected $query = false;
    protected $error = false;

    public function __construct($adapter, $query)
    {
        $this->a = $adapter;
        $this->query = $query;
        $this->r = $adapter->_query($query);
        if (!$this->r) {
            $this->error = $adapter->error();
        }
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function fetch($type = 'assoc')
    {
        return ($this->r) ? $this->a->fetch($this->r, $type) : false;
    }

    public function error()
    {
        return $this->error;
    }

    public function fetchAll($key = false, $normalize = false)
    {
        $rows = [];
        if ($key) {
            while ($row = $this->fetch()) {
                $index = $row[$key];
                if ($normalize) {
                    unset($row[$key]);
                    if (count($row) == 1) {
                        $row = array_shift($row);
                    }
                }
                if ($normalize === 2) {
                    $rows[$index][] = $row;
                } else {
                    $rows[$index] = $row;
                }
            }
        } elseif ($normalize) {
            while ($row = $this->fetch()) {
                $rows[] = array_shift($row);
            }
        } else {
            while ($row = $this->fetch()) {
                $rows[] = $row;
            }
        }

        return $rows;
    }

    public function fetchField($name = false)
    {
        $data = $this->fetch();

        return ($data) ? ((!$name && is_array($data)) ? array_shift($data) : ifset($data, $name, false)) : false;
    }
}