<?php

class gaDatabaseModule extends gaModule
{
    protected static $_name = 'Sql';
    protected $error = '';
    protected $connect_data = [
        'type' => '',
        'host' => '',
        'port' => '',
        'user' => '',
        'pass' => '',
        'db'   => '',
    ];
    protected $query = [];
    protected $qresult = false;
    protected $connect = false;
    protected $db = false;
    protected $dbs = false;
    protected $list_table = false;

    public function execute()
    {
        $this->conn();
        $a = $this->getActionId();
        if ($this->connect) {
            if (in_array($a, ['listtable', 'query'])) {
                $this->query = ifempty(ga('r', 'param1'), false);
                if ($this->getActionId() == 'listtable') {
                    $page = ifempty(ga('r', 'param2'), 1);
                    $this->list_table = ['table' => $this->query, 'page' => ifempty(ga('r', 'param2'), 1)];
                    $this->query = ['table' => $this->query, 'limit' => [($page - 1) * 50, 50]];
                }
            }
            if (!empty($this->query)) {
                $this->qresult = $this->connect->query($this->query);
                $this->query = $this->qresult->getQuery();
            }
        }
        if ($a == 'download') {
            $f = ga('r', 'file');

            if (empty($f)) {
                ga()->config()->set('Action', 'default', true);
                $this->hideModule();
            }
        }
        if (!$this->actionExists($a)) {
            $this->setActionId('default');
        }
    }

    public function downloadAction()
    {
        $tables = ga('r', 'tbl');
        $f = ga('r', 'file');
        if (empty($f)) {
            header("Content-Disposition: attachment; filename=dump.sql");
            header("Content-Type: text/plain");
            foreach ($tables as $v) {
                $this->connect->dump($v);
            }

            exit;
        } elseif ($fp = @fopen($_POST['file'], 'w')) {
            foreach ($tables as $v) {
                $this->connect->dump($v, $fp);
            }
            fclose($fp);
        } else {
            echo 'NO CREATE DUMP!';
        }


    }

    /* TODO: идея плагна, сделать импорт результатов в .csv и insert sql */
    public function printTables($show_count = false)
    {
        $connect = $this->connect;
        if (($connect = $this->connect)) {
            echo "<span>Tables:</span><br><br>";
            $t = $connect->tables();
            array_map(function ($v) use ($connect, $show_count) {
                $n = ($show_count)
                    ? '<small>(' . $connect->query('SELECT COUNT(*) FROM ' . $v)->fetchField() . ')</small>' : '';
                $v = htsch($v);
                echo "<nobr><input type='checkbox' name='tbl[]' value='" . $v . "'>&nbsp;<a href=# onclick=\"st('" . $v . "',1)\">" . $v . "</a>&nbsp;$n</nobr><br>";
            }, $t);

            echo "<input type='checkbox' onclick='is();'>
              <input type=submit value='Dump' onclick='alert(34);ga.d.sf.Action.value=\"download\";fs(ga.d.sf);return false;'>
              <br>File path:<input type=text name=file value=''>";
        }

    }

    public function defaultAction()
    {
        $connect = $this->connect;
        $connect_data = $this->connect_data;
        $dbs = ($this->connect) ? ifisar($this->connect->databases(), []) : false;
        $f = gaForm::create();
        echo "<script>
                var db = '" . @addslashes(ifempty($this->connect_data, 'db', '')) . "';
                function fs(f) {
                    if(f.db.value!=db) { 
                        if(f.param1) f.param1.value='';
                        if(f.param2) f.param2.value='';
                        if(f.param3) f.param3.value='';
                    }
                    var a = f.Action.value;
                    if(a=='download') {
                         ga.request.submit(f,null,true);
                      f['Action'].value = 'default';
                     
                    } else{
                         ga.request.submit(f,null,true);
                    }
                    return false;
                }
                function st(t,p) {
                    ga.d.sf.Action.value = 'listTable';
                    ga.d.sf.param1.value = t;
                    if(p && ga.d.sf.param2)ga.d.sf.param2.value = p;
                     ga.d.sf.onsubmit = '';
                    ga.request.submit(ga.d.sf);
                }
                function is() {
                    for(i=0;i<ga.d.sf.elements['tbl[]'].length;++i)
                        ga.d.sf.elements['tbl[]'][i].checked = !ga.d.sf.elements['tbl[]'][i].checked;
                }
            </script>";
        echo '<form name="sf" onsubmit="fs(this);return false;" method="post">' . $f->f('Module', [
                't' => 'h',
                'v' => 'Database',
            ]) . $f->f('Action', ['t' => 'h', 'v' => 'default']) . $f->f('param1', [
                't' => 'h',
                'v' => '',
            ]) . $f->f('param2', ['t' => 'h', 'v' => '']) . $f->f('param3', ['t' => 'h', 'v' => '']) . '<table cellpadding="2" cellspacing="0">
            <tr><td>Type</td><td>Host</td><td>Login</td><td>Password</td><td>Database</td><td></td></tr>
            <tr>
            <td>' . $f->f('type', [
                't' => 's',
                'v' => ifempty($connect_data, 'type', ''),
                'o' => ['mysql' => 'MySql', 'pgsql' => 'PostgreSql'],
            ]) . '</td>
            <td>' . $f->f('host', ['v' => ifempty($connect_data, 'host', 'localhost')]) . '</td>
            <td>' . $f->f('user', ['v' => ifempty($connect_data, 'user', 'root')]) . '</td>
            <td>' . $f->f('pass', ['v' => ifempty($connect_data, 'pass', 'root')]) . '</td>
            <td>
            ' . $f->f('db', ($dbs ? [
                't' => 's',
                'v' => ifempty($connect_data, 'db', ''),
                'o' => array_combine($dbs, $dbs),
            ] : ['t' => 't', 'v' => ifempty($connect_data, 'db', '')])) . '</td>
            <td>
            ' . $f->f('pass', [
                't' => 'sb',/*'onclick'=>'fs(d.sf);',*/
                'v' => 'Connect',
            ]) . '
            ' . $f->f('count', [
                't' => 'cb',
                'v' => ifempty(ga('r', 'count'), '0'),
                'o' => ['1' => 'count the number of rows'],
            ]) . '</td>
            </tr></table>
            
            ';
        if ($connect && $this->db) {


            echo "<table width=100% cellpadding=2 cellspacing=0>
                    <tr>
                    <td width=1 style='border-top:2px solid #666;'>", $this->printTables(ifempty(ga('r', 'count'), false)), "</td></form>
                    <td style='border-top:2px solid #666;'>";
            if ($this->list_table) {
                $table = $this->list_table['table'];
                $page = $this->list_table['page'];
                $count = $connect->query('SELECT COUNT(*) as n FROM ' . $table)->fetchField();
                $pages = ceil($count / 50);
                echo "<span>" . $table . "</span> ($count records) Page # <input type=text name='page' value=" . ((int)$page) . "> of $pages";
                if ($page > 1) echo " <a href=# onclick='st(\"" . $table . '", ' . ($page - 1) . ")'>&lt; Prev</a>";
                if ($page < $pages) echo " <a href=# onclick='st(\"" . $table . '", ' . ($page + 1) . ")'>Next &gt;</a>";
                echo "<br><br>";
            }
            if ($this->qresult) {
                $this->printQueryResult($this->qresult);
            }
            echo "<br><form onsubmit='ga.d.sf.Action.value=\"query\";ga.d.sf.param1.value = this.query.value;return false;'>
<textarea name='query' style='width:100%;height:100px'>" . (ifempty($this->query, '')) . "</textarea><br><input type=submit value='submit'>
                    </form></td></tr></table>";
        } else {
            echo $this->error;
            echo "</form>";
            $cs = $this->storage('connections');
            if (!empty($cs)) {
                echo gaForm::create([
                    'Module'  => ['t' => 'h', 'v' => 'Database'],
                    'Action'  => ['t' => 'h', 'v' => 'default'],
                    'connect' => ['t' => 'h', 'v' => ''],
                ], ['name' => 'db_ch', 'submit' => false])->get();
                foreach ($cs as $name => $v) {
                    echo '<div><script>function db_conn(name){
    ga.d.db_ch.connect.value = name; 
    ga.request.submit(ga.d.db_ch);return false;
                            }</script>
                <a href="#" onclick="db_conn(\'' . $name . '\');">
                ' . ifset($v, 'host', '-') . ':' . ifset($v, 'user', '-') . ':' . ifset($v, 'db', '-') . '
                </a></div>';
                }
            }
        }


    }

    protected function conn()
    {
        $cs = $this->storage('connections');
        /* Если пришло соединение */
        if (ga('r', 'host')) {
            array_walk($this->connect_data, function (&$v, $k) {
                $v = trim(ga('r', $k, ''));
            });
            $name = md5(var_export($this->connect_data, true));
            if (!arkex($name, $cs)) {
                $cs[$name] = $this->connect_data;
                $this->storage('connections', $cs);
            }
            /* Если пришло название коннекта */
        } elseif (ga('r', 'connect') && isset($cs[ga('r', 'connect')])) {
            $this->connect_data = $cs[ga('r', 'connect')];
        }
        /* Если данные заполнены создаем коннект */
        if (!empty($this->connect_data['host'])) {
            $ch = ga()->config('Charset');
            $charsets = [
                "Windows-1251" => 'cp1251',
                "UTF-8"        => 'utf8',
                "KOI8-R"       => 'koi8r',
                "KOI8-U"       => 'koi8u',
                "cp866"        => 'cp866',
            ];
            $this->connect_data['charset'] = ifempty($charsets, $ch, false);

            $c = gaDB::create($this->connect_data);
            if ($c->isConnect()) {
                $this->connect = $c;
                $this->dbs = ($this->connect) ? ifisar($this->connect->databases(), []) : false;
                $this->db = ifempty($this->connect_data, 'db', '');
            } else {
                $this->error = $c->error();
            }
        }
    }

    protected function printQueryResult($res)
    {
        if ($res !== false) {
            $h = false;
            $e = $res->error();
            if ($e) {
                echo "<b>$e</b>";
            }
            echo '<table width=100% cellspacing=1 cellpadding=2 class=strips>';
            while ($r = $res->fetch()) {
                if (!$h) {
                    echo '<tr>';
                    foreach ($r as $k => $v) {
                        echo "<th>$k</th>";
                    }
                    echo '</tr><tr>';
                    $h = true;
                }
                echo '<tr>';
                foreach ($r as $k => $v) {
                    echo ($v == null) ? '<td><i>NULL</i></td>' : '<td>' . nl2br(htsch($v)) . '</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }
    }
}


