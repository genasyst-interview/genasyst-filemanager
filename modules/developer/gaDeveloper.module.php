<?php

class gaDeveloperModule extends gaModule
{
    protected static $_name = 'Developer';

    public function execute()
    {
        $this->setLayout(new gaSystemLayout());
    }

    public function defaultAction()
    {
        echo "This is default action!";
    }
}

class gaDeveloperTestLayout extends gaLayout
{
    public function execute()
    {

    }
}

class gaDeveloperTestAction extends gaViewAction
{
    protected static $_name = 'Test Developer Action';

    public function execute()
    {

    }

    public function view()
    {

    }
}