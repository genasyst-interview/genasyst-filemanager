<?php


class gaSearcherModule extends gaModule
{

    protected static $_name = 'Searcher';

    function execute()
    {
        if (!$this->actionExists($this->getActionId())) {
            $this->setActionId('default');
        }
    }

    public function getPublicActions()
    {
        $a = parent::getPublicActions();

        return ['default' => 'test menu action'] + $a;
    }

    function defaultAction()
    {
        echo "TODO: Надо сделать поиск файлов и тд...";
    }
}