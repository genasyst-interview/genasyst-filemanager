<?php

class gaFilesmanagerModule extends gaModule
{

    protected static $_name = 'Filemanager';

    protected $error = '';

    public function execute()
    {
        $f = ga('f');

        $a = $this->getActionId();

        $current_dir = ga('r', 'Directory');
        /* Если экшен листинга директории и не передена папка, то ставим папку по умолчанию из конфига системы */
        if ($a == 'default' && empty($current_dir)) {
            $current_dir = $f->getDir();
        }
        $f->setDir($current_dir, true);
        $dir = $f->getDir();
        $sa = $this->storage('Action');
        $p1 = ga('r', 'param1');

        if ($a == 'copy' || $a == 'move' || $a == 'tar' || $a == 'zip' || $a == 'unzip') {
            $files = ga('r', 'files');
            if (!empty($files)) {
                $this->storage('Directory', $dir);
                $this->storage('files', ifisar($files, []));
                $this->storage('Action', strtolower($a));
            } else {
                $this->error = 'Выберите файлы!';
            }
        } elseif ($a == 'paste') {
            $sd = $this->storage('Directory');
            $sf = ifisar($this->storage('files'), []);
            if ($sa == 'copy' || $sa == 'move') {
                foreach ($sf as $v) {
                    $f->copy($sd, $dir, $v, ($sa == 'move'));
                }
            } elseif ($sa == 'tar' || $sa == 'zip' || $sa == 'unzip') {
                if ($sa == 'unzip') {
                    $f->unZip($dir, $sd, $sf);
                } elseif (!empty($p1)) {
                    if ($sa == 'tar') {
                        $f->createTar($dir, $p1, $sd, $sf);
                    } elseif ($sa == 'zip') {
                        $f->createZip($dir, $p1, $sd, $sf);
                    }
                }
            }
            $this->storage('Directory', '');
            $this->storage('files', []);
            $this->storage('Action', '');
        } elseif ($a == 'createdir' && !empty($p1)) {
            $p1 = (strpos($f->cast($p1, false), '/') === false) ? $dir . $p1 : $p1;
            $f->createDir($p1);
        }
        if ($a == 'cancel') {
            $this->storage('Directory', '');
            $this->storage('files', []);
            $this->storage('Action', '');
        }
        if ($this->getActionId() == 'upload') {
            $f->uploadFiles();
            $this->setActionId('default');
        }
        if (!$this->actionExists($this->getActionId())) {
            $this->setActionId('default');
        }
    }

    public function defaultAction()
    {
        $config = ga()->config();
        $filesystem = ga()->files();
        $sort = ['name', 1];
        $p2 = ga('r', 'param2');
        if (!empty($p2) && preg_match('/sort_(.*[^_])_(\d)/', $p2, $s)) {
            $sort[0] = $s[1];
            $sort[1] = $s[2];
        }
        $dir = $config->get('Directory');
        $filesystem->setSort($sort);
        $depth = ifempty($this->cookie('listdir_depth'), 1);
        $files = $filesystem->getDirFiles($dir, ($depth > 1), $depth);
        if (!empty($this->error)) {
            echo '<div>' . $this->error . '</div>';
        }
        echo "<form name=files method=post><table width='100%' class='strips' cellspacing='0' cellpadding='2'>";
        echo "<tr><th width='13px'><input type=checkbox onclick='check_all_files()' class=chkbx></th>
				<th>" . gaForm::a(['Filesmanager', '', null, null, 'sort_name_' . ($sort[1] ? 0 : 1)], 'Name') . " / Показано: " . count($files) . "
				/ Глубина сканирования: " . gaForm::create()->f($this->getCookieName('listdir_depth'), [
                't'        => 's',
                'v'        => $depth,
                'o'        => ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'],
                'onchange' => 'ga.cookie.set(this.name,this.value);',
            ]) . "
				</th>
				<th>" . gaForm::a(['Filesmanager', '', null, null, 'sort_size_' . ($sort[1] ? 0 : 1)], 'Size') . "</th>
				<th>" . gaForm::a(['Filesmanager', '', null, null, 'sort_modify_' . ($sort[1] ? 0 : 1)], 'Modify') . "</th>
				<th>Owner/Group</th>
				<th>" . gaForm::a(['Filesmanager', '', null, null, 'sort_oct_perms_' . ($sort[1] ? 0 : 1)], 'Permissions') . "</th>
				<th>Actions</th>
			</tr>";
        if (isar($files)) {
            foreach ($files as $k => $v) {
                $fd = [($v['type'] == 'dir' ? 'Filesmanager' : 'Filetools'), 'Default',
                    ($v['type'] == 'dir' ? $v['path'] : ($v['type'] == 'link' ? dirname($v['link']) : dirname($v['path']))),
                    ($v['type'] == 'file' ? basename($v['path']) : ($v['type'] == 'link' ? basename($v['link']) : ''))];
                $fn = ($v['type'] == 'file' ? htsch($v['name']) : '<b>[ ' . htsch($v['name']) . ' ]</b>');
                echo '<tr><td><input type=checkbox name="files[]" value="' . urlencode($v['name']) . '" class=chkbx></td>
				<td>' . (($v['type'] == 'file' && $v['size'] > (1024 * 1024)) ? gaForm::p($fd, $fn) : gaForm::a($fd, $fn)) . '</td>
				<td>' . ($v['type'] == 'file' ? ga('h')->filesize($v['size']) : $v['type']) . '</td>
				<td>' . date('Y-m-d H:i:s', $v['modify']) . '</td>
				<td>' . $v['owner'] . '/' . $v['group'] . '</td>
				<td>' . gaForm::a(['Filetools', 'Chmod', dirname($v['path']), basename($v['path'])], ga('f')->filepermsHtml($v)) . ' ' . $v['oct_perms'] . '</td>
				<td>' . gaForm::a(['Filetools', 'Rename', dirname($v['path']), basename($v['path'])], 'R') . ' ' .
                    gaForm::a(['Filetools', 'Touch', dirname($v['path']), basename($v['path'])], 'T') .
                    ($v['type'] == 'file' ? ' ' . gaForm::a(['Filetools', 'Edit', dirname($v['path']), basename($v['path'])], 'E') . ' ' .
                        gaForm::p(['Filetools', 'Download', dirname($v['path']), basename($v['path'])], 'D') : '') . '</td></tr>';
            }
        } else {
            echo '<tr><td colspan=7>Not Read Directory</td></tr>';
        }
        echo "<tr>
				<td colspan=7>
					<input type=hidden name=Module value='Filesmanager'>
					<input type=hidden name=Directory value='" . htsch($dir) . "'>
					<input type=hidden name=Charset value='" . $config->get('Charset') . "'>
					<select name='Action'>
						<option value='copy'>Copy</option>
						<option value='move'>Move</option>
						<option value='delete'>Delete</option>";
        echo (class_exists('ZipArchive')) ? "<option value='zip'>Compress (zip)</option><option value='unzip'>Uncompress (zip)</option>" : '';
        echo (ga()->config()->isConsole()) ? "<option value='tar'>Compress (tar.gz)</option>" : '';
        $sa = $this->storage('Action');
        $sf = $this->storage('files');
        if (!empty($sa) && !empty($sf)) {
            switch ($sa) {
                case'tar':
                case 'zip':
                    $pt = 'Run compress files to ' . $sa;
                    break;
                default:
                    $pt = ucfirst($sa) . ' files in this folder';
            }
            echo "<option value='paste' selected>$pt (" . @count($sf) . ")</option><option value='cancel'>Cancel</option>";
        }
        echo "</select>&nbsp;";
        if ((($sa == 'zip') || ($sa == 'tar')) && @count($sf)) {
            echo "<input type=hidden name=ajax value='1'>";
            echo "file name: <input type=text name=param1 value='ga_" . date("Ymd_His") . "." . ($sa == 'zip' ? 'zip' : 'tar.gz') . "'>&nbsp;";
        }
        echo "<input type='submit' value='>>'>
				</td>
			</tr>
			</table>
			</form>";
    }

    public function deleteAction()
    {
        $f = ga('f');
        $dir = $f->getDir();
        foreach (ifisar(ga('r', 'files'), []) as $v) {
            if ($v == '..' || $v == '.' || empty($v)) {
                continue;
            }
            $f->delete($dir . $v);
        }
        $this->defaultAction();
    }
}
