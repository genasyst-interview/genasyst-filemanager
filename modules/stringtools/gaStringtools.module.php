<?php

class gaStringtoolsModule extends gaModule
{

    protected static $_name = 'String Tools';

    protected $tools = [
        'base64_encode'    => 'Base64 encode',
        'base64_decode'    => 'Base64 decode',
        'urlencode'        => 'Url encode',
        'urldecode'        => 'Url decode',
        'full_urlencode'   => 'Full urlencode',
        'md5'              => 'md5 hash',
        'sha1'             => 'sha1 hash',
        'crypt'            => 'crypt',
        'crc32'            => 'CRC32',
        'ascii2hex'        => 'ASCII to HEX',
        'hex2ascii'        => 'HEX to ASCII',
        'hexdec'           => 'HEX to DEC',
        'hex2bin'          => 'HEX to BIN',
        'dechex'           => 'DEC to HEX',
        'decbin'           => 'DEC to BIN',
        'binhex'           => 'BIN to HEX',
        'bindec'           => 'BIN to DEC',
        'mb_strtolower'    => 'String to lower case',
        'mb_strtoupper'    => 'String to upper case',
        'htmlspecialchars' => 'Htmlspecialchars',
        'strlen'           => 'String length (byte)',
        'mb_strlen'        => 'String length',
        'dump_json_decode' => 'Json decode',
        'nl2br'            => 'Enter to <br>',
    ];

    protected $text = '';

    protected $func = '';

    protected $ajax = false;

    protected $all = false;

    public function execute()
    {
        $this->text = $p1 = ga('r', 'param1');
        $this->all = $p3 = ga('r', 'param3');
        $this->func = $p2 = empty($this->all) ? ga('r', 'param2') : false;
        $ajax = ga('r', 'ajax');
        if (!empty($p1) && !empty($p3)) {
            $this->cookie('all', 1);
        } elseif (!empty($p1)) {
            $this->cookie('all', 0);
        }
        $this->all = $this->cookie('all');
        if (!empty($ajax)) {
            $this->cookie('ajax', 1);
            $this->hideModule();
            $this->setActionId('ajax');
        } else {
            if (!empty($p1)) {
                $this->cookie('ajax', 0);
            }
        }
        $this->ajax = $this->cookie('ajax');
        if (!$this->actionExists($this->getActionId())) {
            $this->setActionId('default');
        }
    }

    public function ajaxAction()
    {
        $r = [];
        if (!empty($this->text)) {
            $r = ['content' => $this->resultHtml($this->func)];
        }
        echo ga()->h()->json_encode($r);
    }

    public function defaultAction()
    {
        echo "<script>
				function stfText(t){
						if(t.form[_e('ajax')].checked){
						 stfSend(t.form);
						}
				}
				function stfSend(o)	{
					var res = function(r) {
						ga.d.getElementById('strt_result').innerHTML=r.content;
					};
					ga.request.form(o,null,null,null,o[_e('text')].value,o[_e('select')].value,o[_e('all')].checked?1:'',res);
				}
				</script>";
        $f = [
            'form'   => ['name' => 'stf', 'onsubmit' => 'stfSend(this); return false;', 'submit' => false],
            'fields' => [
                'select' => [
                    't'        => 's',
                    'onchange' => 'stfText(this); return false;',
                    'o'        => ifisar($this->tools, []),
                ],
                'all'    => ['t' => 'cb', 'v' => intval($this->all), 'o' => ['1' => 'Result all Tools']],
                'ajax'   => [
                    't' => 'cb',
                    'l' => ' ',
                    'v' => intval($this->ajax),
                    'o' => ['1' => 'send using AJAX'],
                ],
                'text'   => [
                    't'        => 'ta',
                    'onchange' => 'stfText(this); return false;',
                    'l'        => '<br><br>',
                    'v'        => !empty($this->text) ? $this->text : '',
                    's'        => 'width:80%;height:90px;',
                ],
                '_sb'    => ['t' => 'sb', 'l' => '<br>', 'v' => '>>'],
            ],
        ];
        echo gaForm::create($f['fields'], $f['form'])->get(1) . '<div id="strt_result">' . $this->resultHtml($this->func) . '</div>';

    }

    protected function resultHtml($f = false)
    {
        if (!function_exists('hex2bin')) {
            function hex2bin($p)
            {
                return decbin(hexdec($p));
            }
        }
        if (!function_exists('binhex')) {
            function binhex($p)
            {
                return dechex(bindec($p));
            }
        }
        if (!function_exists('dump_json_decode')) {
            function dump_json_decode($p)
            {
                return var_export(@json_decode($p, true), true);
            }
        }

        if (!function_exists('hex2ascii')) {
            function hex2ascii($p)
            {
                $r = '';
                for ($i = 0; $i < strLen($p); $i += 2) {
                    $r .= chr(hexdec($p[$i] . $p[$i + 1]));
                }

                return $r;
            }
        }
        if (!function_exists('ascii2hex')) {
            function ascii2hex($p)
            {
                $r = '';
                for ($i = 0; $i < strlen($p); ++$i) {
                    $r .= sprintf('%02X', ord($p[$i]));
                }

                return strtoupper($r);
            }
        }
        if (!function_exists('full_urlencode')) {
            function full_urlencode($p)
            {
                $r = '';
                for ($i = 0; $i < strlen($p); ++$i) {
                    $r .= '%' . dechex(ord($p[$i]));
                }

                return strtoupper($r);
            }
        }
        if (empty($this->text)) {
            return '';
        }
        $r = '<table width="80%" class="strips">';
        foreach ($this->tools as $k => $v) {
            if (($f && $f != $k) || !funex($k)) continue;
            $t = htsch((string)@$k($this->text), ENT_QUOTES);
            $r .= "<tr><td width='240px'>" . htsch($v) . "</td><td>" . ((mb_strlen($this->text) > 30)
                    ? "<textarea readonly style='width: 80%'>$t</textarea>"
                    : "<input type='text' value='$t'>") . '</td></tr>';
        }

        return $r . '</table>';
    }

}