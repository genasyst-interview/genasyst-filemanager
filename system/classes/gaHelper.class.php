<?php

class gaHelper
{
    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_STRING = 'string';
    const TYPE_STRING_TRIM = 'string_trim';
    const TYPE_ARRAY_INT = 'array_int';
    const TYPE_ARRAY = 'array';
    const TYPE_FILESIZE = 'fsize';

    protected $json_replaces = [
        [
            ['\u0410', '\u0430', '\u0411', '\u0431', '\u0412', '\u0432',
                '\u0413', '\u0433', '\u0414', '\u0434', '\u0415', '\u0435', '\u0401', '\u0451', '\u0416',
                '\u0436', '\u0417', '\u0437', '\u0418', '\u0438', '\u0419', '\u0439', '\u041a', '\u043a',
                '\u041b', '\u043b', '\u041c', '\u043c', '\u041d', '\u043d', '\u041e', '\u043e', '\u041f',
                '\u043f', '\u0420', '\u0440', '\u0421', '\u0441', '\u0422', '\u0442', '\u0423', '\u0443',
                '\u0424', '\u0444', '\u0425', '\u0445', '\u0426', '\u0446', '\u0427', '\u0447', '\u0428',
                '\u0448', '\u0429', '\u0449', '\u042a', '\u044a', '\u042b', '\u044b', '\u042c', '\u044c',
                '\u042d', '\u044d', '\u042e', '\u044e', '\u042f', '\u044f'],
            ['А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
                'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о',
                'П', 'п', 'Р', 'р', 'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш',
                'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я'],
        ],
    ];

    public static function _castValue($val, $type = null)
    {
        $type = trim(strtolower($type));
        switch ($type) {
            case self::TYPE_INT:
                return (int)$val;
            case self::TYPE_FLOAT:
                return (float)$val;
            case self::TYPE_STRING_TRIM:
                return trim(self::castValue($val, self::TYPE_STRING));
            case self::TYPE_ARRAY_INT:
                if (!is_array($val)) {
                    $val = explode(",", $val);
                }
                foreach ($val as &$v) {
                    $v = self::castValue($v, self::TYPE_INT);
                }
                reset($val);

                return $val;
            case self::TYPE_FILESIZE:
                $val = self::castValue($val, self::TYPE_INT);
                if ($val <= 1024) {
                    return $val . ' b';
                } elseif ($val <= 1048576) {
                    return round($val / 1024, 2) . ' Kb';
                } elseif ($val <= 1073741824) {
                    return round($val / 1048576, 2) . ' Mb';
                } else {
                    return round($val / 1073741824, 2) . ' Gb';
                }
            case self::TYPE_STRING:
                if (is_array($val)) {
                    $val = reset($val);
                    if (is_array($val)) {
                        $val = null;
                    }
                }
                break;
            case self::TYPE_ARRAY:
                if (!is_array($val)) {
                    $val = (array)$val;
                }
                break;
        }

        return $val;
    }

    public static function castValue($val, $type = null)
    {
        return static::_castValue($val, $type);
    }

    public function filesize($size)
    {
        return static::_castValue($size, 'fsize');
    }

    public function jsParam($s, $q = "'")
    {
        if ($s === null):
            $s = 'null';
        elseif ($s === false):
            $s = 'false';
        elseif ($s === true):
            $s = 'true';
        else :
            $s = $q . (urlencode($s)) . $q;
        endif;

        return $s;
    }

    public function json_encode($data)
    {
        $data = json_encode($data);
        foreach ($this->json_replaces as $v) {
            $data = str_replace($v[0], $v[1], $data);
        }

        return $data;
    }
}