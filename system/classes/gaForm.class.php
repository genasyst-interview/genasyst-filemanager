<?php

class gaForm
{
    protected $crypt = false;

    public function crypt($bool = true)
    {
        $this->crypt = $bool;

        return $this;
    }

    public static function a($params = [], $title = '', $attr = '')
    {
        return self::l($params, $title, $attr, 'a');
    }

    public static function p($params = [], $title = '', $attr = '')
    {
        return self::l($params, $title, $attr, 'p');
    }

    public static function l($params = [], $title = '', $attr = '', $js_func_name = 'a')
    {
        $r = '<a href="#" onclick="' . $js_func_name . '(';
        foreach ($params as $v) {
            $r .= ga('h')->jsParam($v) . ",";
        }
        $a = '';
        foreach (ifisar($attr, []) as $k => $v) {
            $a .= $k . '="' . $v . '" ';
        }

        return rtrim($r, ',') . "); return false;\" " . $a . ">" . $title . "</a>";
    }

    public function f($k, $f)
    {
        return $this->buildField($k, $f);
    }

    public static function create($fields = [], $form = [])
    {
        return new static($fields, $form);
    }

    protected $form = [];
    protected $form_type = '';
    protected $pattern = '';
    protected $fields = [];
    protected $hidden_fields = [];
    protected $short_keys = [
        't' => 'type',
        'l' => 'label',
        'v' => 'value',
        'o' => 'options',
        'c' => 'class',
        's' => 'style',
    ];
    protected $specific_keys = ['value' => '', 'options' => []];
    protected $field_patterns = [
        'default' => '{label} {field}<br>',
        'table'   => '<tr><td>{label}</td><td>{field}</td></tr>',
        'inline'  => '<nobr>{label} {field}</nobr>',
        'div'     => '<div class="form-field"><div class="form-title">{label}</div><div class="form-value">{field}</div></div>',
    ];

    public function __construct($fields = [], $form = [])
    {
        $form = isar($form) ? $form : [];
        foreach (['type' => 'inline', 'method' => 'post', 'action' => ''] as $k => $v) {
            $form[$k] = ifset($form, $k, $v);
        }
        $submit = $files = false;
        foreach ($fields as $k => &$v) {
            if (!is_array($v)) {
                continue;
            }
            $v['type'] = isset($v['t']) ? ifset($v, 't', 'text') : ifset($v, 'type', 'text');
            $v['label'] = isset($v['l']) ? ifset($v, 'l', '') : ifset($v, 'label', '');
            $v['value'] = isset($v['v']) ? ifset($v, 'v', '') : ifset($v, 'value', '');
            unset($v['t'], $v['v']);
            if ($v['type'] == 'hidden') {
                $this->hidden_fields[$k] = $v;
                unset($fields[$k]);
            }
            $submit = (!$submit) ? ($v['type'] == 'submit' || $v['type'] == 'sb') : true;
            $files = (!$files) ? ($v['type'] == 'file') : true;
        }
        unset($v);
        if (isset($form['submit']) && empty($form['submit'])) {
            $submit = true;
        }
        unset($form['submit']);
        $submit = ifempty($form, 'submit', $submit);
        if (!$submit) {
            $fields[] = ['type' => 'submit', 'value' => '>>'];
        }
        $this->form_type = $form['type'];
        $this->pattern = ifset($form, 'field_pattern', ifset($this->field_patterns, $this->form_type, 'default'));
        unset($form['type'], $form['field_pattern']);
        $form['enctype'] = $files ? 'multipart/form-data' : 'application/x-www-form-urlencoded';
        $this->form = $form;
        $this->fields = $fields;
    }

    public function get($crypt = false)
    {
        $html = '';
        if ($crypt) {
            $this->crypt = true;
        }
        $f = $this->form;
        if (!isset($f['name'])) {
            $f['name'] = md5(microtime());
        }
        foreach ($this->hidden_fields as $k => $v) {
            $html .= $this->buildField($k, $v);
        }
        foreach ($this->fields as $k => $v) {
            $html .= $this->buildField($k, $v);
        }
        if ($this->form_type == 'table') {
            $html = '<table>' . $html . '</table>';
        }
        $r = '<form ';
        foreach ($f as $k => $v) {
            $r .= $k . '="' . $v . '" ';
        }

        return $r . '>' . $html . '</form>';
        /* return $r.'>'.$fields_html.'</form>'.
             (($crypt)?'<script>ga.crypt.add("'.$f['name'].'",'.ga()->h()->json_encode(ga()->crypt()->getAll($f['name'])).')</script>':'');*/
    }

    public function buildField($k, $f)
    {
        if (!is_array($f)) {
            return $f;
        }
        $f['type'] = isset($f['t']) ? ifset($f, 't', 'text') : ifset($f, 'type', 'text');/*???*/
        $f['value'] = isset($f['v']) ? ifset($f, 'v', '')
            : ifset($f, 'value', '');/*??? ($this->short_keys as $k=>$v) -?*/
        unset($f['t'], $f['v']);
        $html = $this->htmlField(($this->crypt ? _e($k) : $k), $f);

        return ($f['type'] == 'hidden')
            ? $html
            : str_replace(['{label}', '{field}'], [
                ifset($f, 'label', ''),
                $html,
            ], $this->pattern);
    }

    protected function prepareKeys($key, &$d)
    {
        $r = [];
        $d['name'] = $key;
        foreach ($this->short_keys as $k => $v) {
            if (arkex($k, $d)) {
                $d[$v] = $d[$k];
                unset($d[$k]);
            }
        }
        foreach ($this->specific_keys as $k => $v) {
            $r[$k] = ifset($d, $k, $v);
            unset($d[$k]);
        }
        foreach ([
                     't'  => 'text',
                     'p'  => 'password',
                     'n'  => 'number',
                     'h'  => 'hidden',
                     'f'  => 'file',
                     'b'  => 'button',
                     'ta' => 'textarea',
                     's'  => 'select',
                     'r'  => 'radio',
                     'cb' => 'checkbox',
                     'c'  => 'custom',
                     'sb' => 'submit',
                 ] as $s => $tr) {
            if ($d['type'] == $s) {
                $d['type'] = $tr;
                break;
            }
        }

        return $r;
    }

    protected function htmlField($key, $f)
    {
        $sf = $this->prepareKeys($key, $f);
        $attr = '';
        foreach ($f as $k => $v) {
            $attr .= $k . '="' . $v . '" ';
        }
        $val = $sf['value'];
        $r = '';
        switch ($f['type']) {

            case 't':
            case 'text':
            case 'password':
            case 'number':
            case 'hidden':
            case 'file':
            case 'button':
                $r = "<input $attr value='" . htsch($val) . "' >";
                break;
            case 'ta':
            case 'textarea':
                $r = "<textarea $attr >" . htsch($val) . "</textarea>";
                break;
            case 's':
            case 'select':
                $r = "<select $attr >";
                foreach ($sf['options'] as $k => $v) {
                    $r .= isar($v) ? '<optgroup label="' . htsch($k) . '"></optgroup>'
                        : "<option value='" . htsch($k) . "' " . ($k == $val ? 'selected' : '') . ">$v</option>";
                }
                $r .= '</select>';
                break;
            case 'r':
            case 'radio':
                foreach ($sf['options'] as $k => $v) {
                    $r .= "<input $attr value='" . htsch($k) . "' " . ($k == $val ? 'checked' : '') . " /> $v";
                }
                break;
            case 'cb':
            case 'checkbox':
                foreach ($sf['options'] as $k => $v) {
                    $r .= "<input  type='checkbox' name='{$f['name']}" . (count($sf['options']) > 1 ? "[$k]"
                            : '') . "' value='" . htsch($k) . "' " . (((isar($val) && in_array($k, $val)) || ($k == $val))
                            ? 'checked' : '') . " /> $v";
                }
                break;
            case 'sb':
            case 'submit':
                $attr = '';
                foreach ($f as $k => $v) {
                    $attr .= ($k == 'name') ? '' : $k . '="' . $v . '" ';
                }
                $r = "<input $attr value='$val' >";
                break;
            case 'custom':
                $r = $val;
                break;
        }

        return $r;
    }
}