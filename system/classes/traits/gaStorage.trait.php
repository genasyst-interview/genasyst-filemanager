<?php

trait gaStorageTrait
{
    protected $data = [];

    public function getAll()
    {
        return $this->data;
    }

    public function get($key = '', $def = null)
    {
        return arget($this->data, $key, $def);
    }

    public function set($key = '', $data)
    {
        if (isar($key) && count($key) == 2) {
            $this->data[$key[0]][$key[1]] = $data;
        } else {
            $this->data[$key] = $data;
        }
    }

    public function has($key = '')
    {
        return arkex($key, $this->data);
    }

    public function remove($key)
    {
        unset($this->data[$key]);
    }
}