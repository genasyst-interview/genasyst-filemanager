<?php

interface gaLayoutIncludeInterface
{
    function setLayout(gaLayout $layout);

    function isShowLayout();

    function showLayout();

    function hideLayout();
}