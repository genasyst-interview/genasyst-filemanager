<?php

interface gaLayoutInterface extends gaViewInterface
{
    function setContent($content = null);

    function getContent();

    function execute();

    function executeAction($key, $obj);

    function setBlock($key, $value);

    function display($content_obj = null);

    function printContent();

    function printBlock($name);
}