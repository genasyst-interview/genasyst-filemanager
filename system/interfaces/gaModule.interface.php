<?php

interface gaModuleInterface extends
    gaLayoutInterface,
    gaLayoutIncludeInterface,
    gaModuleIncludeInterface
    /* iCookie, iSession*/
{
    public function __construct($params = null);

    public static function getName();

    public static function getId();

    public function setActionId($action = '');

    public function getActionId();

    public function setActionParams($key = '', $value);

    public function getActions();
}