<?php

interface gaActionInterface
{
    function __construct($params = []);

    function execute();

    function display();
}