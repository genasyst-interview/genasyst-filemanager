<?php

interface gaModuleIncludeInterface
{
    function isShowModule();

    function showModule();

    function hideModule();
}