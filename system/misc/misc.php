<?php
define('true', true, true);
define('false', false, true);
define('null', null, true);
/* is_array() */
function isar($a)
{
    return is_array($a);
}

/**
 * Проверяет наличие ячейки массива или сразу наличие нескольких
 * Решает проблему длинных проверок массива
 * пример замена такого кода:
 * if(array_key_exists('tmp_name', $_FILES)  && array_key_exists('name', $_FILES)  && && array_key_exists('error', $_FILES)) {
 *      ////code
 * }
 * на такой:
 * if(arkex(['tmp_name','name','error'], $_POST) {
 *    //// code
 * }
 * array_key_exists() multi
 */
function arkex($key, $a)
{
    if (isar($key)) {
        foreach ($key as $v) {
            if (!arkex($v, $a)) {
                return false;
            }
        }

        return true;
    }

    return isar($a) ? array_key_exists($key, $a) : false;
}

/* function_exists() */
function funex($name)
{
    return function_exists((string)$name);
}

/*  remove error is_dir() - Warning: is_dir(): open_basedir restriction in effect. File(/var/www/..../) is not within the allowed path(s): (/var/www/..../data:.) in */
function isdir($path = '')
{
    return @is_dir((string)$path);
}

/*  remove error is_writable() - Warning: is_writable(): open_basedir restriction in effect. File(/var/www/..../) is not within the allowed path(s): (/var/www/..../data:.) in */
function iswritable($path = '')
{
    return @is_writable((string)$path);
}

/*  remove error is_writable() - Warning: is_writable(): open_basedir restriction in effect. File(/var/www/..../) is not within the allowed path(s): (/var/www/..../data:.) in */
function isreadable($path = '')
{
    return @is_readable((string)$path);
}


/* remove error file_exists() - Warning: file_exists(): open_basedir restriction in effect. File(/var/www/..../) is not within the allowed path(s): (/var/www/..../data:.) in */
function fiex($name)
{
    return @file_exists((string)$name);
}

/* htmlspecialchars */
function htsch($string, $flags = ENT_SUBSTITUTE, $encoding = 'UTF-8', $double_encode = true)
{
    return htmlspecialchars((string)$string, $flags, $encoding, $double_encode);
}

/**
 * Получение вложенной ячейки массива с проверкой функцией, если передана
 * пример
 * $arr = [
 *          'key'=> [
 *              'sub_key'=> [
 *                  'empty_key' => 0,
 *                  'int_key' => 12213,
 *                  'object_key' => new waModel();
 *               ]
 *           ]
 *  ];
 * arget($arr, 'key', 'default') return ['sub_key'=>....] - ячейка есть, вернулось ее значение
 * arget($arr, 'key', 'default', 'is_string') return 'default' - значение не прошло проверку функцией is_string, вернулось значение по умолчанию
 *
 * arget($arr, ['key','sub_key','int_key'], 'default',  'is_array') return 'default' - значение 12213 не прошло проверку функцией is_array
 * arget($arr, ['key','sub_key','no_exists_key_vrv34g'], 'default') return 'default' - ячейки не существует, вернулось значение по умолчанию
 *
 * arget($arr, ['key','sub_key','empty_key'], 'default') return 0 - ячейка есть, вернулось значение
 * arget($arr, ['key','sub_key','empty_key'], 'default',  'empty') return 'default' - значение не прошло проверку функцией
 *
 * arget($arr, ['key','sub_key','int_key'], 'default') return 12213 - ячейка есть, вернулось значение
 * arget($arr, ['key','sub_key','int_key'], 'default',  'is_array') return 'default' - значение 12213 не прошло проверку функцией is_array
 *
 *
 * @param mixed        $array   массив из которого нужно получить вложенную ячейку
 * @param array|string $key     ключи пути к ячейке или ключ ячейки в виде строки ['key','sub_key','sub_sub_key']
 * @param null         $default Значение по умолчанию при отсутствии нужной ячейки или не проходе проверки функцией
 * @param bool         $func    - функция проверки с которой бедет сравнение правильности полученной чейки, например empty()
 *
 * @return mixed|null
 */
function arget($array, $key, $default = null, $func = false)
{
    if (func_num_args() > 4) {
        $keys = func_get_args();
        $func = array_pop($keys);
        $default = array_pop($keys);
        $array = array_shift($keys);
    } else {
        $keys = isar($key) ? $key : (array)$key;
    }

    foreach ($keys as $segment) {
        if (is_object($array)) {
            if (!$array instanceof ArrayAccess || !$array->offsetExists($segment)) {
                return $default;
            }
        } elseif (!is_array($array) || (isar($array) && !array_key_exists($segment, $array))) {
            return $default;
        }
        $array = $array[$segment];
    }
    if ($func) {
        $array = valdef($array, $func, $default);
    }

    return $array;
}

/**
 * проверяет значение функцией, если не проходит возвращает значение по умолчанию
 * пример:
 *  valdef('string','is_array', 'default') return 'default'
 *  valdef(['key','key2'],'is_array', 'default') return ['key','key2']
 *
 *  valdef('0','empty', 'default') return 'default'
 *  valdef('no_empty_str','empty', 'default') return 'no_empty_str'
 *
 * @param      $value
 * @param      $func
 * @param null $default
 *
 * @return null
 */
function valdef($value, $func, $default = null)
{
    if (is_string($func) && !empty($func)) {
        switch (strtolower($func)) {
            case 'empty':
                $value = (!empty($value)) ? $value : $default;
                break;
            case 'isset':
                $value = !is_null($value) ? $value : $default;
                break;
            default:
                if (function_exists($func)) {
                    $value = (($func($value))) ? $value : $default;
                }

        }
    }

    return $value;
}

/**
 * @see ifempty()
 * @see ifset()
 * @see ifisar()
 *
 * @param        $var
 * @param null   $key
 * @param null   $def
 * @param string $func
 *
 * @return mixed|null
 */
function iffunc($var, $key = null, $def = null, $func = 'isset')
{
    if (func_num_args() < 4) {
        $func = $def;
        $def = $key;

        return valdef($var, $func, $def);
    } elseif (func_num_args() == 4) {
        return arget($var, $key, $def, $func);
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return arget($var, $key, $def, $func);
    }
}

/**
 * Проверяет существование ячейки массива и сравнивает через is_null  или возвращает переданное значение по умолчанию
 * используется для проверки массивов или сорращения конструкций
 *
 * примеры:
 *
 * if(isset($_POST['value'])) {
 *     $val = $_POST['value'];
 * } else {
 *     $val = '';
 * }
 * ====== можно так:
 * $val = ifset($_POST, 'value','');
 *
 *
 * if(isset($_POST['files']) && isset(isset($_POST['files']['upload']))) {
 *     $files = $_POST['files']['upload'];
 * } else {
 *     $files = array();
 * }
 * ====== можно так:
 * $files = ifset($_POST, ['files', 'upload'], array() );
 *
 *
 * @param      $var
 * @param null $key
 * @param null $def
 *
 * @return mixed|null
 */
function ifset($var, $key = null, $def = null)
{
    if (func_num_args() < 3) {
        return iffunc($var, $key, 'isset');
    } elseif (func_num_args() == 3) {
        return iffunc($var, $key, $def, 'isset');
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return iffunc($var, $key, $def, 'isset');
    }
}

/**
 * Проверяет существование ячейки массива и сравнивает значение через empty() или возвращает переданное значение по умолчанию
 * используется для проверки массивов или сорращения конструкций
 *
 * примеры:
 *
 * if(isset($_POST['value']) && !empty($_POST['value'])) {
 *     $val = $_POST['value'];
 * } else {
 *     $val = 'def';
 * }
 * ====== можно так:
 * $val = ifempty($_POST, 'value','def');
 *
 *
 * if(isset($_POST['files']) && isset(isset($_POST['files']['upload']))  && !empty($_POST['files']['upload']) {
 *     $files = $_POST['files']['upload'];
 * } else {
 *     $files = array();
 * }
 * ====== можно так:
 * $files = ifempty($_POST, ['files', 'upload'], array());
 *
 * !!!!! Важно, функция не предназначена для проверки существования простых переменных, хотя проверяет и обычные на is_null
 *
 * @param      $var
 * @param null $key
 * @param null $def
 *
 * @return mixed|null
 */
function ifempty($var, $key, $def = null)
{
    if (func_num_args() < 3) {
        return iffunc($var, $key, 'empty');
    } elseif (func_num_args() == 3) {
        return iffunc($var, $key, $def, 'empty');
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return iffunc($var, $key, $def, 'empty');
    }
}

/**
 * Проверяет существование ячейки массива и сравнивает значение через is_array() или возвращает переданное значение по умолчанию
 * используется для проверки массивов или сорращения конструкций
 *
 * примеры:
 *
 * if(isset($_POST['value']) && is_array($_POST['value'])) {
 *     $val = $_POST['value'];
 * } else {
 *     $val = 'def';
 * }
 * ====== можно так:
 * $val = ifisar($_POST, 'value',array('1','2'));
 *
 *
 * if(isset($_POST['files']) && isset(isset($_POST['files']['upload']))  && !is_array($_POST['files']['upload']) {
 *     $files = $_POST['files']['upload'];
 * } else {
 *     $files = array('def_file'=>array('tmp_name'...));
 * }
 * ====== можно так:
 * $files = ifisar($_POST, ['files', 'upload'], array('def_file'=>array('tmp_name'...))  );
 *
 * @param      $var
 * @param null $key
 * @param null $def
 *
 * @return mixed|null
 */
function ifisar($var, $key, $def = null)
{
    if (func_num_args() < 3) {
        return iffunc($var, $key, 'is_array');
    } elseif (func_num_args() == 3) {
        return iffunc($var, $key, $def, 'is_array');
    } else {
        $keys = func_get_args();
        $var = array_shift($keys);
        $def = array_pop($keys);
        $key = $keys;

        return iffunc($var, $key, $def, 'is_array');
    }
}

