<?php

abstract class gaModule implements gaModuleInterface
{
    use gaActionTrait, gaLayoutTrait, gaLayoutIncludeTrait, gaModuleIncludeTrait;
    protected $_action_params = [];
    protected $_action_name = 'default';
    protected $_default_action_name = 'default';

    public function __construct($params = null)
    {
        $this->setLayout(new gaSystemLayout());
        $this->init($params);
    }

    protected function init($params = null)
    {
    }

    protected function event($name, &$data)
    {
        return ga()->event(static::getId() . '_' . $name, $data);
    }

    public function menu($c = 'menu-h')
    {
        $menu = '<ul ' . (!empty($c) ? ' class="' . $c . '"' : '') . '>';
        foreach ($this->getPublicActions() as $id => $v) {
            $menu .= '<li>' . gaForm::a([static::getId(), $id, ga('f')->getDir()], $v) . '</li>';
        }

        return $menu . '</ul>';
    }

    public function getPublicActions()
    {
        $a = [];
        foreach ($this->getActions() as $id => $v) {
            $c = $this->getActionClass($id);
            if ($v == 'class' && method_exists($c, 'getName')) {
                $t = $c::getName();
                if (!empty($t)) {
                    $a[$id] = $t;
                }
            }/* TODO: Публичные экшены функций могут быть добавлены в самом классе модуля */
        }

        return $a;
    }

    public function setActionId($action = '', $cookie = false)
    {
        ga()->config()->set('Action', $action, $cookie);
        $this->_action_name = $action;
    }

    public function getActionId()
    {
        return strtolower($this->_action_name);
    }

    public function setActionParams($key = '', $value)
    {
        if (is_array($key)) {
            $this->_action_params = $key;
        } else {
            $this->_action_params[$key] = $value;
        }
    }

    public function getActions($lower = false)
    {
        $actions = [];
        $module_id = static::getId();
        $parent_methods = array_flip(get_class_methods(get_parent_class($this)));
        foreach (get_class_methods($this) as $method) {
            if (preg_match('/(.*)Action$/i', $method, $method_data)) {
                if (!isset($parent_methods[$method_data[0]])) {
                    $action = $lower ? strtolower($method_data[1]) : $method_data[1];
                    $actions[$action] = 'method';
                }
            }
        }
        foreach (get_declared_classes() as $method) {
            if (preg_match('/ga' . strtolower($module_id) . '(.+)Action$/i', $method, $method_data) && strtolower($method_data[0]) != strtolower(get_class($this))) {
                $action = $lower ? strtolower($method_data[1]) : $method_data[1];
                $actions[$action] = 'class';
            }
        }

        return $actions;
    }

    public function actionExists($action_name)
    {
        return arkex(strtolower($action_name), $this->getActions(true));
    }

    protected function getActionClass($action_name = '')
    {
        return preg_replace('@module@i', '', get_class($this)) . ucfirst($action_name) . 'Action';
    }

    public function prepareCurrentAction()
    {
        $content = null;
        if (isset($this->_blocks['__content'])) {
            $content = $this->getContent();
        } else {
            $as = $this->getActions(true);
            $an = $this->_action_name;
            if (!empty($an) && isset($as[strtolower($an)])) {
                if ($as[strtolower($an)] == 'class') {
                    $action_class = $this->getActionClass($this->_action_name);
                    if (class_exists($action_class)) {
                        $content = new $action_class($this->_action_params);
                        $content->setModule($this);
                        $content->execute();
                    }
                }
            }
        }
        if ($content && is_object($content)) {
            if (!$content->isShowModule()) {
                $this->hideModule();
            }
            $content->hideModule();// избегаем рекурсии
            $this->setContent($content);
        }
    }

    public function printContent()
    {
        $content = $this->getContent();
        if (!empty($content)) {
            if (is_object($content)) {
                $content->display();
            } else {
                echo $content;
            }
        } else {
            $action_method = strtolower($this->_action_name) . 'Action';
            if (method_exists($this, $action_method)) {
                $this->$action_method();
            } else {
                echo 'ERROR: Action - ' . $this->_action_name . ' (' . get_class($this) . '::' . $action_method . ') not found';
            }
        }

        return '';
    }

    public function fetch()
    {
        ga()->sendHeaders();
        $this->isShowModule() ? $this->view() : $this->printContent();
    }

    public function display($content_obj = null)
    {
        $this->execute();
        if (!empty($content_obj)) {
            $this->setContent($content_obj);
        }
        $this->prepareCurrentAction();
        if ($this->isShowModule()) {
            if ($this->isShowLayout() && is_object($this->_layout)) {
                $this->_layout->display($this);
            } else {
                $this->fetch();
            }
        } else {
            $this->fetch();
        }
    }

    public function view()
    {
        echo '<h1>' . self::getName() . '</h1><div class=menu>',
        $this->menu(),
        '</div><div class=content>',
        $this->printContent(),
        '</div>';
    }
}