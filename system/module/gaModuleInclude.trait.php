<?php

trait gaModuleIncludeTrait
{
    protected $_show_module = true;

    public function isShowModule()
    {
        return $this->_show_module;
    }

    public function showModule()
    {
        $this->_show_module = true;
    }

    public function hideModule()
    {
        $this->_show_module = false;
    }
}