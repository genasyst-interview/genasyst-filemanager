<?php

class gaConfig extends gaStorage
{

    protected $default_config = [
        'Charset'     => 'UTF-8',
        'Module'      => 'Filesmanager',
        'Action'      => 'Default',
        'Directory'   => '',
        'File'        => '',
        'Charsets'    => ['UTF-8' => 'UTF-8', 'Windows-1251' => 'Windows-1251', 'KOI8-R' => 'KOI8-R', 'KOI8-U' => 'KOI8-U', 'cp866' => 'cp866'],
        '__storage__' => 'session',
    ];

    protected $registered_factories = [
        'server'     => ['class' => 'gaServer'],
        /* 'filesystem' => ['class' => 'gaFilesystemPhp'],*/
        'filesystem' => ['class' => 'gaFilesystemCallback'],
        'request'    => ['class' => 'gaRequest'],
        'helper'     => ['class' => 'gaHelper'],
        'cookie'     => ['class' => 'gaCookie'],
        'session'    => ['class' => 'gaSession'],
        'console'    => ['class' => 'gaConsole'],
    ];


    protected $crypt = null;

    public function __construct($data)
    {
        if (ifisar($data, 'factories')) {
            foreach (ifisar($data, 'factories', []) as $factory => $factory_data) {
                $this->registered_factories[$factory] = $factory_data;
            }
        }
        $this->crypt = new gaCrypt(ifempty($data, 'ga_crypt', null)); /*TODO:use factory*/
        $this->init($data);
    }

    public function set($key = '', $data, $cookie = false)
    {
        parent::set($key, $data);
        if ($cookie) {
            if (is_string($data)) {
                ga()->c($key, $data);
            }
        }
    }

    public function crypt()
    {
        return $this->crypt;
    }

    public function getDefault($key = '')
    {
        return ifset($this->default_config, $key, null);
    }

    public function getFactories()
    {
        return $this->registered_factories;
    }

    protected function init($data = null)
    {
        //debug_print_backtrace(2,15);
        function _stripslashes($a)
        {
            return isar($a) ? array_map("_stripslashes", $a) : stripslashes($a);
        }

        if (get_magic_quotes_gpc()) {
            $_GET = _stripslashes($_GET);
            $_POST = _stripslashes($_POST);
            $_COOKIE = _stripslashes($_COOKIE);
            $_REQUEST = _stripslashes($_REQUEST);
        }
        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (strtolower(substr($name, 0, 5)) == 'http_') {
                        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }

                return $headers;
            }
        }

        $this->crypt->decodeArray($_GET);
        $this->crypt->decodeArray($_POST, true);
        $this->crypt->decodeArray($_COOKIE, true);

        /* Header request если в запросе мало данных , то данные передаем через заголовок в гет запросе */
        $header_data = ifempty(getallheaders(), 'Authorization', false);
        if ($header_data && $this->crypt->isEncode($header_data)) {
            $post_data = @json_decode($this->crypt->decode($header_data), true);
            $_POST = $this->crypt->decodeArray($post_data, true);
        }
        /*default settings */
        $cookie = gaCookie::getInstance();
        foreach ($this->default_config as $k => $v) {
            if (isset($_POST[$k]) && !empty($_POST[$k])) {
                parent::set($k, $_POST[$k]);
                if (is_string($_POST[$k])) {
                    $cookie->set($k, $_POST[$k]);
                }
            } elseif (isset($_COOKIE[$k])) {
                parent::set($k, $_COOKIE[$k]);
                //$this->set($k, $_COOKIE[$k]);
            } else {
                parent::set($k, $v);
            }
        }

    }

    public function hash()
    {
        return $this->crypt->key();
    }

    public function isConsole()
    {
        return $this->get('is_console');
    }
}