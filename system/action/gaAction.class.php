<?php

abstract class gaAction implements gaActionInterface
{
    use gaActionTrait, gaStorageTrait;
    protected $params = [];

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function display()
    {
        $this->execute();

        return $this->data;
    }
}