<?php

abstract class gaViewAction extends gaAction implements gaViewActionInterface
{
    use gaLayoutIncludeTrait, gaLayoutTrait, gaModuleIncludeTrait;
    protected $_module = null;

    public function setModule(/*gaModule*/
        $module)
    {
        $this->_module = $module;
    }

    public function fetch()
    {
        ga()->sendHeaders();
        $this->view();
    }

    public function display()
    {
        $module = $this->_module;
        if ($this->isShowModule() && is_object($module)) {
            $module->display($this);
        } else {
            $l = $this->getLayout();
            if ($l && $this->isShowLayout()) {
                $this->hideLayout();
                $this->_layout = null;
                $l->display($this);
            } else {
                $this->fetch();
            }
        }
    }
}