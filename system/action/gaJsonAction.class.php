<?php

abstract class gaJsonAction extends gaAction implements gaJsonActionInterface
{
    use gaModuleIncludeTrait;
    protected $errors = '';

    public function display()
    {
        ga()->sendHeaders();
        $data = (empty($this->errors))
            ? ['status' => 'ok', 'data' => $this->data] : [
                'status' => 'fail',
                'errors' => $this->errors,
                'data'   => $this->data,
            ];
        echo ga('h')->json_encode($data);
    }
}
