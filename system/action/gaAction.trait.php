<?php

trait gaActionTrait
{
    protected static $_name = null;

    public static function getName()
    {
        return ifempty(static::$_name, false);
    }

    public static function getId()
    {
        return substr(substr(get_called_class(), 0, -6), 2);
    }

    public function cookie($key = '', $val = null)
    {
        $key = ['module', strtolower(static::getId()), $key];
        if (func_num_args() == 1) {
            return ga()->cookie()->get($key, null);
        }

        return ga()->cookie()->set($key, $val);
    }

    public function getCookieName($key = '')
    {
        return implode('_', ['module', strtolower(static::getId()), $key]);
    }

    public function storage($key = null, $val = null)
    {
        $c = ga()->storage()->get(static::getId(), []);
        if (func_num_args() == 1) {
            return ifset($c, $key, null);
        }
        $c[$key] = $val;
        ga()->storage()->set(static::getId(), $c);
    }
}