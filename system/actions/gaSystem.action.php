<?php

class gaSystemAction extends gaViewAction
{

    public function execute()
    {

    }

    public function view()
    {

        ?>
        <script>
            function setSetting(i, v) {
                n = i.name;
                if (i.checked) {
                    v = v || i.value;
                    ga.cookie.set(n, i.value)
                } else {
                    ga.cookie.delete(n);
                }
            }

            function show_system_action() {
                var d = ga.d.getElementById('system_action');
                d.style.display = '';
            }
        </script>
        <div style="position: absolute;right:  5px; top: 5px;">
            <a href="#" onclick="show_system_action();return false;"> <img style="width: 24px"
                                                                           src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAEd0lEQVR42pWWa0xbZRjHW6Cc0iJVEihQEFoKnBboeoG24GAZRIeNmeM6jSxRE4FlLHIbQmf4IBt+WLzFjJvGuBhj5iUxRj/4ya+aSXRIXAaYqBtOTUyMEzpaev4+73toObAVkOTPCac9/9/z/t/nfQ4qlUol7Sa1Wi0JgiDl5uZKXq9XcrkOSMYcI91LlfZ6dlP8FxIpIyMDFosFVVVV8Pv9XD6fFyUlVhgMGdjt2U3dG6DVamE2m+FyOcnUJ5v75KtvE+R2u1BcXAydTrd/AEWC7OxsOJ1OqtQXr5pB2N9edo3f8/N7Lo8beaY8/mxCQFJSEtLT0+FwOFBbW7tlwKqvq0FDxxE888ULePqrIBpPBOCvr5U/q6mBr8bPxYrKzMzkXtsAsTj8isqYWLUNx4+gfbILp369gIHIJAYjUzh96xV0vHMSjZ2BeGQ+xYpYbHq9XgYQTWKVFxYWorq6Om5eFziElteeRdf8SxgKTWJImtmmgfAUeq5NoH26G3WPN8YhDOD2eGDKN0HNVpKcnCylpqby/BjVXlGOQF8bTl4dR//qmxiMTmMoOkWaweAG0zTdkyHsswGCdxMoMNJOK/aiorISNpuN9sQkRxUDxDLLysnCBz9+hgvhyxiOzHKT/rWL6Fl+GcfGnsLRs0+i5/p59IUuyoB1WsniBB4ebYXoKCdzkQNMMQCLSKPRxAG5OTlYXLyO3yN/4fPw1xi79TZaZ7pw8NhheGuog0h1Rw/jidkuvHhtDM2zz8HT5IdIxqJdhJ3M9wQsLy1iIxpFJBrGW5++B7PVAg+1oo+3qA+PHXJj6oQZc+fMeP14ARq8djItIwDJZueA/Pz8xICflpYQlSRIUhTvXrpEX1TzE13hqMRgswMLfffjn+EkrI2o8PdwMq72ZuD5gAUV5WIcUFBQsMsKlpcggf1IHKA8OL2PluH73nQyVnPAnVFZP/cLqHdbufk2ANtkIVVICPjo40+gbAKRoqj3lOCNtjz8eUbA+lkV140BDQFK4gCTsosEhcFOwMrKbxgfH0dRUZEMEMtQ5ynFqwT4Y1ibEJBDPnx08IhSUrYBlmiT+R4QIkqbHQqFsPDDPDo7O9HdJOK7XgPPPsTiCcoR/TIg4KAioqysLAVAsQcpBGtpacaVK99gdW2VNlriikphhCN3cDuowVpQHTe+PaLG/Gk9zjQVoLy8DDbqJAZgA5MDWERpaWk7BxRlmIdgcBRzc99ifX2dr2iD2nYtmIIQmf9Lxgun0nCxzYhHqikau00WmdvtdhiNxth0VfEV6PU62kzNXePW6TyAifPncPPmDYQ5IBkrgxrMdmSjrdZCpqw1bVsAUuGDhVvDLjauGY2txGAwQBlZ7OXDRvHlD9/Hl925aH2oGI6KMnksxKsmiSKdl/vuHtc7Xzh6ekNpBeGeL5BSMuNjgc8cupJpaUnpVub/55Wp1Qp8NcqKeLWbslqtvN8VcewfEBM7ZDpdGui/CBlAFbNNZCd1D+P9AWKxsZVkZj7AqzbQXNrZdYn0H6PXtko4NFjzAAAAAElFTkSuQmCC">
            </a></div>
        <div style="width: 100%;margin: 0px auto;position: absolute;top: 0px;height: 100%;display: none;"
             id="system_action">
            <div style="width: 100%;height: 100%;position: absolute;background: rgba(0, 0, 0, 0.24);"
                 onclick="this.parentNode.style.display='none';"></div>
            <div style="position: absolute;top: 10px;width: 100%;">
                <div style="
    width:  900px;
    margin:  0px auto;
    background: #222222;
    border: 1px #ccc solid;
    padding:  15px;
">
                    <div class="settings" style="
">
                        <h2 style="  margin: 0; border-bottom: 1px #ccc solid; color: #72ff7e; padding-bottom:  3px;
">Settings</h2>
                        <table>
                            <tbody>
                            <tr>
                                <td>Ajax загрузка страниц</td>
                                <td>
                                    <input type="checkbox" onclick="setSetting(this);" name="system_header_request"
                                           value="1" <?= (!empty(ga()->cookie('system_header_request', '')) ? 'checked=""' : '') ?>>
                                    <br>При включенной опции большинство запросов, в которых будет передаваться немного
                                    данных будут проходить методом GET, а сами данные передаваться в заголовке запроса.
                                    <br>При запросах в которых ожидается получать много данных рекомендуется выключать.
                                </td>
                            </tr>
                            <tr>
                                <td>Шифрование форм</td>
                                <td>
                                    <input type="checkbox" onclick="setSetting(this);" name="system_crypt_form"
                                           value="1" <?= (!empty(ga()->cookie('system_crypt_form', '')) ? 'checked=""' : '') ?>>
                                    <br>При включенной опции все значения формы будут зашифрованы
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="system_log"><h2 style="  margin: 0; border-bottom: 1px #ccc solid; color: #72ff7e; padding-bottom:  3px;
">Log</h2> <?= ga()->printLog() ?></div>
                </div>

            </div>

        </div>
        <?php
    }
}