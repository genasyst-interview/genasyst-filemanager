<?php

class gaSystemLayout extends gaLayout
{
    function execute()
    {
        $this->executeAction('head', new gaHeadAction());
        $this->executeAction('footer', new gaFooterAction());
        $this->executeAction('system', new gaSystemAction());

    }

    function view()
    {
        $c = ga()->config();
        echo '<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=' . $c->get('Charset') . '">
	<title>' . ga('s', 'HTTP_HOST') . '</title>
	<style>/*COMPRESS_CSS*/</style>
	<script type="text/javascript">
	/*COMPRESS_JS*/
		ga.setData({"request":{
		"Module":  ' . ga('h')->jsParam($c->get('Module')) . ',
		"Action":  ' . ga('h')->jsParam($c->get('Action')) . ',
		"Directory":  ' . ga('h')->jsParam($c->get('Directory')) . ',
		"Charset":  ' . ga('h')->jsParam($c->get('Charset')) . ',
		"param1":  "",
		"param2":  "",
		"param3":  "",
		"_target":  "_self",
		"_action":  "//' . ga('s', 'HTTP_HOST') . ga('s', "PHP_SELF") . '"
		}});
		' . ga()->crypt()->js() . '
	</script>
	</head>
	<body>';
        $ga_form = ['form' => ['name' => 'gaf', 'style' => 'display:none;']];
        foreach (['Module', 'Action', 'Directory', 'param1', 'param2', 'param3', 'Charset'] as $v) {
            $ga_form['fields'][$v] = ['t' => 'h'];
        }
        echo gaForm::create($ga_form['fields'], $ga_form['form'])->get();
        echo '<div style="position:absolute; width:100%;top:0;left:0;">',
        $this->printBlock('head'),
        '<div id="module-content">', $this->printContent(), '</div>',
        $this->printBlock('footer'), $this->printBlock('system'),
        '</div></body></html>';
    }
}