<?php

trait gaLayoutTrait
{
    protected $_blocks = [];

    public function setContent($content_obj = null)
    {
        $this->_blocks['__content'] = $content_obj;
    }

    public function getContent()
    {
        return ifset($this->_blocks, '__content', '');
    }

    public function printContent()
    {
        $c = $this->getContent();
        if (is_object($c)) {
            $c->fetch();
        } else {
            echo $c;
        }
    }

    public function fetch()
    {
        foreach ($this->_blocks as $v) {
            if (is_object($v) && method_exists($v, 'execute') && !($v instanceof gaModule)) {
                $v->execute();
            }
        }
        ga()->sendHeaders();
        $this->view();
    }

    public function display($content_obj = null)
    {
        $this->execute();
        if ($content_obj) {
            $this->setContent($content_obj);
        }
        $this->fetch();
    }

    /*TODO: this alias?*/
    public function executeAction($key, $obj)
    {
        $this->setBlock($key, $obj);
    }

    public function setBlock($key, $value)
    {
        $this->_blocks[$key] = $value;
    }

    public function getBlock($name)
    {
        return ifset($this->_blocks, $name, '');
    }

    public function printBlock($name)
    {
        $c = $this->getBlock($name);
        if (is_object($c)) {
            $c->display();
        } else {
            echo $c;
        }
    }
}