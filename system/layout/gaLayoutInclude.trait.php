<?php

trait gaLayoutIncludeTrait
{
    /**
     * @var gaLayout
     */
    protected $_layout = null;
    protected $_show_layout = true;

    public function setLayout(gaLayout $layout)
    {
        $this->_layout = $layout;
    }

    /**
     * @return gaLayout|false
     */
    public function getLayout()
    {
        return (is_object($this->_layout)/*$this->_layout instanceof gaLayout*/) ? $this->_layout : false;
    }

    public function isShowLayout()
    {
        return $this->_show_layout;
    }

    public function showLayout()
    {
        $this->_show_layout = true;
    }

    public function hideLayout()
    {
        $this->_show_layout = false;
    }
}