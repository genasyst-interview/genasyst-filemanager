<?php

namespace phpCompressor\PhpParser\Traverses\Entity\Global_;

use  phpCompressor\PhpParser\Traverses\Entity\Entity;
use phpCompressor\PhpParser\Visitors\Base\LocalVariables;
use PhpParser\Node;
use phpCompressor;

class Function_ extends Entity
{
    /**
     * @var  Node\Stmt\ClassLike $data
     */
    protected $data = null;
    protected $scope_object = null;
    protected $properties_names_storage = null;

    public function __construct(Node\Stmt\Function_ $node)
    {
        parent::__construct($node);
    }

    protected function getVisitors()
    {
        return [
            new LocalVariables($this->scope_object),
        ];
    }

    protected function initVisitors($traverser)
    {
        foreach ($this->getVisitors() as $visitor) {
            $traverser->addVisitor($visitor);
        }
    }

    public function beforeTraverse()
    {
        // var_dump($this->scope_object->fte());
        //  echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //  ';
        $traverser = new \phpCompressor\PhpParser\Traverses\MethodTraverser('beforeNode');
        $this->initVisitors($traverser);
        // $traverser->traverse($this->data->params);
        $data = [&$this->data];
        $traverser->traverse($data);
        //var_dump($this->scope_object->fte());
        // var_dump($this->data->stmts);
        $this->data->setAttribute('scope_object', $this->scope_object);
        //var_dump($this->data->getAttribute('scope_object')->fte());

    }

    public function traverse()
    {
//var_dump($this->data);
        // var_dump($this->getVisitors());
        $traverser = new \phpCompressor\PhpParser\Traverses\MethodTraverser('leaveNode');
        $this->initVisitors($traverser);
        $data = [&$this->data];
        $traverser->traverse($data);
        //var_dump($this->data->params);
    }

    public function afterTraverse()
    {
        $traverser = new \phpCompressor\PhpParser\Traverses\MethodTraverser('afterNode');
        $this->initVisitors($traverser);
        // $traverser->traverse($this->data->params);
        $data = [&$this->data];
        $traverser->traverse($data);
        // var_dump($this->data->stmts);
        $this->data->setAttribute('scope_object', $this->scope_object);
    }

    public function getName()
    {
        return $this->data->name;
    }

    public function getScopeObject()
    {
        return $this->scope_object;
    }

}