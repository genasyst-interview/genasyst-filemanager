<?php

namespace phpCompressor\PhpParser\Visitors\Traverser;

use PhpParser\Node;
use phpCompressor;
use phpCompressor\PhpParser\Traverses\Entity;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

class GlobalVisitor extends NodeVisitorAbstract
{
    protected $visitors = [];

    public function __construct()
    {
        $this->visitors = [
            new ClassVisitor(),
            new FunctionVisitor(),
        ];
    }

    public function beforeTraverse(array $nodes)
    {
        $d = new phpCompressor\PhpParser\ScopeData\ScopeData();
        $traverser = new NodeTraverser();
        //var_dump($nodes);
        $traverser = new phpCompressor\PhpParser\Traverses\MethodTraverser('beforeNode', $d);
        $traverser->traverse($nodes);
        /*$d= \phpCompressor\NamesRegistry\Constants::getInstance();
        $names = $d->getAll();
        $count = 0;
        foreach($names as $name=>$c){
            $count += strlen($name)*$c;
        }
        arsort($names);
        var_dump($count);
        var_dump($names);*/
        $traverser = new phpCompressor\PhpParser\Traverses\MethodTraverser('leaveNode');
        $traverser->traverse($nodes);

    }

    public function leaveNode(Node $node)
    {
        /* foreach($this->visitors as $v) {
             $v->leaveNode($node);
         }*/
    }

    public function afterTraverse(array $nodes)
    {
        /* foreach($this->visitors as $v) {
             $v->afterTraverse($nodes);
         }*/
    }

    public function __call($name, $arguments)
    {
        echo 'Not found stage method ' . get_class($this) . "::" . $name;
    }
}