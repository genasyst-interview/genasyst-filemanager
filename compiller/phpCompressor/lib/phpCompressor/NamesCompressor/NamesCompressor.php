<?php

namespace phpCompressor\NamesCompressor;

use phpCompressor\NamesRegistry\NamesRegistry;
use phpCompressor\Utils\StringGenerator;

class NamesCompressor
{

    protected $registry = null;
    protected $data = [];

    public function __construct(NamesRegistry $registry)
    {
        $this->init($registry);
    }

    protected function init(NamesRegistry $registry)
    {
        $names = $registry->getAll();
        arsort($names);
        $generator = new StringGenerator();
        $short_names = $generator->range('a', 'zzzzz', count($names), $registry->getExcludeNames());
        /*   echo'!!!!!!!!!!!!';
           var_dump($names);
           var_dump($short_names);*/
        $this->data = array_combine(array_keys($names), $short_names);

    }

    public function set($name)
    {
        $str_gen = new StringGenerator();
        $start = !empty(self::$variables_replaces) ? end(self::$variables_replaces) : 'a';
        $short_names = $str_gen->range('a', 'zzzzz', count($names));
        /* Если не пустой массив избегаем дублирования, вырезаем стартовый элемент,
        от которого начинаем генерацию, тк. он тоже попадает в результат, а по факту уже есть */
        if (!empty(self::$variables_replaces)) {
            if (isset($data[0])) {
                unset($data[0]);
            }
        }
        self::$variables_replaces = array_merge(self::$variables_replaces, $data);
    }

    public function getShortName($name)
    {
        return $this->data[$name];
    }

}