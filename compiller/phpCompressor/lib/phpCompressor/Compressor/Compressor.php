<?php


namespace phpCompressor\Compressor;

use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter;
use PhpParser\Error;
use phpCompressor\PhpParser;
use phpCompressor\NamesRegistry;

//use phpCompressor\PhpParserVisitors\Functions;


class Compressor
{

    protected $content = '';

    protected $whitespaces = false;

    protected $comments = false;

    protected $variables = [
        'user_variables'         => false,
        'class_variables'        => false,
        'class_static_variables' => false,
    ];
    protected $variables_visitors = [
        'class_variables'        => 'phpCompressor\\PhpParserVisitors\\Variables\\ClassVariables',
        'class_static_variables' => false,
        'user_variables'         => 'phpCompressor\\PhpParserVisitors\\Variables\\UserVariables',
    ];

    protected $functions = [
        'user_functions'         => false,
        'class_static_functions' => false,
        'class_functions'        => false,
    ];
    /**
     * @var NodeTraverser
     */
    protected $traverser = null;

    public function __construct($data = [])
    {

    }

    public function compress()
    {
        try {
            $statements = $this->getStatements();
            // $this->initPhpParserVisitors($traverser, 'prepare');
            // $prepare_statements = $traverser->traverse($statements);

            $traverser = $this->getTraverser();
            $traverser->addVisitor(new \phpCompressor\PhpParser\Visitors\Traverser\GlobalVisitor());
            //$this->initPhpParserVisitors($traverser, 'replace');
            $new_statements = $traverser->traverse($statements);
            //var_dump($new_statements);
            $this->content = $this->getCompiller()->prettyPrint($new_statements);
            //  var_dump($this->content);
        } catch (Error $e) {
            echo 'Parse Error: ', $e->getMessage();
        }
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getStatements($perfer = ParserFactory::PREFER_PHP5)
    {
        $parser = (new ParserFactory)->create($perfer);

        return $parser->parse($this->content);
    }

    /**
     * @var \phpCompressor\NamesRegistry\NamesRegistry $registry
     */
    protected function getPhpParserVisitors($stage = 'prepare')
    {
        foreach ($this->variables as $type => $on) {
            if ($on && class_exists($this->variables_visitors[$type])) {
                $class = $this->variables_visitors[$type];
                $traverser->addVisitor(new $class($stage));
            }
        }
    }

    /**
     * @return PrettyPrinter\Standard
     */
    protected function getCompiller()
    {
        return new PrettyPrinter\Standard;
    }

    protected function getTraverser()
    {
        return new \PhpParser\NodeTraverser();
    }

    public function removeWhitespaces()
    {
        $this->whitespaces = true;
    }

    public function removeComments()
    {
        $this->comments = true;
    }

    /**********************
     *  Variables settings
     **********************/
    public function compressUserVariablesName()
    {
        $this->variables['user_variables'] = true;
    }

    public function compressObjectsVariablesName()
    {
        $this->variables['class_variables'] = true;
    }

    public function compressClassStaticVariablesName()
    {
        $this->variables['class_static_variables'] = true;
    }

    /**********************
     *  Functions settings
     **********************/
    public function compressUserFunctionsName()
    {
        $this->variables['user_functions'] = true;
    }

    public function compressObjectsFunctionsName()
    {
        $this->variables['class_static_functions'] = true;
    }

    public function compressClassStaticFunctionsName()
    {
        $this->variables['class_functions'] = true;
    }

    public function setContentByFile($file)
    {
        if (false === file_exists($file)) {
            //throw new \Exception('Not found file '.$file);
        }

        return $this->content = file_get_contents($file);
    }

    public function setContentByCode($code)
    {
        $this->content = $code;
    }

    public function parseBlock($blockCode)
    {
        $this->content = '<?php ' . $blockCode;
    }


}