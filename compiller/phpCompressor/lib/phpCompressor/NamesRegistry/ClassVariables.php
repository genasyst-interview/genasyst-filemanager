<?php

namespace phpCompressor\NamesRegistry;

class ClassVariables
{
    protected static $instance;
    protected static $data = [];
    protected $class = '';

    public function __construct($class)
    {
        $this->class = $class;
    }

    public function set($name)
    {
        if ($this->has($name)) {
            self::$data[$this->class][$name]++;
        } else {
            self::$data[$this->class][$name] = 1;
        }
    }

    protected function hasClass($class)
    {
        return array_key_exists($class, self::$data);
    }

    public function has($name)
    {
        return ($this->hasClass($this->class) && array_key_exists($name, self::$data[$this->class][$name]));
    }

    public function get($name)
    {
        if ($this->has($name)) {
            return self::$data[$this->class][$name];
        }

        return 0;
    }

    public function getAll($class = null)
    {
        if ($class === false) {
            return self::$data;
        }
        if ($class === null) {
            return $this->hasClass($this->class) ?
                self::$data[$this->class] : [];
        }

        return $this->hasClass($class) ?
            self::$data[$class] : [];
    }
}