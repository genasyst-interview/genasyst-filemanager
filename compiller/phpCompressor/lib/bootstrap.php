<?php
include_once __DIR__ . '/phpParser/lib/bootstrap.php';
if (!class_exists('phpCompressor\Autoloader')) {
    require __DIR__ . '/phpCompressor/Autoloader.php';
}
phpCompressor\Autoloader::register();
