<?php

class gaFilesystemCallBackConsoleFile extends gaFilesystemFile {
    
    protected function init() {
        
    }
    public function getModify() {
        return @filemtime($this->file_path);
    }

    public function getPath()
    {
        return basename($this->file_path);
    }

    public function getSize()
    {
        return @filesize($this->file_path);
    }

    public function getType()
    {
        $file_path = $this->file_path;
        $type = 'file';
        if (isdir($file_path)) {
            $type = 'dir';
        } elseif(@is_link($file_path)) {
            $type = 'link';
        }
        return $type;
    }

    public function getLink()
    {
        $file_path = $this->file_path;
        return  @is_link($file_path)?readlink($file_path):false;
    }

    public function getOwner()
    {
        return  @fileowner($this->file_path);
    }


    public function getGroup()
    {
        return  @filegroup($this->file_path);
    }

    public function getPerms()
    {
        return  @fileperms($this->file_path);
    }

    public function isWriteable()
    {
        return iswritable($this->file_path);
    }

    public function isReadable()
    {
        return isreadable($this->file_path);
    }
}