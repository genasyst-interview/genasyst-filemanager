<?php
class gaFilesystemFilesIterator implements Iterator {

	protected $_position = 0;

	protected $data = [];

    protected $class_name = '';

	public function __construct($data = [], $class_name = '')
	{
		$this->data = ifisar($data, []);
	}

	public function rewind()
	{
		$this->_position = 0;
	}

	public function current()
	{
		return $this->data[$this->_position];
	}

	public function key()
	{
		return $this->_position;
	}

	public function next()
	{
		++$this->_position;
	}

	public function valid()
	{
		return isset($this->data[$this->_position]);
	}

}