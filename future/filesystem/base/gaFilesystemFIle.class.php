<?php
abstract class gaFilesystemFile implements ArrayAccess {
	
	protected $finfo = [
        'path' => '',
        'name' => '',
        'size' => 0,
        'type'=> 'file',
        'link'=> 'file',
        'owner' => 0,
        'group'=> 0,
        'modify' => 0,
        'perms' => 0,
        'str_perms' => 'u---------',
        'oct_perms' =>  '0000',
        'writeable' => false,
        'readable'  => false,
    ];
    
    public function getName() 
    {
        return $this->filename;
    }
    
    public function getPath()
    {
        return basename($this->file_path);
    }
    abstract public function getSize();
    abstract public function getType();
    abstract public function getLink();
    abstract public function getOwner();
    abstract public function getGroup();
    abstract public function getModify();
    abstract public function getPerms();
    abstract public function isWriteable();
    abstract public function isReadable();
 
    protected $file_path = '';
    
    protected $filename = null;
	protected $data = [];
	
   	public function __construct($file_path, $filename = null)
	{
        if(empty($filename)) {
            $filename = basename($file_path);
        }
		$this->file_path = $file_path;
        $this->filename = $filename;
        $this->init();
	}
    
    protected function init(){}
   
	public function offsetExists($offset)
	{
		return arkex($offset, $this->data);
	}

	public function offsetGet($offset)
	{
		return arget($this->data, $offset);
	}

	public function offsetSet($offset, $value)
	{
		$this->data[$offset] = $value;
	}

	public function offsetUnset($offset){}

}